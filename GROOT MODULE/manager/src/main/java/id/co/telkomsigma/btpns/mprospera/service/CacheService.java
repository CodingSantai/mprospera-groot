package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service("cacheService")
public class CacheService extends GenericService {

    @Autowired
    AreaManager areaManager;
    @Autowired
    CustomerManager customerManager;
    @Autowired
    LoanManager loanManager;
    @Autowired
    MenuManager menuManager;
    @Autowired
    MMManager mmManager;
    @Autowired
    ParamManager paramManager;
    @Autowired
    PDKManager pdkManager;
    @Autowired
    RoleManager roleManager;
    @Autowired
    SentraManager sentraManager;
    @Autowired
    SWManager swManager;
    @Autowired
    TerminalManager terminalManager;
    @Autowired
    UserManager userManager;
    @Autowired
    ManageApkManager manageApkManager;
    @Autowired
    PmManager pmManager;
    @Autowired
    LocationManager locationManager;
    @Autowired
    SurveyManager surveyManager;
    @Autowired
    EODManager eodManager;

    public void clearAreaCache() {
        areaManager.clearCache();
    }

    public void clearCustomerCache() {
        customerManager.clearCache();
    }

    public void clearLoanCache() {
        loanManager.clearCache();
    }

    public void clearMenuCache() {
        menuManager.clearCache();
    }

    public void clearMMCache() {
        mmManager.clearCache();
    }

    public void clearParamCache() {
        paramManager.clearCache();
    }

    public void clearPDKCache() {
        pdkManager.clearCache();
    }

    public void clearRoleCache() {
        roleManager.clearCache();
    }

    public void clearSentraCache() {
        sentraManager.clearCache();
    }

    public void clearSWCache() {
        swManager.clearCache();
    }

    public void clearTerminalCache() {
        terminalManager.clearCache();
    }

    public void clearUserCache() {
        userManager.clearCache();
    }

    public void clearProsperaSurveyCache() {
        surveyManager.clearCache();
    }

    public void clearApkCache() {
        manageApkManager.clearCache();
    }

    public void clearCache() {
        areaManager.clearCache();
        customerManager.clearCache();
        loanManager.clearCache();
        menuManager.clearCache();
        mmManager.clearCache();
        paramManager.clearCache();
        pdkManager.clearCache();
        roleManager.clearCache();
        sentraManager.clearCache();
        swManager.clearCache();
        terminalManager.clearCache();
        userManager.clearCache();
        pmManager.clearCache();
        manageApkManager.clearCache();
        locationManager.clearCache();
        eodManager.clearCache();
    }

}