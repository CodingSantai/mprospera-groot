package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.SentraDao;
import id.co.telkomsigma.btpns.mprospera.manager.SentraManager;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service("sentraManager")
public class SentraManagerImpl implements SentraManager {

	@Autowired
	private SentraDao sentraDao;

	@Override
	public Page<Sentra> findByCreatedDate(String username, String loc, Date startDate, Date endDate) {
		// TODO Auto-generated method stub
		if(username == null){
			return sentraDao.findByCreatedDateWithLoc(loc, startDate, endDate, new PageRequest(0, Integer.MAX_VALUE));
		}else{
			return sentraDao.findByCreatedDate(username,startDate, endDate, new PageRequest(0, Integer.MAX_VALUE));
		}
	}

	@Override
	public Page<Sentra> findByCreatedDatePageable(String username, String loc, Date startDate, Date endDate, PageRequest pageRequest) {
		// TODO Auto-generated method stub
		if(username == null){
			return sentraDao.findByCreatedDateWithLoc(loc, startDate, endDate, pageRequest);
		}else{
			return sentraDao.findByCreatedDate(username,startDate, endDate, pageRequest);
		}
	}

	@Override
	public void clearCache() {
		// TODO Auto-generated method stub
	}

	@Override
	public List<Sentra> findAll() {
		// TODO Auto-generated method stub
		return sentraDao.findAll();
	}

}