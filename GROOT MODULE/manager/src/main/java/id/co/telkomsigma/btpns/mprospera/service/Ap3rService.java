package id.co.telkomsigma.btpns.mprospera.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.util.TransactionIdGenerator;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;

@Service("ap3rservice")
public class Ap3rService {
	
    protected Log log = LogFactory.getLog(this.getClass());

    @Autowired
    private LoanService loanService;

    @Autowired
    private AreaService areaService;

    @Autowired
    private ReportService reportService;

    @Autowired
    private CacheManager cacheManager;
    
    @Autowired
    private AuditLogService auditLogService;
    
    @Autowired
    private TerminalActivityService terminalActivityService;
    
    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    
    @Autowired
    private TerminalService terminalService;
    
    protected final Log LOGGER = LogFactory.getLog(getClass());
    private int days;
    private String reffNo;
    
    
    private void loadAreaDataToMemory() {
        List<Location> locs = areaService.findAllKfo();
        for (Location loc : locs) {
            cacheManager.getCache("gro.location.findByCode").put(loc.getLocationCode(), loc);
            cacheManager.getCache("gro.location.findByLocationId").put(loc.getLocationId(), loc);
        }
    }

    public void generateAp3r(String strStartDate, String strEndDate) throws Exception {
		int ok = 0;
		int fail = 0;
        String fileName = "/apps/report/AP3R_M-Prospera_No_APPID";
        String zipName = "/apps/report/";
		String logFileNameErr = zipName+new SimpleDateFormat("yyyyMMdd").format(new Date())+".error.log";
		String logFileNameOK = zipName+new SimpleDateFormat("yyyyMMdd").format(new Date())+".success.log";
		LOGGER.info("generateAp3r service running: "+strStartDate+" - "+strEndDate);

		try {
    		days = getDays()==0?6:getDays()+1;
	        loadAreaDataToMemory();
	
	        Map<String, Object> parameters = new HashMap<String, Object>();
	
	        Date startDate = new Date();
	        Date endDate = new Date();
	
	        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	        final String dateTimeFormatPattern = "yyyyMMMdd";
	        final SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
	
	        if (strStartDate != null && !"".equals(strStartDate)) {
	            startDate = java.sql.Date.valueOf(LocalDate.parse(strStartDate, dtf));
	        }
	
	        // convert date to calendar
	        Calendar c = Calendar.getInstance();
	        c.setTime(startDate);
	
	        // manipulate date
	        //c.add(Calendar.MONTH, 1);
	        c.add(Calendar.DATE, days);
	        c.clear(Calendar.HOUR_OF_DAY);
	        c.clear(Calendar.AM_PM);
	        c.clear(Calendar.MINUTE);
	        c.clear(Calendar.SECOND);
	        c.clear(Calendar.MILLISECOND);//same with c.add(Calendar.DAY_OF_MONTH, 1);
	
	        // convert calendar to date
	        Date currentDatePlusOne = c.getTime();
	
	        if (strEndDate != null && !"".equals(strEndDate)) {
	            endDate = java.sql.Date.valueOf(LocalDate.parse(strEndDate, dtf));
	            Calendar cEnd = Calendar.getInstance();
	            cEnd.setTime(endDate);
	            cEnd.add(Calendar.DATE, days);
	            cEnd.clear(Calendar.HOUR_OF_DAY);
	            cEnd.clear(Calendar.AM_PM);
	            cEnd.clear(Calendar.MINUTE);
	            cEnd.clear(Calendar.SECOND);
	            cEnd.clear(Calendar.MILLISECOND);
	            endDate = cEnd.getTime();
	        } else {
	            endDate = currentDatePlusOne;
	        }
	
	        List<Loan> loanList = loanService.findLoanByDisbursementDate(startDate, endDate);
	        for (Loan loan : loanList) {
	        	if (loan.getSwId() != null) {
                    String appidInfo = "";
	        		try {
        				long start = System.currentTimeMillis();
	        			Long loanId = loan.getLoanId();
	                    parameters.put("loanId", loanId);
	                    String wisma = loan.getAppId().substring(0, 5);
	                    Location wismaLoc = areaService.findByCode(wisma);
	                    String kfoId = wismaLoc.getParentLocation();
	                    Location kfoLoc = areaService.findLocationById(kfoId);
	                    String kfoCode = kfoLoc.getLocationCode();
	                    String kfoName = kfoLoc.getName();
	        			appidInfo = kfoCode + "-" + kfoName + "-" + loan.getAppId();
	                    File file = new File(fileName + "_" + loan.getAppId() + "_" + sdf.format(loan.getDisbursementDate()) + ".pdf");
	                    ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
	                    reportService.generatePdf("AP3R.jrxml", parameters, byteArrayOutputStream);
	                    byte[] bniBytes = byteArrayOutputStream.toByteArray();
	                    try(FileOutputStream fos = new FileOutputStream(file)){
							fos.write(bniBytes);
							ZipFile zipFile = new ZipFile(zipName + kfoCode + kfoName + ".zip");
							ZipParameters zipParameters = new ZipParameters();
							zipParameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
							zipParameters.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
							String dateJustYMD = String.valueOf(loan.getDisbursementDate()).substring(0, 10);
							zipParameters.setRootFolderInZip(dateJustYMD);
							zipFile.addFile(file, zipParameters);
							fos.flush();
						}catch (Exception e){
	                    	e.printStackTrace();
						}
	                    file.delete();
	                    ok++;
                        long duration = System.currentTimeMillis()-start;
	                	saveLogFile(logFileNameOK,"Success generateAp3r: "+appidInfo+" ("+duration+" ms)");
		            } catch (Exception e) {
		            	fail++;
		            	log.error("Error generateAp3r loanList: "+e.getMessage());
		            	saveLogFile(logFileNameErr,appidInfo+": "+e.getMessage());
		            }
	        	}
	        }
    	}catch (Exception e) {
        	log.error("Error generateAp3r: "+e.getMessage());
        	saveLogFile(logFileNameErr,"Error generateAp3r: "+e.getMessage()+"\n"+e.toString());
		}finally {
			log.info(WebGuiConstant.GENERATE_AP3R+": Success="+ok+", Error="+fail);
			auditLogService.insertAuditLog(generateAuditLog(WebGuiConstant.GENERATE_AP3R+": Success="+ok+", Error="+fail));
			
            try {
                threadPoolTaskExecutor.execute(new Runnable() {
                    @Override
                    public void run() {
                        Terminal terminal = terminalService.loadTerminalByImei("358525072034683");
                        
                        TerminalActivity terminalActivity = new TerminalActivity();
                        // Logging to terminal activity
                        terminalActivity.setTerminalActivityId(TransactionIdGenerator.generateTransactionId());
                        terminalActivity.setActivityType(TerminalActivity.ACTIVITY_AP3R_REPORT);
                        terminalActivity.setCreatedDate(new Date());
                        terminalActivity.setReffNo(getReffNo());
                        terminalActivity.setSessionKey("");
                        terminalActivity.setTerminal(terminal);
                        terminalActivity.setUsername("system");

                        terminalActivityService.saveTerminalActivityAndMessageLogs(terminalActivity, null);
                    }
                });
            } catch (Exception e) {
                log.error("generateAp3r saveTerminalActivityAndMessageLogs error: " + e.getMessage());
            }
		}
    }

	private AuditLog generateAuditLog(String description) {
		reffNo = new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date());
		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(AuditLog.GENERATE_REPORT);
		auditLog.setCreatedBy("Mprospera");
		auditLog.setCreatedDate(new Date());
		auditLog.setDescription(description);
		auditLog.setReffNo(reffNo);
		return auditLog;
	}

	public int getDays() {
		return days;
	}

	public String getReffNo() {
		return reffNo;
	}

	public void setReffNo(String reffNo) {
		this.reffNo = reffNo;
	}

	public void setDays(int days) {
		this.days = days;
	}
	
	
	private void saveLogFile(String filename, String logtxt) {
		try (FileWriter fw = new FileWriter(filename, true)){
			// the true will Append the new data
			fw.write(logtxt+"\n");// appends the string to the file
		} catch (Exception ioe) {
			log.error("saveLogFile IOException: " + ioe.getMessage());
		}
	}

}