package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.SentraManager;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("sentraService")
public class SentraService extends GenericService {

    @Autowired
    private SentraManager sentraManager;

    public List<Sentra> findAll() {
        // TODO Auto-generated method stub
        return sentraManager.findAll();
    }

}