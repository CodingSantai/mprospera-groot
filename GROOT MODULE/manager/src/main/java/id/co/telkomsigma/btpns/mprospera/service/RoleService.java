package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.RoleManager;
import id.co.telkomsigma.btpns.mprospera.model.user.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.List;

@Service("roleService")
public class RoleService extends GenericService {
    @Autowired
    private RoleManager roleManager;
    
    public List<Role> getRoles() {
        return roleManager.getAll();
    }

	public Role createRole(Role role)
	{
		return roleManager.insertRole(role);
	}

	public Role loadUserByRoleId(final Long roleId)  {
        return roleManager.getRoleById(roleId);
    }

	public Role updateRole(Role role)
	{
		return roleManager.updateRole(role);
	}

	public Page<Role> search(String authority,
			LinkedHashMap<String, String> columnMap, int finalOffset,
			int finalLimit) {
		// TODO Auto-generated method stub
		return roleManager.searchRoles(authority, columnMap, finalOffset, finalLimit);
	}

	public void clearAllRolesCache(){
		roleManager.clearCache();
	}

}