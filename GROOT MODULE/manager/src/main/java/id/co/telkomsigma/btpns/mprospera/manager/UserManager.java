package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.exception.UserProfileException;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.data.domain.Page;

import java.util.LinkedHashMap;
import java.util.List;

//import org.neo4j.cypher.internal.compiler.v2_1.ast.rewriters.isolateAggregation;

/**
 * Created by daniel on 3/31/15.
 */
public interface UserManager {
    /**
     * Gets single user by email
     *
     * @param email
     * @return user or null
     */
    User getUserByEmail(String email);

    /**
     * Gets single user by username
     *
     * @param username
     * @return user or null
     */
    User getUserByUsername(String username);
    
    User getUserByUserId(Long userId);
    
    List<User> getUserByLocId(String locId);

    /**
     * Saves new user (admin / CS)
     *
     * @param user
     * @return user or null
     */
    User insertUser(User user) throws UserProfileException;

    /**
     * Resets current password
     *
     * @param user
     * @return user or null
     */
    User resetPassword(User user);

    /**
     * Searches users by username or email as keyword, with/without given authority order by orderMap from offset limited by limit
     *
     * @param keyword
     * @param authority
     * @param orderMap
     * @param offset
     * @param limit
     * @return list of user found limited by limit from offset and total count of found user wrapped in UserListPojo object
     */
    Page<User> searchUsers(String keyword, String authority, LinkedHashMap<String, String> orderMap, int offset, int limit);
    
    Page<User> searchUsersLogin(String keyword, String status, LinkedHashMap<String, String> orderMap, int offset, int limit);


    /**
     * Updates a user
     *
     * @param user
     * @return user or null
     */
    User updateUser(User user);
    
    User lockUser(User user,Long lockDuration);

	User unlockUser(User user);

    List<User> getAllLockedUser();
	
    Integer failedLoginAttempt(Long userId,String raw,boolean accountNonLocked);

	User enableUser(User user);

	User disableUser(User user);

	void clearCache();

    User updateProfileAdmin(User user);
    
    List<User> getAllUser();
	
}