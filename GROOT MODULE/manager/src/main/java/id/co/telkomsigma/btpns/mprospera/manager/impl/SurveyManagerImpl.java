package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.SurveyDao;
import id.co.telkomsigma.btpns.mprospera.manager.SurveyManager;
import id.co.telkomsigma.btpns.mprospera.model.survey.Survey;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service("surveyManager")
public class SurveyManagerImpl implements SurveyManager {

	@Autowired
	private SurveyDao surveyDao;

	protected final Log log = LogFactory.getLog(getClass());

	@Override
	public Page<Survey> findByCreatedDate(Date startDate, Date endDate, String officeId) {
		return surveyDao.findByCreatedDate(startDate, endDate, officeId,new PageRequest(0, Integer.MAX_VALUE));
	}

	@Override
	public Page<Survey> findByCreatedDatePageable(Date startDate, Date endDate, PageRequest pageRequest, String officeId) {
		return surveyDao.findByCreatedDate(startDate, endDate, officeId, pageRequest);
	}

	@Override
	public void clearCache() {
	}

}