package id.co.telkomsigma.btpns.mprospera.manager.impl;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.ILock;
import id.co.telkomsigma.btpns.mprospera.dao.WowIbBalanceDao;
import id.co.telkomsigma.btpns.mprospera.manager.WowibManager;
import id.co.telkomsigma.btpns.mprospera.model.wowib.WowIbBalance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service("wowibManager")
public class WowibManagerImpl implements WowibManager {

	@Autowired
	private WowIbBalanceDao accountDao;

	@Autowired
	private HazelcastInstance hazelcastInstance;
	
	@Override
	public WowIbBalance findById(Long id) {
		return accountDao.findOne(id);
	}

	@Override
	@Transactional
	public WowIbBalance save(WowIbBalance account) {
		WowIbBalance dbAccount = new WowIbBalance();
		if(account.getPhoneNumber()!=null)
				dbAccount = accountDao.findByPhoneNumber(account.getPhoneNumber());
		ILock lock = hazelcastInstance.getLock(account.getPhoneNumber());
		lock.lock();
		try {
			dbAccount.setPhoneNumber(account.getPhoneNumber());
			dbAccount.setBalance(account.getBalance());
			accountDao.save(dbAccount);
		}finally {
			lock.unlock();
		}
		return dbAccount;
	}
	
}