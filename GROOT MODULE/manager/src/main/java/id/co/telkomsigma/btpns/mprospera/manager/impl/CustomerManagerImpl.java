package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.CustomerDao;
import id.co.telkomsigma.btpns.mprospera.manager.CustomerManager;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("customerManager")
public class CustomerManagerImpl implements CustomerManager {

	@Autowired
	private CustomerDao customerDao;

	@Override
	public void clearCache() {
		// TODO Auto-generated method stub

	}

	@Override
	public Customer findById(long parseLong) {
		// TODO Auto-generated method stub
		return customerDao.findOne(parseLong);
	}

}