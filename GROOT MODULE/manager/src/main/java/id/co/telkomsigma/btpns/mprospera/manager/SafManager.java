package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.saf.Saf;

import java.util.List;

public interface SafManager {

    List<Saf> findAll();

    Saf findById(Long id);

    void delete(Long id);

    void save(Saf saf);

}