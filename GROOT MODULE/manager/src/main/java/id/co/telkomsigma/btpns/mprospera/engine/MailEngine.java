package id.co.telkomsigma.btpns.mprospera.engine;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.UrlResource;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

/**
 * Created by daniel on 4/16/15.
 */
@Service("mailEngine")
public class MailEngine {
    private final Log log = LogFactory.getLog(MailEngine.class);

    private MessageSourceAccessor messages;
    @Autowired
    protected Locale language;

    @Autowired
    public void setMessages(MessageSource messageSource) {
        messages = new MessageSourceAccessor(messageSource);
    }

    public String getMessage(String msgKey) {
        return getMessage(msgKey, null);
    }

    public String getMessage(String msgKey, Object ... args) {
        return getMessage(language, msgKey, args);
    }

    public String getMessage(Locale locale, String msgKey, Object ... args) {
        return messages.getMessage(msgKey, args, locale);
    }

    @Autowired
    private JavaMailSender javaMailSender;

    public void sendHtmlMessage(String setTo, String from, String replyTo, String subject, String message) throws MailException, MessagingException {
        sendMimeMessage(setTo, from, replyTo, subject, message, true, (ByteArrayResource) null, null, null);
    }

    public void sendHtmlMessage(String[] setTo, String from, String replyTo, String subject, String message) throws MailException, MessagingException {
        sendMimeMessage(setTo, from, replyTo, subject, message, true, (ByteArrayResource) null, null, null);
    }

    public void sendMimeMessage(String setTo, String from, String replyTo, String subject, String message, boolean messageAsHtml, String attachmentUrl, String attachmentName, String attachmentContentType) throws MailException, MessagingException, IOException {
        UrlResource urlResource = new UrlResource(attachmentUrl);
        InputStream inputStream = urlResource.getInputStream();
        byte[] byteArrayResource = new byte[inputStream.available()];
        int count = inputStream.read(byteArrayResource);
        while (count > 0){
            sendMimeMessage(setTo, from, replyTo, subject, message, messageAsHtml, new ByteArrayResource(byteArrayResource), attachmentName, attachmentContentType);
        }
    }

    public void sendMimeMessage(String[] setTo, String from, String replyTo, String subject, String message, boolean messageAsHtml, String attachmentUrl, String attachmentName, String attachmentContentType) throws MailException, MessagingException, IOException {
        UrlResource urlResource = new UrlResource(attachmentUrl);
        InputStream inputStream = urlResource.getInputStream();
        byte[] byteArrayResource = new byte[inputStream.available()];
        int count = inputStream.read(byteArrayResource);
        while (count > 0) {
            sendMimeMessage(setTo, from, replyTo, subject, message, messageAsHtml, new ByteArrayResource(byteArrayResource), attachmentName, attachmentContentType);
        }
    }

    public void sendMimeMessage(String setTo, String from, String replyTo, String subject, String message, boolean messageAsHtml, ByteArrayResource attachment, String attachmentName, String attachmentContentType) throws MailException, MessagingException {
        try {
            log.debug("\n\n\n");
            log.debug("========================================================================");
            log.debug("====================== SENDING MIME MESSAGE EMAIL ======================");
            log.debug("========================================================================");
            log.debug("SEND TO : " + setTo);
            log.debug("FROM    : " + from);
            log.debug("REPLY TO: " + replyTo);
            log.debug("SUBJECT : " + subject);
            // log.debug("MESSAGE : " + message);

            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, (attachment != null), "UTF-8");
            helper.setTo(setTo);
            helper.setFrom(from);
            helper.setReplyTo(replyTo);
            helper.setSubject(subject);
            helper.setText(message, messageAsHtml);
            if (attachment != null) {
                helper.addAttachment(attachmentName, attachment, attachmentContentType);
            }
            this.javaMailSender.send(mimeMessage);

            log.debug("RESULT : MAIL SHOULD BE SENT");
            log.debug("========================================================================");
            log.debug("\n\n\n");
        } catch (MailException | MessagingException e) {
            log.debug("RESULT : SEND MAIL FAILED");
            log.error(e.getMessage(), e);
            log.debug("========================================================================");
            log.debug("\n\n\n");
            throw e;
        }
    }

    public void sendMimeMessage(String[] setTo, String from, String replyTo, String subject, String message, boolean messageAsHtml, ByteArrayResource attachment, String attachmentName, String attachmentContentType) throws MailException, MessagingException {
        try {
            StringBuffer sbTo = new StringBuffer();

            for (int i = 0; i < setTo.length; i++) {
                sbTo.append(setTo[i] + ", ");
            }

            sbTo.setLength(sbTo.length() - 2);

            log.debug("\n\n\n");
            log.debug("========================================================================");
            log.debug("====================== SENDING MIME MESSAGE EMAIL ======================");
            log.debug("========================================================================");
            log.debug("SEND TO : " + sbTo.toString());
            log.debug("FROM    : " + from);
            log.debug("REPLY TO: " + replyTo);
            log.debug("SUBJECT : " + subject);
            // log.debug("MESSAGE : " + message);

            MimeMessage mimeMessage = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, (attachment != null), "UTF-8");
            helper.setTo(setTo);
            helper.setFrom(from);
            helper.setReplyTo(replyTo);
            helper.setSubject(subject);
            helper.setText(message, messageAsHtml);
            if (attachment != null) {
                helper.addAttachment(attachmentName, attachment, attachmentContentType);
            }
            this.javaMailSender.send(mimeMessage);

            log.debug("RESULT : MAIL SHOULD BE SENT");
            log.debug("========================================================================");
            log.debug("\n\n\n");
        } catch (MailException | MessagingException e) {
            log.debug("RESULT : SEND MAIL FAILED");
            log.error(e.getMessage(), e);
            log.debug("========================================================================");
            log.debug("\n\n\n");
            throw e;
        }
    }
}
