package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.dao.*;
import id.co.telkomsigma.btpns.mprospera.manager.LoanManager;
import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service("loanManager")
public class LoanManagerImpl implements LoanManager {

	@Autowired
	private LoanDao loanDao;
	
	@Override
	public void save(Loan loan) {
		loanDao.save(loan);
	}

	@Override
	public Page<Loan> findByCreatedDate(Date startDate, Date endDate, String officeId) {
		// TODO Auto-generated method stub
		return loanDao.findByCreatedDate(startDate, endDate, new PageRequest(0, Integer.MAX_VALUE), officeId);
	}

	@Override
	public Page<Loan> findByCreatedDatePageable(Date startDate, Date endDate, PageRequest pageRequest, String officeId) {
		// TODO Auto-generated method stub
		return loanDao.findByCreatedDate(startDate, endDate, pageRequest, officeId);
	}

	@Override
	public void clearCache() {
		// TODO Auto-generated method stub

	}

	@Override
	public Loan findById(long parseLong) {
		// TODO Auto-generated method stub
		return loanDao.findOne(parseLong);
	}

	@Override
	public List<Loan> findLoanByDisbursementDate(Date startDate, Date endDate) {
		return loanDao.findByDisbursementDateAndStatus(startDate,endDate, WebGuiConstant.STATUS_APPROVED);
	}

}