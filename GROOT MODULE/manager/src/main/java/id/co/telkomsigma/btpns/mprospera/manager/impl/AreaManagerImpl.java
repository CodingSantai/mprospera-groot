package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.*;
import id.co.telkomsigma.btpns.mprospera.manager.AreaManager;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import id.co.telkomsigma.btpns.mprospera.model.sda.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Date;
import java.util.List;

@Service("areaManager")
public class AreaManagerImpl implements AreaManager {

	@Autowired
	AreaDistrictDao areaDistrictDao;

	@Autowired
	AreaSubDistrictDao areaSubDistrictDao;
	
	@Autowired
	AreaSubDistrictDetailsDao areaSubDistrictDetailsDao;
	
	@Autowired
	AreaKelurahanDao areaKelurahanDao;

	@PersistenceContext
	public EntityManager em;	
	
	protected final Log log = LogFactory.getLog(getClass());

	@Override
	public AreaKelurahan findById(String id) {
		return areaKelurahanDao.findOne(id);
	}

	@Override
	public List<AreaSubDistrict> getSubDistrictByParentId(String areaId) {
		// TODO Auto-generated method stub
		return areaSubDistrictDao.findByParentAreaId(areaId);
	}

	@Override
	public Page<AreaSubDistrictDetails> getAllSubDistrictDetailsByParentAreaId(String districtAreaId) {
		List<AreaSubDistrict> subDistrictList = areaSubDistrictDao.findByParentAreaId(districtAreaId);
		if(subDistrictList==null || subDistrictList.size()==0)
			return null;
		return areaSubDistrictDetailsDao.searchAllDetailsByParentId(subDistrictList,new PageRequest(0, Integer.MAX_VALUE));
	}

	@Override
	public Page<AreaSubDistrictDetails> findByCreatedDate(String areaId,
			Date parse, Date parse2) {
		List<AreaSubDistrict> subDistrictList = areaSubDistrictDao.findByParentAreaId(areaId);
		if(subDistrictList==null || subDistrictList.size()==0)
			return null;
		return areaSubDistrictDetailsDao.searchDetailsByParentIdAndDate(subDistrictList,parse,parse2,new PageRequest(0, Integer.MAX_VALUE));
	}

	@Override
	public Page<AreaSubDistrictDetails> findByCreatedDatePageable(
			String areaId, Date parse, Date parse2, PageRequest pageRequest) {
		List<AreaSubDistrict> subDistrictList = areaSubDistrictDao.findByParentAreaId(areaId);
		if(subDistrictList==null || subDistrictList.size()==0)
			return null;
		return areaSubDistrictDetailsDao.searchDetailsByParentIdAndDate(subDistrictList,parse,parse2,pageRequest);
	}

	@Override
	public void clearCache() {
		// TODO Auto-generated method stub
		
	}

	@Autowired
	LocationDao locationDao;
	
	@Override
	public List<AreaDistrict> getWismaById(String officeCode) {
		Location loc = locationDao.findOne(officeCode);		
		return areaDistrictDao.findByAreaNameLikeIgnoreCase("%"+loc.getKabupaten()+"%");
	}

	@Override
	public AreaDistrict findDistrictById(String districtCode) {
		return areaDistrictDao.findOne(districtCode);
	}

}