package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.MenuDao;
import id.co.telkomsigma.btpns.mprospera.manager.MenuManager;
import id.co.telkomsigma.btpns.mprospera.model.menu.Menu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("menuManager")
public class MenuManagerImpl implements MenuManager {

    @Autowired
    private MenuDao menuDao;

    @Override
    @Cacheable(value = "gro.menu.getAll", unless = "#result == null")
    public List<Menu> getAll() {
        return menuDao.findAll();
    }


    @Override
    @Cacheable(value = "gro.menu.getListMenuId", unless = "#result == null")
    public List<Menu> getListMenuId(Long roleId) {
        return menuDao.getListMenu(roleId);
    }

    @Override
    @Cacheable(value = "gro.menu.getMenuByMenuId", unless = "#result == null")
    public Menu getMenuByMenuId(Long menuId) {
        // TODO Auto-generated method stub
        return menuDao.findByMenuId(menuId);
    }

    @Override
    @CacheEvict(allEntries = true, beforeInvocation = true, cacheNames = {"gro.menu.getAll",
            "gro.menu.getListMenuId", "gro.menu.getMenuByMenuId"})
    public void clearCache() {
    }

}