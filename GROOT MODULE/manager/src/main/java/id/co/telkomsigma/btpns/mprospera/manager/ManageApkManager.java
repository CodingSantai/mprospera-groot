package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.manageApk.ManageApk;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import org.springframework.data.domain.Page;

import java.util.LinkedHashMap;
import java.util.List;

public interface ManageApkManager {
	
	List<ManageApk> getAll();
	
	Page<ManageApk> search(LinkedHashMap<String, String> orderMap, int offset, int limit);
	
	void uploadApk(byte[] apkBytes, String version, String description,  User user);
	
	ManageApk updateApk(ManageApk updateApk);
	
	ManageApk getById(Long apkId);
	
	ManageApk getByVersion(String version);
	
	void clearCache();

}