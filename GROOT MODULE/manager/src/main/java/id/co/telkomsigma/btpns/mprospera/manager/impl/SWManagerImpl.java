package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.SWDao;
import id.co.telkomsigma.btpns.mprospera.manager.SWManager;
import id.co.telkomsigma.btpns.mprospera.model.sw.SurveyWawancara;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service("swManager")
public class SWManagerImpl implements SWManager {
	
	@Autowired
	SWDao swDao;

	@Override
	public int countAll() {
		// TODO Auto-generated method stub
		return swDao.countAll();
	}

	@Override
	public Page<SurveyWawancara> findAll(List<String> kelIdList) {
		// TODO Auto-generated method stub
		return swDao.findByAreaIdIn(kelIdList,new PageRequest(0, countAll()));
	}

	@Override
	public Page<SurveyWawancara> findByCreatedDate(List<String> kelIdList,Date startDate, Date endDate) {
		// TODO Auto-generated method stub
		Calendar cal = Calendar.getInstance();
		cal.setTime(endDate);
		cal.add(Calendar.DATE, 1);
		return swDao.findByCreatedDate(kelIdList,startDate, cal.getTime(), new PageRequest(0, Integer.MAX_VALUE));
	}

	@Override
	public Page<SurveyWawancara> findByCreatedDatePageable(List<String> kelIdList,Date startDate, Date endDate, PageRequest pageRequest) {
		// TODO Auto-generated method stub
		Calendar cal = Calendar.getInstance();
		cal.setTime(endDate);
		cal.add(Calendar.DATE, 1);
		return swDao.findByCreatedDate(kelIdList,startDate, cal.getTime(), pageRequest);
	}

	@Override
	public void clearCache() {
		// TODO Auto-generated method stub
		
	}

}