package id.co.telkomsigma.btpns.mprospera.feign;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("productServices")
public interface ProductServiceInterface {

    @HystrixCommand
    @HystrixProperty(name = "hystrix.command.default.execution.isolation.thread.timeoutInMilliseconds", value = "300000")
    @RequestMapping(value = "/productServices/clearCache", method = {RequestMethod.POST})
    public String clearAllCache();

}
