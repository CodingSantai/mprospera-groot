package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.dao.RoleDao;
import id.co.telkomsigma.btpns.mprospera.dao.UserDao;
import id.co.telkomsigma.btpns.mprospera.engine.MailEngine;
import id.co.telkomsigma.btpns.mprospera.exception.UserProfileException;
import id.co.telkomsigma.btpns.mprospera.manager.UserManager;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Caching;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by daniel on 3/31/15.
 */
@Service("userManager")
public class UserManagerImpl implements UserManager {

    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private MailEngine mailEngine;

    @Autowired
    private Locale timezone;

    @Value("${mail.reply.to.customer.registration}")
    public String mailReplyToCustomerRegistration;
    @Value("${mail.sender.customer.registration}")
    public String mailSenderCustomerRegistration;
    @Value("${reset.password.link.expiry.hours}")
    private String resetPasswordLinkExpiryHours;
    @Value("${web-gui.url}")
    public String webGuiUrl;

    
	@Bean(name = "passwordEncoder")
	public PasswordEncoder getPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

    
    @Override
    public User getUserByEmail(String email) {
        return userDao.findByEmailIgnoreCase(email.toLowerCase());
    }

    @Override
    public User getUserByUsername(String username) {
     	return userDao.findByUsername(username.toLowerCase());
    }
    
    @Override
    public User getUserByUserId(Long userId) {
    	User user = userDao.findByUserId(userId);
        return user;
    }

    @Override
    @Caching(put = { @CachePut(value = "userByUsername", key = "#user.username", unless = "#result == null"),
            @CachePut(value = "userByUserId", key = "#user.userId", unless = "#result == null") }, evict = {
            @CacheEvict("allLockedUser"), @CacheEvict("allUser"), @CacheEvict("failedLoginAttempt"),
            @CacheEvict("loadUserByUsernameFromUpload") })
    public User insertUser(User user) throws UserProfileException {
        user.setUsername(user.getUsername().toLowerCase());
        user.setCreatedDate(new Date());
        user.setPassword("000000");
        user.setEnabled(true);
        user.setAccountNonLocked(true);
        user.setAccountNonExpired(true);
        user.setCredentialsNonExpired(true);

        // do search user by username / email address
        User temp0 = userDao.findByUsername(user.getUsername());
        User temp1 = userDao.findByEmailIgnoreCase(user.getEmail());
        if (temp0 != null && temp1 != null) {
            throw new UserProfileException(true, true);
        } else if (temp0 != null) {
            throw new UserProfileException(true, false);
        } else if (temp1 != null) {
            throw new UserProfileException(false, true);
        }

        user = userDao.save(user);

        return user;
    }



    @Override
    public User resetPassword(User user) {
        User result = userDao.findByUsername(user.getUsername());
        if (result != null) {
            result.setPassword(passwordEncoder.encode("000000"));
            result.setUpdatedBy(user.getUserId());
            result.setUpdatedDate(new Timestamp(System.currentTimeMillis()));
            result.setVerificationCode("");
            userDao.save(result);
        }
        return result;
    }

    @Override
    public Page<User> searchUsers(String keyword, String authority, LinkedHashMap<String, String> orderMap, int offset, int limit) {
        if (keyword == null) {
            keyword = "";
        }
        keyword = "%" + keyword + "%";
            PageRequest pageRequest = new PageRequest((offset / limit), limit);
            if (authority != null && !authority.equalsIgnoreCase("")) {
                return userDao.getUsers(keyword, authority, pageRequest);
            } else {
                return userDao.findByUsernameLikeOrEmailLikeOrNameLikeAllIgnoreCaseOrderByUsernameAsc(keyword,keyword,keyword, pageRequest);
            }
    }

    @Override
    @Caching(evict = { @CacheEvict(value = "userByUsername", allEntries = true, beforeInvocation = true),
            @CacheEvict(value = "userByUserId", key = "#result.userId", beforeInvocation = true),
            @CacheEvict(value = "userByLocId", key = "#result.officeCode", beforeInvocation = true),
            @CacheEvict(value = "allLockedUser", allEntries = true), @CacheEvict(value = "allUser", allEntries = true),
            @CacheEvict(value = "failedLoginAttempt", allEntries = true),
            @CacheEvict(value = "loadUserByUsernameFromUpload", allEntries = true) })
    public User updateUser(User result) {
    	User fromDb = userDao.findByUserId(result.getUserId());
        if (result.getPassword() != null && !result.getPassword().equalsIgnoreCase("")) {
        	if(!fromDb.getPassword().equals(result.getPassword()))
        		fromDb.setPassword(passwordEncoder.encode(result.getPassword()));
        }
        fromDb.setEmail(result.getEmail());
        fromDb.setName(result.getName());
        fromDb.setAddress(result.getAddress());
        fromDb.setCity(result.getCity());
        fromDb.setProvince(result.getProvince());
        fromDb.setPhone(result.getPhone());
        fromDb.setPhoneMobile(result.getPhoneMobile());
        fromDb.setOfficeCode(result.getOfficeCode());
        fromDb.setRoles(result.getRoles());
        fromDb.setRaw(result.getRaw());
        fromDb.setSessionKey(result.getSessionKey());
        fromDb.setSessionCreatedDate(result.getSessionCreatedDate());
        fromDb.setSessionTime(result.getSessionTime());
        fromDb.setDistrictCode(result.getDistrictCode());
        fromDb.setRoleUser(result.getRoleUser());
        userDao.save(fromDb);
        return result;
    }
    
    @Override
    public User updateProfileAdmin(User user) {
        User result = userDao.findByUsername(user.getUsername());
        if (result != null) {
            if (user.getPassword() != null && !user.getPassword().equalsIgnoreCase("")) {
            	if(!result.getPassword().equals(user.getPassword()))
            		result.setPassword(passwordEncoder.encode(user.getPassword()));
            }
            result.setName(user.getName());
            result.setAddress(user.getAddress());
            result.setCity(user.getCity());
            result.setProvince(user.getProvince());
            result.setPhoneMobile(user.getPhoneMobile());
            result.setPhone(user.getPhone());
            result.setUpdatedBy(result.getUserId());
            result.setUpdatedDate(new Timestamp(System.currentTimeMillis()));
            userDao.save(result);
        }
        return result;
    }


	@Override
	public User lockUser(User user, Long lockDuration) {
		User fromDb = userDao.findByUserId(user.getUserId());
		user.setAccountNonLocked(false);
		
		fromDb.setAccountNonLocked(false);
		Date lockUntil = DateUtils.addMinutes(new Date(), lockDuration.intValue());     			
		fromDb.setCustomProp(WebGuiConstant.PARAMETER_USER_CURRENT_LOCK_UNTIL, ""+lockUntil.getTime());
		userDao.save(fromDb);
		return user;
	}

	@Override
	public User unlockUser(User user) {
		User fromDb = userDao.findByUserId(user.getUserId());
		fromDb.setAccountNonLocked(true);
		userDao.save(fromDb);
		return user;
	}

	@Override
	public List<User> getAllLockedUser() {
		List<User> lockedUserList = userDao.findByAccountNonLocked(false);
		return lockedUserList;
	}

	@Override
	public Integer failedLoginAttempt(Long userId, String raw,boolean accountNonLocked) {
		// TODO Auto-generated method stub
		return userDao.failedLoginAttempt(userId, raw,accountNonLocked);
	}

	@Override
	public User enableUser(User user) {
		User fromDb = userDao.findByUserId(user.getUserId());
		fromDb.setEnabled(true);
		userDao.save(fromDb);
		return user;
	}

	@Override
	public User disableUser(User user) {
		User fromDb = userDao.findByUserId(user.getUserId());
		fromDb.setEnabled(false);
		userDao.save(fromDb);
		return user;
	}

	@Override
	public void clearCache(){
		
	}

	@Override
	public List<User> getAllUser() {
		// TODO Auto-generated method stub
		List<User> users = userDao.findAll();
		return users;
	}

	@Override
	public Page<User> searchUsersLogin(String keyword, String status, LinkedHashMap<String, String> orderMap,
			int offset, int limit) {
		// TODO Auto-generated method stub
		  if (keyword == null) {
	            keyword = "";
	        }
	        keyword = "%" + keyword + "%";
	            PageRequest pageRequest = new PageRequest((offset / limit), limit);
	            if (status != null && !status.equalsIgnoreCase("")) {
	            	if(status.equals("LOGGED IN")){
	            		return userDao.getUsersLogin(keyword, pageRequest);
	            	}else{
	            		return userDao.getUsersLogout(keyword, pageRequest);
	            	}
	            } else {
	                return userDao.findByUsernameLikeOrEmailLikeOrNameLikeAllIgnoreCaseOrderByUsernameAsc(keyword,keyword,keyword, pageRequest);
	            }
	}

	@Override
	public List<User> getUserByLocId(String locId) {
		// TODO Auto-generated method stub
		List<User> userWisma = userDao.findByOfficeCode(locId);
		return userWisma;
	}

}