package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.location.Location;

import java.util.List;

public interface LocationManager {

	void clearCache();

	Location findById(String id);
	
	Location findByCode(String code);
	
	List<Location> findAllKfo();
	
	Location findByLocationId(String id);
	
	List<Location> findByParentLocation(String id);

}