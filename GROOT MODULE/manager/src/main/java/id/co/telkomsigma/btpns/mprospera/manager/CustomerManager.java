package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;

public interface CustomerManager {

	void clearCache();

	Customer findById(long parseLong);

}