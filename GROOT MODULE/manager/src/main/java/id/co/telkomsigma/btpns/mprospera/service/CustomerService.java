package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.CustomerManager;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;


@Service("customerService")
public class CustomerService extends GenericService {

    @Autowired
    @Qualifier("customerManager")
    private CustomerManager customerManager;

    public Customer findById(String customerId) {
        if (customerId == null || "".equals(customerId))
            return null;
        return customerManager.findById(Long.parseLong(customerId));
    }

}