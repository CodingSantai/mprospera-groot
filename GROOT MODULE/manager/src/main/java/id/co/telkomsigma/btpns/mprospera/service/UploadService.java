package id.co.telkomsigma.btpns.mprospera.service;

import id.co.telkomsigma.btpns.mprospera.manager.HolidayManager;
import id.co.telkomsigma.btpns.mprospera.manager.JenisUsahaManager;
import id.co.telkomsigma.btpns.mprospera.manager.LoanProductManager;
import id.co.telkomsigma.btpns.mprospera.manager.WowibManager;
import id.co.telkomsigma.btpns.mprospera.model.Holiday;
import id.co.telkomsigma.btpns.mprospera.model.JenisUsaha;
import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.model.wowib.WowIbBalance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@Service("uploadService")
public class UploadService extends GenericService {

	@Autowired
	private WowibManager wowibManager;

	@Autowired
	private LoanProductManager productManager;

	@Autowired
	private HolidayManager holidayManager;

	@Autowired
	private JenisUsahaManager jenisUsahaManager;

	@Autowired
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;

	public List<Long> childLocation = new ArrayList<>();

	HashMap<Integer, String[]> map = new HashMap<Integer, String[]>();

	int i = 0;

	private String progressMessage;

	public String getProgressMessage() {
		return progressMessage;
	}

	private boolean enable = true;

	public boolean isEnable() {
		return enable;
	}

	private Date startDate;

	public Date getStartDate() {
		return startDate;
	}

	private Date endDate;

	public Date getEndDate() {
		return endDate;
	}

	String barChoice = "NotJU";

	private int count = 0;
	private final Object currentRow = new Object();
	private Long maxRow = 0l;
	private List<String> errorLine = new ArrayList();

	public void insertDb(List<String> dataList, final User admin) {
		String cvsSplitBy = "\\,";
		count = 0;
		maxRow = new Long(dataList.size());
		startDate = new Date();
		errorLine = new ArrayList();

		enable = false;

		Long lineNumber = 1l;
		for (final String str : dataList) {
			log.info("Processing Line " + lineNumber);
			lineNumber++;

			try {
				try {
					threadPoolTaskExecutor.execute(new Runnable() {

						@Override
						public void run() {
							WowIbBalance balance = new WowIbBalance();
							try {
								String[] data = str.split(cvsSplitBy);
								if (data[0].startsWith("62"))
									balance.setPhoneNumber("0" + data[0].substring(2));
								else
									balance.setPhoneNumber(data[0]);
								if (data[1].startsWith("."))
									balance.setBalance(new BigDecimal("0"));
								else
									balance.setBalance(new BigDecimal(data[1]));
								wowibManager.save(balance);
								getProgressBar();
							} catch (Exception e) {
								log.error("Error Wow IB Account = " + balance.getPhoneNumber(), e);
								synchronized (errorLine) {
									errorLine.add("[ Raw Data ]" + str);
								}
								getProgressBar();
							}
						}
					});
				} catch (Exception eL) {
					synchronized (errorLine) {
						errorLine.add("[ Raw Data ]" + str);
					}
					eL.printStackTrace();
					getProgressBar();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	public void insertDbProductRate(List<String> dataList, final User admin) {

		String cvsSplitBy = ";";
		count = 0;
		barChoice = "Products";
		maxRow = new Long(dataList.size()-1L);
		startDate = new Date();
		errorLine = new ArrayList();
		enable = false;
		Long lineNumber = 1l;
		for (final String str : dataList) {
			log.info("Processing Line "+lineNumber);

			try {
				try {
					threadPoolTaskExecutor.execute(new Runnable() {

						@Override
						public void run() {
							LoanProduct product;
							String productCode = new String();
							try {
								if(!str.startsWith("product")) {
									System.out.println("Log Ambil CSV");
									String[] data = str.split(cvsSplitBy);
									productCode = data[0].trim();
									log.debug("productCode : "+productCode);
									
									String temp = data[1].trim().replace(",", ".");
									if(temp.startsWith("."))
										temp = "0"+temp;
									BigDecimal productRate = new BigDecimal(temp, MathContext.DECIMAL32);
									log.debug("productRate : "+productRate);
									
									String temp2 = data[2].trim().replace(",", ".");
									if(temp2.startsWith("."))
										temp2 = "0"+temp2;									
									BigDecimal productIir = new BigDecimal(temp2, MathContext.DECIMAL32);									
									log.debug("productIir : "+productIir);
									
									String jnsNasabah = data[3].trim();
									log.debug("Jenis Nasabah : "+jnsNasabah);
									
									String tjnPembiayaan = data[4].trim();
									log.debug("Tujuan Pembiayaan : "+tjnPembiayaan);
									
									String wismaStatus = data[5].trim();
									log.debug("Wisma Status : "+wismaStatus);
									
									String typeCustumer = data[6].trim();
									log.debug("Type Custumer : "+typeCustumer);
								    
									int tenorBln = Integer.parseInt(data[7].trim());
									log.debug("Tenor Bulan : "+tenorBln);
									
									product = productManager.findByProductCode(productCode);
									if(product != null){
										System.out.println("Log SIMPAN");

										product.setMargin(productRate);
										log.debug("Margin : "+product.getMargin());
										
										product.setProductRate(productRate);
										log.debug("Product Rate : "+product.getProductRate());
										
										product.setMargin(productRate);
										log.debug("Margin : "+product.getProductRate());
										
										product.setIir(productIir);
										log.debug("Iir   : "+product.getIir());
										
										product.setJenisNasabah(jnsNasabah);
										log.debug("Jenis Nasabah : "+product.getJenisNasabah());
										
										product.setTujuanpembiayaan(tjnPembiayaan);
										log.debug("Tujuan Pembiayaan : "+product.getTujuanpembiayaan());
										
										product.setRegularPiloting(wismaStatus);
										log.debug("Wisma Status : "+product.getRegularPiloting());										
										
										product.setTypeNasabah(typeCustumer);
										log.debug("Type Custumer : "+typeCustumer );
										
										product.setTenorBulan(tenorBln);
										log.debug("Tenor Bulan : "+tenorBln);
										
										productManager.save(product);
									} else {
										log.error("Loan Product not found, '" + productCode + "'");
										synchronized (errorLine) {
											errorLine.add("[ Raw Data ]" + str);
										}
									}
								}
								getProgressBar();
							} catch (Exception e) {
								log.error("Error Loan Product = " +productCode, e);
								synchronized (errorLine) {
									errorLine.add("[ Raw Data ]" + str);
								}
								getProgressBar();
							}
						}
					});
				} catch (Exception eL) {
					synchronized (errorLine) {
						errorLine.add("[ Raw Data ]" + str);
					}
					eL.printStackTrace();
					getProgressBar();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			lineNumber++;
		}
	}

	public void insertDbHoliday(List<String> dataList, final User admin) {
		count = 0;
		maxRow = new Long(dataList.size());
		startDate = new Date();
		errorLine = new ArrayList();
		enable = false;
		Long lineNumber = 1l;
		for (final String str : dataList) {
			log.info("Processing Line " + lineNumber);

			try {
				try {
					threadPoolTaskExecutor.execute(new Runnable() {

						@Override
						public void run() {
							Holiday holiday = new Holiday();
							Date date = new Date();
							try {
								String data = str.trim();
								log.debug("Holiday date read : " + data);
								SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
								date = formatter.parse(data);
								holiday = holidayManager.findByDate(date);
								if (holiday == null) {
									holiday = new Holiday();
									holiday.setDate(date);
									log.debug("Saving holiday date : " + holiday.getDate());
									holidayManager.save(holiday);
								} else {
									log.warn("Holiday date already exist, '" + date + "'");
								}
								getProgressBar();
							} catch (Exception e) {
								log.error("Error holiday date = " + date, e);
								synchronized (errorLine) {
									errorLine.add("[ Raw Data ]" + str);
								}
								getProgressBar();
							}
						}
					});
				} catch (Exception eL) {
					synchronized (errorLine) {
						errorLine.add("[ Raw Data ]" + str);
					}
					eL.printStackTrace();
					getProgressBar();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			lineNumber++;
		}
	}

	public void insertDbJenisUsaha(List<String> dataList, final User user) {
		String cvsSplitBy = ";";
		barChoice = "JU";
		count = 0;
		maxRow = new Long(dataList.size());
		startDate = new Date();
		errorLine = new ArrayList();
		enable = false;
		Long lineNumber = 1l;
		for (final String str : dataList) {
			log.info("Start Processing Data for Line : " + lineNumber);
				threadPoolTaskExecutor.execute(new Runnable() {

					@Override
					public void run() {
						try {
						Date date = new Date(System.currentTimeMillis());
						JenisUsaha jenisUsaha = new JenisUsaha();
						String data = str.trim();
						String kdSecEkonomi = "";
						Long identifier = null;
						String[] dataSplit = data.split(cvsSplitBy);
						String check = dataSplit[0].trim();
						if(!"".equals(check)) {
							identifier = Long.parseLong(dataSplit[0].trim());
						}else {
							log.error("Identifier Cannot be NULL");
						}
						String tujuanPem = dataSplit[1].trim();
						String katBidUsaha = dataSplit[2].trim();
						String descUsaha = dataSplit[3].trim();
							   check = dataSplit[4].trim();
						if(!"".equals(check)) {
							kdSecEkonomi = dataSplit[4].trim();
						}
						String sdBaruDesc = dataSplit[5].trim();
						String flaging = dataSplit[6].trim();
						if("add".equals(flaging)) {
						log.debug("kdSecEkonomi : " + kdSecEkonomi);
						log.debug("identifier : " + identifier);
						jenisUsaha = jenisUsahaManager.findByIdentifier(identifier);
						if (jenisUsaha == null) {
							jenisUsaha = new JenisUsaha();
							jenisUsaha.setIdentifier(identifier);log.info("identifier : " + jenisUsaha.getIdentifier());
							jenisUsaha.setTujuanPembiayaan(tujuanPem);log.info("tujuanPem : " + jenisUsaha.getTujuanPembiayaan());
							jenisUsaha.setKatBidangUsaha(katBidUsaha);log.info("katBidUsaha : " + jenisUsaha.getKatBidangUsaha());
							jenisUsaha.setDescUsaha(descUsaha);log.info("descUsaha : " + jenisUsaha.getDescUsaha());
							jenisUsaha.setKdSectorEkonomi(kdSecEkonomi);log.info("kdSecEkonomi : " + jenisUsaha.getKdSectorEkonomi());
							jenisUsaha.setSandiBaruDesc(sdBaruDesc);log.info("sdBaruDesc : " + jenisUsaha.getSandiBaruDesc());
							jenisUsaha.setCreatedDate(date);log.info("date : " + date);
							log.debug("Save Data Jenis Usaha dengan Code : " + jenisUsaha.getKdSectorEkonomi());
							jenisUsahaManager.doSave(jenisUsaha);
						} else {
							log.warn("Data Jenis Usaha dengan Code Identifier : "+jenisUsaha.getIdentifier()+" is Already exist");
						}
					}else if("edit".equals(flaging)) {
						jenisUsaha = jenisUsahaManager.findByIdentifier(identifier);
						jenisUsaha.setTujuanPembiayaan(tujuanPem);
						jenisUsaha.setKatBidangUsaha(katBidUsaha);
						jenisUsaha.setDescUsaha(descUsaha);
						jenisUsaha.setKdSectorEkonomi(kdSecEkonomi);
						jenisUsaha.setSandiBaruDesc(sdBaruDesc);
						jenisUsaha.setCreatedDate(date);
						jenisUsahaManager.doEdit(jenisUsaha);
					}else if ("delete".equals(flaging)) {
						jenisUsaha = jenisUsahaManager.findByIdentifier(identifier);
						jenisUsahaManager.doDelete(jenisUsaha);
					}else {
						log.error("identifier cannot be NULL or flag is not present");
					}
						getProgressBar();
						} catch (Exception ex) {
							log.error(ex.getMessage());
							synchronized (errorLine) {
								errorLine.add("[ RAW Data ]" + str);
								enable = true;
							}
							ex.printStackTrace();
							getProgressBar();
							enable = true;
						}
					}
				});
			lineNumber++;
		}

	}

	public String getErrorBar() {
		synchronized (errorLine) {
			String printout = "";
			for (String error : errorLine) {
				printout = printout + error + "<br>";
			}
			if (!printout.equals("")) {
				printout = "Data contain error(s)<br>" + printout;
				return printout;
			}
		}
		return null;
	}
	
	public void clearBar() {
		errorLine.clear();
		progressMessage = null;
	}

	public synchronized void getProgressBar() {
		synchronized (currentRow) {
			count++;
			Double precentage = new Double(0);
			Double maxRow2 = new Double(maxRow);
			Double currentRow2 = new Double(count);

			precentage = (double) ((currentRow2 / maxRow2) * 100);
			String progress = new DecimalFormat("##.##").format(precentage);

			progressMessage = "Processing row " + count + " of total " + maxRow + " ( " + progress + "%)";
			// button upload dienablekan
			if (progress.equals("100") || count >= maxRow) {
				log.info("progress : "+progress);
				endDate = new Date();
				log.info("endDate : "+endDate.getTime());
				log.info("startDate : "+startDate.getTime());
				long millis = endDate.getTime() - startDate.getTime();
				log.info(millis);

				long second = (millis / 1000) % 60;
				long minute = (millis / (1000 * 60)) % 60;
				long hour = (millis / (1000 * 60 * 60)) % 24;

				String time = String.format("%02d:%02d:%02d", hour, minute, second);
				enable = true;
				if(barChoice.equals("JU")) {
					progressMessage = "Finished importing " + maxRow + " row of raw Jenis Usaha Data "
							+ "<br>start time " + startDate + "<br> end time " + endDate
							+ " <br> duration(hour/menute/second) " + time + "";
				}else if(barChoice.equals("Products")) {
					progressMessage = "Finished importing " + maxRow  + " row of raw Products Data "
							+ "<br>start time " + startDate + "<br> end time " + endDate
							+ " <br> duration(hour/menute/second) " + time + "";
				}else {
					progressMessage = "Finished importing " + maxRow + " row of raw WOW IB Balance Data "
							+ "<br>start time " + startDate + "<br> end time " + endDate
							+ " <br> duration(hour/menute/second) " + time + "";
				}
				barChoice = "NotJU";
				
				
			}
		}
	}
	
	

}