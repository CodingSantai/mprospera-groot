package id.co.telkomsigma.btpns.mprospera.manager;

import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import org.springframework.data.domain.Page;

public interface MessageLogsManager {

    void insert(MessageLogs messageLogs);

    Page<MessageLogs> search(String terminalId, int finalOffset, int finalLimit);

}