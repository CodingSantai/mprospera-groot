package id.co.telkomsigma.btpns.mprospera.service;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.*;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.engine.xml.JasperDesignFactory;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;

import javax.servlet.ServletContext;
import javax.sql.DataSource;
import java.io.File;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.Map;

@Service("reportService")
public class ReportService {
	protected Log log = LogFactory.getLog(this.getClass());

	@Autowired
	@Qualifier("primaryDataSource")
	private DataSource primaryDataSource;

	@Autowired
	private ServletContext servletContext;

	@Autowired
	private ResourceLoader resourceLoader;

	public JasperPrint generateJasperPrint(String name, Map<String, Object> parameters)
			throws Exception {
		try (Connection conn = primaryDataSource.getConnection()){
			File reportFile = resourceLoader.getResource("WEB-INF/classes/jrxml/"+name).getFile();
			JasperDesign design = JRXmlLoader.load(reportFile);
			JasperReport report = JasperCompileManager.compileReport(design);
			return JasperFillManager.fillReport(report, parameters, conn);
		}catch (Exception e){
			log.error(e,e);
			return null;
		}
	}

	public void generateCsv(String name, Map<String, Object> parameters, OutputStream outputStream) throws Exception {
		try {
			JasperPrint jasperPrint = generateJasperPrint(name, parameters);
			JRCsvExporter exporter = new JRCsvExporter();
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream);
			exporter.exportReport();
		} catch (JRException e) {
			log.error("fail generate csv", e);
		}
	}

	public void generateText(String name, Map<String, Object> parameters, OutputStream outputStream) throws Exception {
		try {
			JasperPrint jasperPrint = generateJasperPrint(name, parameters);
			JRTextExporter exporter = new JRTextExporter();
			exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream);
			exporter.setParameter(JRTextExporterParameter.PAGE_WIDTH, 800);
			exporter.setParameter(JRTextExporterParameter.PAGE_HEIGHT, 400);
			exporter.exportReport();
		} catch (JRException e) {
			log.error("fail generate text", e);
		}
	}

	public void generatePdf(String name, Map<String, Object> parameters,
		            OutputStream outputStream) throws Exception {
		        try {
		            JasperPrint jasperPrint = generateJasperPrint(name, parameters);
					/*JRPdfExporter exporter = new JRPdfExporter();
					exporter.setParameter(JRExporterParameter.JASPER_PRINT, jasperPrint);
					exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, outputStream);
					//log.info("PASSWORD : "+parameters.get("filePass"));
					exporter.setParameter(JRPdfExporterParameter.USER_PASSWORD, parameters.get("filePass"));
					exporter.setParameter(JRPdfExporterParameter.IS_ENCRYPTED, Boolean.TRUE);
					exporter.exportReport();*/
					JasperExportManager.exportReportToPdfStream(jasperPrint, outputStream);
		        } catch (JRException e) {
		            log.error("fail generate pdf", e);
		        }
		    }

}