package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.SafDao;
import id.co.telkomsigma.btpns.mprospera.manager.SafManager;
import id.co.telkomsigma.btpns.mprospera.model.saf.Saf;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("safManager")
public class SafManagerImpl implements SafManager {

	@Autowired
	private SafDao safDao;

	@Override
	public List<Saf> findAll() {
		return safDao.findAll();
	}

	@Override
	public Saf findById(Long id) {
		return safDao.findOne(id);
	}

	@Override
	public void delete(Long id) {
		safDao.delete(id);
	}

	@Override
	public void save(Saf saf) {
		safDao.saveAndFlush(saf);
	}

}