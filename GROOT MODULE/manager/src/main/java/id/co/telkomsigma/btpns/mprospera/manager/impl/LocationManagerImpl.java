package id.co.telkomsigma.btpns.mprospera.manager.impl;

import id.co.telkomsigma.btpns.mprospera.dao.LocationDao;
import id.co.telkomsigma.btpns.mprospera.manager.LocationManager;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("locationManager")
public class LocationManagerImpl implements LocationManager {

	@Autowired
	private LocationDao locationDao;

	@Override
	@CacheEvict(value={"gro.location.findAllKfo","gro.location.findLocationByCode","gro.location.findByLocationId",
			"gro.location.findById","gro.location.findByParentLocation"},
			allEntries=true,beforeInvocation = true)
	public void clearCache() {
		// TODO Auto-generated method stub
	}

	@Override
	@Cacheable(value = "gro.location.findById", unless = "#result == null")
	public Location findById(String id) {
		return locationDao.findOne(id);
	}

	@Override
	@Cacheable(value="gro.location.findLocationByCode",unless="#result == null")
	public Location findByCode(String code) {
		// TODO Auto-generated method stub
		return locationDao.findByLocationCode(code);
	}

    @Override
	@Cacheable(value="gro.location.findAllKfo", unless="#result == null")
	public List<Location> findAllKfo() {
		// TODO Auto-generated method stub
		String levelKfo = "4";
		return locationDao.findAllLoc(levelKfo);
	}

	@Override
	@Cacheable(value="gro.location.findByLocationId", unless="#result == null")
	public Location findByLocationId(String id) {
		// TODO Auto-generated method stub
		return locationDao.findByLocationId(id);
	}

	@Override
	@Cacheable(value="gro.location.findByParentLocation", unless="#result == null")
	public List<Location> findByParentLocation(String id) {
		// TODO Auto-generated method stub
		return locationDao.findByParentLocation(id);
	}

}