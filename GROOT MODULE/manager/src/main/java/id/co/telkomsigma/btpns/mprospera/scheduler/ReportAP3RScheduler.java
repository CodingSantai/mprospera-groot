package id.co.telkomsigma.btpns.mprospera.scheduler;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.service.Ap3rService;
import id.co.telkomsigma.btpns.mprospera.service.AuditLogService;

/**
 * Created by Dzulfiqar on 8/05/2017.
 */
@Component
public class ReportAP3RScheduler {

	@Autowired
	private Ap3rService ap3rService;
	
	@Autowired
	private AuditLogService auditLogService;

	protected final Log LOGGER = LogFactory.getLog(getClass());

	@Scheduled(cron = WebGuiConstant.CRON_AP3R)
	public void runReportAp3r() {
		
		try {
			String ipRunning = System.getProperty(WebGuiConstant.IP_AP3R).trim();
			String ip = getIpAddress();
			String daysProperty = null==System.getProperty(WebGuiConstant.SYSTEM_PROPERTY_AP3R_DAYS)?"0":System.getProperty(WebGuiConstant.SYSTEM_PROPERTY_AP3R_DAYS).trim();
			
			LOGGER.info("AP3R Scheduler running.. on "+System.getProperty(WebGuiConstant.SYSTEM_PROPERTY_AP3R)+" for "+ipRunning+" in "+ip);
			if (ip.equals(ipRunning) || ipRunning.equalsIgnoreCase("all")) {
				long start = System.currentTimeMillis();
				LOGGER.info("AP3R Scheduler running.....START");
				auditLogService.insertAuditLog(generateAuditLog(WebGuiConstant.GENERATE_AP3R+"..START"));
				ap3rService.setDays(new Integer(daysProperty).intValue());
				ap3rService.generateAp3r(null,null);

				long duration = System.currentTimeMillis()-start;
				LOGGER.info("AP3R Scheduler running.....FINISH ("+duration+" ms)");
				auditLogService.insertAuditLog(generateAuditLog(WebGuiConstant.GENERATE_AP3R+"..FINISH ("+duration+" ms)"));
			}
		} catch (Exception e) {
			LOGGER.error("error runReportAp3r: "+e.getMessage());
		}
	}

	private String getIpAddress() {
		String ip = "";
		try {
			ip = InetAddress.getLocalHost().getHostAddress();

			String interfaceName = "eth0";
			NetworkInterface networkInterface = NetworkInterface.getByName(interfaceName);
			Enumeration<InetAddress> inetAddress = networkInterface.getInetAddresses();
			InetAddress currentAddress;
			currentAddress = inetAddress.nextElement();
			while (inetAddress.hasMoreElements()) {
				currentAddress = inetAddress.nextElement();
				if (currentAddress instanceof InetAddress && !currentAddress.isLoopbackAddress()) {
					ip = currentAddress.toString();
					break;
				}
			}
			
			if(ip.equals("")) {
				final NetworkInterface netInf = NetworkInterface.getNetworkInterfaces().nextElement();
				Enumeration<InetAddress> addresses = netInf.getInetAddresses();
				
				ip = addresses.nextElement().getHostAddress();
			}

		} catch (Exception e) {
			LOGGER.error("error: "+e.getMessage());
		}

		return ip.replace("/", "");
	}
	
	private static AuditLog generateAuditLog(String description) {
		String reffNo = new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date());
		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(AuditLog.GENERATE_REPORT);
		auditLog.setCreatedBy("Mprospera");
		auditLog.setCreatedDate(new Date());
		auditLog.setDescription(description);
		auditLog.setReffNo(reffNo);
		return auditLog;
	}

}
