package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.pm.ProjectionMeeting;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface PmDao extends JpaRepository<ProjectionMeeting, String> {

    @Query("SELECT p FROM ProjectionMeeting p WHERE p.createdDate>=:startDate AND p.createdDate<:endDate AND p.isDeleted=false AND p.areaId in :areaList")
    Page<ProjectionMeeting> findByCreatedDate(@Param("areaList") List<String> kelIdList,
                                              @Param("startDate") Date startDate, @Param("endDate") Date endDate, Pageable pageRequest);

}