package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.wowib.WowIbBalance;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by fujitsuPC on 5/16/2017.
 */
public interface WowIbBalanceDao extends JpaRepository<WowIbBalance, Long> {

    WowIbBalance findByPhoneNumber(String phoneNumber);

}