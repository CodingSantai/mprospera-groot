package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.loan.Loan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.Temporal;
import org.springframework.data.repository.query.Param;

import javax.persistence.TemporalType;
import java.util.Date;
import java.util.List;

public interface LoanDao extends JpaRepository<Loan, Long> {

	@Query("SELECT s FROM Loan s INNER JOIN s.customer c "
			+ "INNER JOIN s.customer.group g "
			+ "INNER JOIN s.customer.group.sentra st "
			+ ", Location l WHERE st.locationId = l.locationId AND s.createdDate>=:startDate AND s.createdDate<:endDate AND l.locationId = :officeId ORDER BY s.createdDate")
	Page<Loan> findByCreatedDate(@Param("startDate") Date startDate, @Param("endDate") Date endDate,
			Pageable pageRequest, @Param("officeId") String officeId);

	@Query("SELECT la FROM Loan la WHERE la.disbursementDate >= :startDate AND la.disbursementDate < :endDate AND la.status = :status")
	List<Loan> findByDisbursementDateAndStatus(@Param("startDate")@Temporal(value = TemporalType.DATE) Date prsDate,  @Param("endDate")@Temporal(value = TemporalType.DATE) Date prsDate2, @Param("status")String status);

}