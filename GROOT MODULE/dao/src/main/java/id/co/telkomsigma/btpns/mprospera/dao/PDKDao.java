package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.pdk.PelatihanDasarKeanggotaan;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface PDKDao extends JpaRepository<PelatihanDasarKeanggotaan, String> {

	@Query("SELECT COUNT(m) FROM PelatihanDasarKeanggotaan m WHERE m.isDeleted = false")
	int countAll();

	@Query("SELECT m FROM PelatihanDasarKeanggotaan m WHERE m.createdBy IN :userList AND m.isDeleted = false ORDER BY m.createdBy ASC")
	Page<PelatihanDasarKeanggotaan> findAll(@Param("userList") List<String> userList, Pageable pageable);

	@Query("SELECT m FROM PelatihanDasarKeanggotaan m WHERE m.createdBy IN :userList AND m.createdDate>=:startDate AND m.createdDate<:endDate AND m.isDeleted = false ORDER BY m.createdBy ASC")
	Page<PelatihanDasarKeanggotaan> findByCreatedDate(@Param("userList") List<String> userList,
			@Param("startDate") Date startDate, @Param("endDate") Date endDate, Pageable pageable);

}