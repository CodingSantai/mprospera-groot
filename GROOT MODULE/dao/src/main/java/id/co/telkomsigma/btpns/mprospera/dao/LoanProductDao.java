package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
public interface LoanProductDao extends JpaRepository<LoanProduct, Long> {

	LoanProduct findByProductCode(String productCode);
	
	// get Loan by productId, return loanProduct
	LoanProduct getByProductId(Long productId);
	
	
	//coba
//	@Query("SELECT DISTINCT p FROM LoanProduct p WHERE (LOWER(p.productCode) LIKE :keyword OR LOWER(p.productName) LIKE :keyword OR LOWER(p.tenor) LIKE :keyword) ORDER BY p.productCode ASC")
//	Page<LoanProduct> getMappingLoanProduct(@Param("keyword") String keyword, Pageable pageable );
	
//	@Query("SELECT DISTINCT p FROM LoanProduct p WHERE (LOWER(p.loanType) LIKE :keyword OR LOWER(p.productName) LIKE :keyword OR LOWER(p.productCode) LIKE :keyword OR LOWER(p.tenor) LIKE :keyword ) ORDER BY p.productCode ASC")
//	Page<LoanProduct> getMappingLoanProduct(@Param("keyword") String keyword, Pageable pageable );
	
//	@Query("SELECT DISTINCT p FROM LoanProduct p INNER JOIN p.detailMappingProducts mp WHERE (LOWER(p.productName) LIKE :keyword)")
//	Page<LoanProduct> getMappingLoanProduct(@Param("keyword") String keyword, Pageable pageable );

	@Query("SELECT DISTINCT p FROM LoanProduct p WHERE(LOWER(p.jenisNasabah) LIKE :keyword OR LOWER(p.regularPiloting) LIKE :keyword) ORDER BY p.updateDate DESC")
	Page<LoanProduct> getMappingLoanProduct(@Param("keyword") String keyword, Pageable pageable );

	
	Page<LoanProduct>findByJenisNasabahLikeOrRegularPilotingLikeAllIgnoreCaseOrderByUpdateDateDesc(String jenisNasabah, String regularPiloting,
			 Pageable pageable);
	
	// get LoanProduct by LoanProduct Id, return loan
	LoanProduct findByProductId(Long productId);

	
}
