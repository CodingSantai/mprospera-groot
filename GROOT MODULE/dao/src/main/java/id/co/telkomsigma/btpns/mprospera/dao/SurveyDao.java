package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.survey.Survey;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;

public interface SurveyDao extends JpaRepository<Survey, Long> {

    @Query("SELECT s FROM Survey s INNER JOIN s.customer c "
            + "INNER JOIN s.customer.group g "
            + "INNER JOIN s.customer.group.sentra st "
            + ", Location l WHERE st.locationId = l.locationId AND s.createdDate>=:startDate AND s.createdDate<:endDate AND l.locationId = :officeId ORDER BY s.surveyId")
    Page<Survey> findByCreatedDate(@Param("startDate") Date startDate, @Param("endDate") Date endDate,
                                   @Param("officeId") String officeId, Pageable pageRequest);

}