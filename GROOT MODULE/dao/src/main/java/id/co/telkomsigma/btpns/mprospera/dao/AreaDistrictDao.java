package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.sda.AreaDistrict;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AreaDistrictDao extends JpaRepository<AreaDistrict, String> {

	List<AreaDistrict> findByAreaNameLikeIgnoreCase(String kabupaten);

}