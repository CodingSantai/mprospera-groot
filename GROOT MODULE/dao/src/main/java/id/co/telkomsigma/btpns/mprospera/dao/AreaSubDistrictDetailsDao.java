package id.co.telkomsigma.btpns.mprospera.dao;

import id.co.telkomsigma.btpns.mprospera.model.sda.AreaSubDistrict;
import id.co.telkomsigma.btpns.mprospera.model.sda.AreaSubDistrictDetails;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface AreaSubDistrictDetailsDao extends JpaRepository<AreaSubDistrictDetails, Long> {

	@Query("select e from AreaSubDistrictDetails e where e.areaSubDistrict in :subDistrictList and e.isDeleted=false")
	Page<AreaSubDistrictDetails> searchAllDetailsByParentId(
			@Param("subDistrictList") List<AreaSubDistrict> subDistrictList, Pageable pageable);

	@Query("SELECT e FROM AreaSubDistrictDetails e WHERE e.areaSubDistrict in :subDistrictList AND e.createdDate>=:startDate AND e.createdDate<:endDate and e.isDeleted=false")
	Page<AreaSubDistrictDetails> searchDetailsByParentIdAndDate(
			@Param("subDistrictList") List<AreaSubDistrict> subDistrictList, @Param("startDate") Date startDate,
			@Param("endDate") Date endDate, Pageable pageable);

}