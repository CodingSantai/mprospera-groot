package id.co.telkomsigma.btpns.mprospera.controller;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import id.co.telkomsigma.btpns.mprospera.service.AuditLogService;
import id.co.telkomsigma.btpns.mprospera.service.UploadService;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.user.User;

@Controller
public class ManageUploadJenisUsahaController extends GenericController {

	private static final String PAGE_NAME = "upload/uploadjenisusahacsv";

	@Autowired
	private UploadService uploadService;

	@Autowired
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;

	@Autowired
	private AuditLogService auditLogService;

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_UPLOAD_JENIS_USAHA_MAPPING, method = RequestMethod.GET)
	public String showListPage(@ModelAttribute("uploadForm") UploadForm uploadForm, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) {

		model.addAttribute("uploadForm", uploadForm);

		if (null != uploadService.getProgressMessage()) {
			model.addAttribute("okMessage", uploadService.getProgressMessage());
			model.addAttribute("enable", uploadService.isEnable());
		} else
			model.addAttribute("enable", true);

		if (uploadService.getErrorBar() != null) {
			model.addAttribute("errorMessage", uploadService.getErrorBar());
		}
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		
				AuditLog auditLog = new AuditLog();
				try {
					auditLog.setActivityType(auditLog.VIEW_PAGE);
					auditLog.setCreatedBy(user.getUsername());
					auditLog.setCreatedDate(new Date());
					auditLog.setDescription("Akses Antarmuka Upload File Jenis Usaha");
					auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));
					
					auditLogService.insertAuditLog(auditLog);
				}catch(Exception e) {
					log.info(e.getMessage(), e);
					e.printStackTrace();
				}
			log.debug("Added Audit Log");
			
		return getPageContent(PAGE_NAME);
	}
	
	@RequestMapping(value= WebGuiConstant.AFTER_LOGIN_PATH_UPLOAD_JENIS_USAHA_MAPPING, method= {RequestMethod.POST})
	public String doUpload(@ModelAttribute("uploadForm") UploadForm uploadForm, BindingResult errors, final Model model,
								final HttpServletRequest request, HttpServletResponse response) {
		
		String errorMsg = null;
		if (uploadService.isEnable()) {
			try {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			final User user = (User) auth.getPrincipal();
			final List<String> data = new ArrayList<String>();
				String line = "";
			String extensi = getFileExtension(uploadForm.getName());
			if(extensi.equals("csv")) {
				BufferedReader buferReader = null;
				buferReader = new BufferedReader(new InputStreamReader(uploadForm.getFile().getInputStream(), "UTF-8"));
				while ((line = buferReader.readLine()) != null) {
					if (line.equals(""))
						continue;
					data.add(line);
				}

				if (data.size() == 0) {
					errorMsg = "Data is empty";
				} else {
					threadPoolTaskExecutor.execute(new Runnable() {

						@Override
						public void run() {
							try {
								log.info("run upload service jenis usaha");
								uploadService.insertDbJenisUsaha(data, user);
							} catch (Exception e) {
								log.error(e.getMessage());
								model.addAttribute("errorMessage", getMessage("form.error.upload.empty"));
							}
						}
						
					});
				}
			}else {
				errorMsg = "notCsv";
			}
				
			}catch(Exception e) {
				log.error(e.getMessage());
				model.addAttribute("errorMessage",getMessage("form.error.upload.empty"));
			}
		}else {
			errorMsg = "alreadyRun";
		}
		
		if (null == errorMsg) {
			model.addAttribute("okMessage", getMessage("form.progress.upload.start"));
			model.addAttribute("enable", false);
		}else if (errorMsg.equals("empty")){
			model.addAttribute("errorMessage", getMessage("form.error.upload.empty"));
			model.addAttribute("enable", true);
		}else if (errorMsg.equals("notCsv")) {
			model.addAttribute("errorMessage", getMessage("form.error.upload.unFormatted"));
			model.addAttribute("enable", true);
		}else if (errorMsg.equals("alreadyRun")) {
			model.addAttribute("errorMessage", getMessage("form.error.upload.alreadyRun"));
			model.addAttribute("enable", false);
		}if (null != uploadService.getProgressMessage()) {
			model.addAttribute("okMessage", uploadService.getProgressMessage());
		}
		return getPageContent(PAGE_NAME);
	}
	
	@RequestMapping(value=WebGuiConstant.AFTER_LOGIN_PATH_PROGRESS_JENIS_USAHA_OK, method = RequestMethod.GET)
	public void getOkMessage(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		if(null != uploadService.getProgressMessage()) {
			response.getWriter().write(uploadService.getProgressMessage());
		}else {
			response.getWriter().write("");
		}
	}
	
	@RequestMapping(value= WebGuiConstant.AFTER_LOGIN_PATH_PROGRESS_JENIS_USAHA_ERROR, method = RequestMethod.GET)
	public void getErrorMessage(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		if (null != uploadService.getErrorBar()) {
			response.getWriter().write(uploadService.getErrorBar());
		}else {
			response.getWriter().write("");
		}
	}
	
	public static String getFileExtension(String fullName) {
		String fileName = new File(fullName).getName();
		int dotIndex = fileName.lastIndexOf('.');
		return (dotIndex == -1) ? "" : fileName.substring(dotIndex + 1);
	}
}
