package id.co.telkomsigma.btpns.mprospera.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.HtmlUtils;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.JenisUsaha;
import id.co.telkomsigma.btpns.mprospera.pojo.DataTablesPojo;
import id.co.telkomsigma.btpns.mprospera.service.JenisUsahaService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;

@Controller("businessTypeController")
public class BusinessTypeController extends GenericController {

	@Autowired
	JenisUsahaService jenisUsahaService;

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_UPLOAD_JENIS_USAHA_LIST, method = RequestMethod.GET)
	public void businessTypeList(final HttpServletRequest request, final HttpServletResponse response)
			throws JsonMappingException, JsonGenerationException, IOException {
		if (isAjaxRequest(request)) {
			setResponseAsJson(response);

			DataTablesPojo dataTablesPojo = new DataTablesPojo();

			try {
				dataTablesPojo.setDraw(Integer.parseInt(request.getParameter("draw")));
			} catch (NumberFormatException e) {
				dataTablesPojo.setDraw(0);
			}

			final int orderableColumnCount = 5;
			final StringBuffer sbOrder = new StringBuffer();
			LinkedHashMap<String, String> columnMap = new LinkedHashMap<String, String>();
			columnMap.put("0", "identifier");
			columnMap.put("1", "tujuan_pembiayaan");
			columnMap.put("2", "kat_bidang_usaha");
			columnMap.put("3", "desc_usaha");
			columnMap.put("4", "kd_sector_usaha");
			columnMap.put("5", "sandi_baru_desc");

			int offset = 0;
			int limit = 50;

			try {
				offset = Integer.parseInt(request.getParameter("start"));
			} catch (NumberFormatException e) {
			}

			try {
				limit = Integer.parseInt(request.getParameter("length"));
			} catch (NumberFormatException e) {
			}

			try {
				Page<JenisUsaha> jenisUsahaListPojo = jenisUsahaService.listJenisUsaha(offset, limit);

				List<String[]> data = new ArrayList<>();
				for (JenisUsaha jenisUsaha : jenisUsahaListPojo) {
					String identifier = new String();
					if(jenisUsaha.getIdentifier()==null) {
						identifier = "";
					}else {
						identifier = jenisUsaha.getIdentifier().toString();
					}
					data.add(new String[] { 
							
							HtmlUtils.htmlEscape(identifier),
							HtmlUtils.htmlEscape(jenisUsaha.getKatBidangUsaha()),
							HtmlUtils.htmlEscape(jenisUsaha.getDescUsaha()),
							HtmlUtils.htmlEscape(jenisUsaha.getKdSectorEkonomi().toString()),
							HtmlUtils.htmlEscape(jenisUsaha.getSandiBaruDesc()),
							HtmlUtils.htmlEscape(jenisUsaha.getTujuanPembiayaan())
					});
				}
				dataTablesPojo.setData(data);
				dataTablesPojo.setRecordsFiltered(Integer.parseInt("" + jenisUsahaListPojo.getTotalElements()));
				dataTablesPojo.setRecordsTotal(Integer.parseInt("" + jenisUsahaListPojo.getTotalElements()));
			} catch (HttpClientErrorException e) {
				log.error(e.getStatusCode().toString(), e);
				dataTablesPojo.setError(getMessage("message.datatables.error.fetch.terminal"));
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				dataTablesPojo.setError(getMessage("message.datatables.error.fetch.terminal"));
			} finally {
				response.getWriter().write(new JsonUtils().toJson(dataTablesPojo));
			}
		} else {
			response.setStatus(HttpServletResponse.SC_FOUND);
		}
	}
}
