package id.co.telkomsigma.btpns.mprospera.controller;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.request.GenerateAp3rRequest;
import id.co.telkomsigma.btpns.mprospera.response.BaseResponse;
import id.co.telkomsigma.btpns.mprospera.service.Ap3rService;
import id.co.telkomsigma.btpns.mprospera.service.AuditLogService;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;


@Controller
public class Ap3rController extends GenericController {

    @Autowired
    private Ap3rService ap3rService;

    @Autowired
    private ThreadPoolTaskExecutor executor;
    
    @Autowired
    private AuditLogService auditLogService;	
    
    protected final Log LOGGER = LogFactory.getLog(getClass());

    @PostMapping(value = "/webservice/ap3r/generate", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<BaseResponse> generateAp3r(@RequestBody GenerateAp3rRequest ap3rRequest){
        BaseResponse response = new BaseResponse();
        try {
			String daysProperty = null==System.getProperty(WebGuiConstant.SYSTEM_PROPERTY_AP3R_DAYS)?"0":System.getProperty(WebGuiConstant.SYSTEM_PROPERTY_AP3R_DAYS).trim();

            executor.execute(new Runnable() {
                @Override
                public void run() {
                    try {
        				long start = System.currentTimeMillis();
        				LOGGER.info("AP3R Generate running.....START");
        				auditLogService.insertAuditLog(generateAuditLog(WebGuiConstant.GENERATE_AP3R+" Service ..START"));
        				ap3rService.setDays(new Integer(daysProperty).intValue());
        				ap3rService.generateAp3r(ap3rRequest.getStartDate(),ap3rRequest.getEndDate());
                        long duration = System.currentTimeMillis()-start;
        				LOGGER.info("AP3R Generate running.....FINISH ("+duration+" ms)");
        				auditLogService.insertAuditLog(generateAuditLog(WebGuiConstant.GENERATE_AP3R+" Service ..FINISH ("+duration+" ms)"));
                   } catch (Exception e) {
                        e.printStackTrace();
                   }
                }
            });

            response.setResponseCode(WebGuiConstant.RC_SUCCESS);
            response.setResponseMessage("SUCCESS");
        } catch (Exception e) {
            e.printStackTrace();
            response.setResponseCode("A9");
            response.setResponseMessage("Failed to generate report: "+ e.getMessage());
        }

        return ResponseEntity.ok(response);
    }
    
	private static AuditLog generateAuditLog(String description) {
		String reffNo = new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date());
		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(AuditLog.GENERATE_REPORT);
		auditLog.setCreatedBy("Mprospera");
		auditLog.setCreatedDate(new Date());
		auditLog.setDescription(description);
		auditLog.setReffNo(reffNo);
		return auditLog;
	}

}
