package id.co.telkomsigma.btpns.mprospera.controller;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.feign.*;
import id.co.telkomsigma.btpns.mprospera.manager.EODManager;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.service.AuditLogService;
import id.co.telkomsigma.btpns.mprospera.service.CacheService;
import id.co.telkomsigma.btpns.mprospera.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by daniel on 4/24/15.
 */
@Controller
public class ManageCacheController extends GenericController {

	@Autowired
	private AuditLogService auditLogService;

	@Autowired
	private EODManager eodManager;

	@Autowired
	private CacheService cacheService;

	@Autowired
	private WolverineInterface wolverineInterface;

	@Autowired
	private HawkeyeInterface hawkeyeInterface;

	@Autowired
	private IronmanInterface ironmanInterface;

	@Autowired
	private VisionInterface visionInterface;

	@Autowired
	private ProductServiceInterface productServiceInterface;

	@Autowired
	private PpiacqInterface ppiacqInterface;

	@Autowired
	private Acqv2Interface acqv2Interface;

	@Autowired
	private UserService userService;
	
	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_CACHE, method = RequestMethod.GET)
	public String clearAllCache(final Model model, final HttpServletRequest request, final HttpServletResponse response) {

		if (request.getParameter("clear") == null) {
			model.addAttribute("clear", "null");
			return getPageContent("cache");
		} else
			model.addAttribute("clear", request.getParameter("clear"));

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();

		AuditLog auditLogs = new AuditLog();
		auditLogs.setActivityType(auditLogs.VIEW_PAGE);
		auditLogs.setCreatedBy(user.getUsername());
		auditLogs.setCreatedDate(new Date());
		auditLogs.setDescription("Buka Halaman Kelola Cache");
		auditLogs.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));

		try {
			auditLogService.insertAuditLog(auditLogs);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String mode = request.getParameter("clear");

		AuditLog activity = new AuditLog();
		activity.setCreatedBy(user.getUsername());
		activity.setCreatedDate(new Date());
		activity.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));
		activity.setActivityType(activity.DELETE_CACHE);

		if (mode.equals("all")) {
			try{
				visionInterface.clearAllCache();
			}catch (Exception e){ }
			try {
				wolverineInterface.clearAllCache();
			}catch (Exception e){}
			try{
				hawkeyeInterface.clearAllCache();
			}catch (Exception e){}
			try {
				ironmanInterface.clearAllCache();
			}catch (Exception e){}
			try {
				productServiceInterface.clearAllCache();
			}catch (Exception e){}
			try {
				ppiacqInterface.clearAllCache();
			}catch (Exception e){}
			try {
				acqv2Interface.clearAllCache();
			}catch (Exception e){}
			try{
				cacheService.clearCache();
			}catch (Exception e){}
			try {
				auditLogService.insertAuditLog(activity);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (mode.equals("area")) {
			cacheService.clearAreaCache();
			activity.setDescription("Hapus cache area");
			try {
				auditLogService.insertAuditLog(activity);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (mode.equals("customer")) {
			cacheService.clearCustomerCache();
			activity.setDescription("Hapus cache nasabah");
			try {
				auditLogService.insertAuditLog(activity);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (mode.equals("loan")) {
			cacheService.clearLoanCache();
			activity.setDescription("Hapus cache pinjaman");
			try {
				auditLogService.insertAuditLog(activity);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (mode.equals("menu")) {
			cacheService.clearMenuCache();
			activity.setDescription("Hapus cache menu");
			try {
				auditLogService.insertAuditLog(activity);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (mode.equals("mm")) {
			cacheService.clearMMCache();
			activity.setDescription("Hapus cache MM");
			try {
				auditLogService.insertAuditLog(activity);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (mode.equals("param")) {
			cacheService.clearParamCache();
			activity.setDescription("Hapus cache param");
			try {
				auditLogService.insertAuditLog(activity);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (mode.equals("pdk")) {
			cacheService.clearPDKCache();
			activity.setDescription("Hapus cache PDK");
			try {
				auditLogService.insertAuditLog(activity);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (mode.equals("roles")) {
			cacheService.clearRoleCache();
			activity.setDescription("Hapus cache role");
			try {
				auditLogService.insertAuditLog(activity);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (mode.equals("sda")) {
			activity.setDescription("Hapus cache SDA");
			try {
				auditLogService.insertAuditLog(activity);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (mode.equals("sentra")) {
			cacheService.clearSentraCache();
			activity.setDescription("Hapus cache sentra");
			try {
				auditLogService.insertAuditLog(activity);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (mode.equals("sw")) {
			cacheService.clearSWCache();
			activity.setDescription("Hapus cache SW");
			try {
				auditLogService.insertAuditLog(activity);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (mode.equals("terminal")) {
			cacheService.clearTerminalCache();
			activity.setDescription("Hapus cache terminal");
			try {
				auditLogService.insertAuditLog(activity);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (mode.equals("user")) {
			cacheService.clearUserCache();
			activity.setDescription("Hapus cache user");
			try {
				auditLogService.insertAuditLog(activity);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (mode.equals("survey")) {
			cacheService.clearProsperaSurveyCache();
			activity.setDescription("Hapus cache survey");
			try {
				auditLogService.insertAuditLog(activity);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (mode.equals("apk")) {
			cacheService.clearApkCache();
			activity.setDescription("Hapus cache apk");
			try {
				auditLogService.insertAuditLog(activity);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (mode.equals("prs")) {
			activity.setDescription("Hapus cache PRS");
			try {
				auditLogService.insertAuditLog(activity);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else if (mode.equals("saving")) {
			activity.setDescription("Hapus cache Saving");
			try {
				auditLogService.insertAuditLog(activity);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return getPageContent("cache");
	}

	@RequestMapping(value = WebGuiConstant.PATH_CACHE_CLEAR_ALL, method = RequestMethod.GET)
	public String clearAll(final Model model, final HttpServletRequest request, final HttpServletResponse response) {
		return clearAllCache(model, request, response);
	}

	
	@RequestMapping(value = WebGuiConstant.PATH_CACHE_CLEAR_SESSION_KEY, method = { RequestMethod.GET })
	public @ResponseBody String doClear(@RequestParam(value = "username", required = true) final String username,			 
	final HttpServletRequest request) {
		try {
			
			User usernameToClear = (User) userService.loadUserByUsername(username);
				try {
					userService.logout(usernameToClear);
		
				} catch (HttpClientErrorException e) {
					log.error(e.getStatusCode().toString(), e);
					
				} catch (Exception e) {
					log.error(e.getMessage(), e);				
				}
				
		} catch (UsernameNotFoundException e) {
			log.error(e.getMessage(), e);			
		}
		
		return WebGuiConstant.RC_SUCCESS;
	}
}