package id.co.telkomsigma.btpns.mprospera.controller;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.parameter.SystemParameter;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.pojo.DataTablesPojo;
import id.co.telkomsigma.btpns.mprospera.service.AuditLogService;
import id.co.telkomsigma.btpns.mprospera.service.ParameterService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.html.HTML;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;

/**
 * Created by daniel on 4/24/15.
 */
@Controller
public class ManageParamController extends GenericController {
	// ----------------------------------------- LIST PARAMETER SECTION
	// ----------------------------------------- //
	private static final String LIST_PAGE_NAME = "manage/parameter/list";
	private static final String LIST_COMMAND = "redirect:" + WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_PARAM;

	@Autowired
	private ParameterService paramService;

	@Autowired
	private AuditLogService auditLogService;

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_PARAM_MAPPING, method = RequestMethod.GET)
	public String showListPage(final Model model, final HttpServletRequest request, final HttpServletResponse response) {
		List<SystemParameter> params = paramService.getParameter();
		List<String> paramGroup = new ArrayList<String>();

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.VIEW_PAGE);
		auditLog.setCreatedBy(user.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setDescription("Buka Halaman Kelola Parameter");
		auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));

		try {
			auditLogService.insertAuditLog(auditLog);
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.debug("Added Audit Log");

		Iterator iterator = params.iterator();
		while (iterator.hasNext()) {
			SystemParameter systemParameter = (SystemParameter) iterator.next();
			paramGroup.add(systemParameter.getParamGroup());
		}

		Set<String> grupParam = new HashSet<String>(paramGroup);

		model.addAttribute("params", params);
		model.addAttribute("paramGroup", grupParam);
		return getPageContent(LIST_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_PARAM_GET_LIST_MAPPING, method = RequestMethod.GET)
	public void doSearch(final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws IOException {
		if (isAjaxRequest(request)) {
			setResponseAsJson(response);

			DataTablesPojo dataTablesPojo = new DataTablesPojo();
			try {
				dataTablesPojo.setDraw(Integer.parseInt(request.getParameter("draw")));
			} catch (NumberFormatException e) {
				dataTablesPojo.setDraw(0);
			}

			final int orderableColumnCount = 4;
			final StringBuffer sbOrder = new StringBuffer();
			LinkedHashMap<String, String> columnMap = new LinkedHashMap<String, String>();
			columnMap.put("0", "paramGroup");
			columnMap.put("1", "paramName");
			columnMap.put("2", "paramDescription");
			columnMap.put("3", "paramValue");

			int offset = 0;
			int limit = 50;
			try {
				offset = Integer.parseInt(request.getParameter("start"));
			} catch (NumberFormatException e) {
			}
			try {
				limit = Integer.parseInt(request.getParameter("length"));
			} catch (NumberFormatException e) {
			}

			for (int i = 0; i < orderableColumnCount; i++) {
				String orderColumn = request.getParameter("order[" + i + "][column]");
				if (orderColumn == null || orderColumn.equalsIgnoreCase("")) {
					break;
				} else {
					orderColumn = columnMap.get(orderColumn);
					if (orderColumn == null) {
						break;
					}
					String orderDir = request.getParameter("order[" + i + "][dir]");
					if (orderDir == null || !orderDir.equalsIgnoreCase("desc")) {
						orderDir = "asc";
					}
					sbOrder.append(orderColumn);
					sbOrder.append("-");
					sbOrder.append(orderDir);
					sbOrder.append("+");
				}
			}

			final String keyword = request.getParameter("q");
			final String paramGroup = request.getParameter("a");

			try {
				final int finalOffset = offset;
				final int finalLimit = limit;

				Page<SystemParameter> paramListPojo = paramService.searchParam(keyword, paramGroup, columnMap,
						finalOffset, finalLimit);

				List<String[]> data = new ArrayList<>();

				for (SystemParameter param : paramListPojo) {

					String url = "";

					url = url
							+ "<a href=\"#\" data-toggle=\"modal\" data-target=\"#buttonModal\" data-event=\"edit\" data-value=\""
							+ HtmlUtils.htmlEscape(param.getParamValue())
							+ "\" data-username=\""
							+ HtmlUtils.htmlEscape(param.getParamName())
							+ "\" data-group=\""
							+ HtmlUtils.htmlEscape(param.getParamGroup())
							+ "\" data-description=\""
							+ HtmlUtils.htmlEscape(param.getParamDescription())
							+ "\" data-link=\""
							+ getServletContext().getContextPath()
							+ WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_PARAM_EDIT_REQUEST.replace("{paramId}",
									"" + param.getParameterId()) + "\" data-id=\"" + param.getParameterId()
							+ "\"><i class='fa fa-edit'><div class='action' id='action-edit'>edit</div></i></a>";
					data.add(new String[] { HtmlUtils.htmlEscape(param.getParamGroup()), HtmlUtils.htmlEscape(param.getParamName()), HtmlUtils.htmlEscape(param.getParamDescription()),
							HtmlUtils.htmlEscape(param.getParamValue()), url });

				}

				dataTablesPojo.setData(data);
				dataTablesPojo.setRecordsFiltered(Integer.parseInt("" + paramListPojo.getTotalElements()));
				dataTablesPojo.setRecordsTotal(Integer.parseInt("" + paramListPojo.getTotalElements()));

			} catch (HttpClientErrorException e) {
				log.error(e.getStatusCode().toString(), e);
				dataTablesPojo.setError(getMessage("message.datatables.error.fetch.user"));
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				dataTablesPojo.setError(getMessage("message.datatables.error.fetch.user"));
			} finally {
				response.getWriter().write(new JsonUtils().toJson(dataTablesPojo));

			}
		} else {
			response.setStatus(HttpServletResponse.SC_FOUND);
		}
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_PARAM_EDIT_MAPPING, method = { RequestMethod.POST })
	public String editValue(@RequestParam(value = "paramId", required = true) final Long paramId,
			@RequestParam(value = "paramValue", required = false) final String paramValue,
			@ModelAttribute("params") SystemParameter param, BindingResult errors, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) {

		try {

			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = (User) auth.getPrincipal();

			AuditLog auditLog = new AuditLog();
			auditLog.setActivityType(auditLog.SAVE_OR_UPDATE);
			auditLog.setCreatedBy(user.getUsername());
			auditLog.setCreatedDate(new Date());
			auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));

			try {
				SystemParameter editParam = paramService.loadParamByParamId(paramId);

				if (!errors.hasErrors()) {
					try {

						paramService.updateParam(editParam, HtmlUtils.htmlEscape(paramValue));

						List<SystemParameter> params = paramService.getParameter();
						List<String> paramGroup = new ArrayList<String>();

						Iterator iterator = params.iterator();
						while (iterator.hasNext()) {
							SystemParameter systemParameter = (SystemParameter) iterator.next();
							paramGroup.add(HtmlUtils.htmlEscape(systemParameter.getParamGroup()));
						}

						Set<String> grupParam = new HashSet<String>(paramGroup);

						model.addAttribute("paramGroup", grupParam);
						model.addAttribute("okMessage", getMessage("form.ok.edit.param"));

						auditLog.setDescription("Ubah Parameter " + HtmlUtils.htmlEscape(editParam.getParamName()) + " Berhasil");
						try {
							auditLogService.insertAuditLog(auditLog);
						} catch (Exception e) {
							e.printStackTrace();
						}

					} catch (HttpClientErrorException e) {
						log.error(e.getStatusCode().toString(), e);
						auditLog.setDescription("Ubah Parameter " + HtmlUtils.htmlEscape(editParam.getParamName()) + " Gagal");
						try {
							auditLogService.insertAuditLog(auditLog);
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					} catch (Exception e) {
						log.error(e.getMessage(), e);
						model.addAttribute("errorMessage",
								getMessage("field.error.param.not.found", HtmlUtils.htmlEscape(param.getParamName())));
						auditLog.setDescription("Ubah Parameter " + HtmlUtils.htmlEscape(editParam.getParamName()) + " Gagal");
						try {
							auditLogService.insertAuditLog(auditLog);
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}
				}
			} catch (HttpClientErrorException e) {
				log.error(e.getStatusCode().toString(), e);
				auditLog.setDescription("Ubah Parameter " + HtmlUtils.htmlEscape(param.getParamName()) + " Gagal");
				try {
					auditLogService.insertAuditLog(auditLog);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				model.addAttribute("errorMessage", getMessage("field.error.param.not.found", HtmlUtils.htmlEscape(param.getParamName())));
				auditLog.setDescription("Ubah Parameter " + HtmlUtils.htmlEscape(param.getParamName()) + " Gagal");
				try {
					auditLogService.insertAuditLog(auditLog);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}

		} catch (UsernameNotFoundException e) {
			log.error(e.getMessage(), e);
			model.addAttribute("errorMessage", getMessage("form.error.edit.param"));
		}
		return getPageContent(LIST_PAGE_NAME);
	}
}