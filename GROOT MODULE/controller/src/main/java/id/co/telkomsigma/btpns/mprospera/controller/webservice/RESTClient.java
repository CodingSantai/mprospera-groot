package id.co.telkomsigma.btpns.mprospera.controller.webservice;

import id.co.telkomsigma.btpns.mprospera.request.ProsperaLoginRequest;
import id.co.telkomsigma.btpns.mprospera.response.LDAPLoginResponse;
import id.co.telkomsigma.btpns.mprospera.response.ProsperaLoginResponse;
import id.co.telkomsigma.btpns.mprospera.service.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import javax.net.ssl.*;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

@Component("restClient")
public class RESTClient {

	protected final Log LOGGER = LogFactory.getLog(getClass());

	private String HOST;
	private String PORT;
	private String MAIN_URI;

	@Autowired
	private SentraService sentraService;

	@Autowired
	private CustomerService customerService;

	@Autowired
	private LoanService loanService;

	@Autowired
	private SafService safService;

	@Autowired
	ParameterService parameterService;

	@PostConstruct
	protected void init() {
		HOST = parameterService.loadParamByParamName("esb.host", "https://10.7.17.163");
		PORT = parameterService.loadParamByParamName("esb.port", "8443");
		MAIN_URI = parameterService.loadParamByParamName("esb.uri", "/btpns/mprospera");
	}

	public Boolean echoSuccess() {
		String url = HOST + ":" + PORT + MAIN_URI + "/echo";
		RestTemplate restTemplate = getRestTemplate();
		try {
			LOGGER.info("SEND ECHO TO ESB...");
			ResponseEntity<String> response = restTemplate.postForEntity(url, null, String.class);
			LOGGER.info("ECHO SUCCESS...");
			return response.getStatusCode().is2xxSuccessful();
		} catch (HttpClientErrorException e) {
			HttpStatus httpStatus = e.getStatusCode();
			LOGGER.error("ECHO FAILED..., " + httpStatus.toString() + " : " + httpStatus.getReasonPhrase());
			return e.getStatusCode().is2xxSuccessful();
		} catch (Exception e) {
			LOGGER.error("ECHO FAILED..., " + e.getMessage());
			return false;
		}
	}

	public ProsperaLoginResponse login(ProsperaLoginRequest request)
			throws NoSuchAlgorithmException, KeyManagementException {

		if (echoSuccess()) {
			String url = HOST + ":" + PORT + MAIN_URI + "/login";
			LOGGER.info("POST URL : " + url);
			try {
				ProsperaLoginResponse response = getRestTemplate().postForObject(url, request, ProsperaLoginResponse.class);
				LOGGER.info("Got response..., " + response.toString());
				return response;
			} catch (HttpClientErrorException e) {
				HttpStatus httpStatus = e.getStatusCode();
				ProsperaLoginResponse response = new ProsperaLoginResponse();
				response.setResponseCode("XX");
				response.setResponseMessage("ESB " + httpStatus.toString() + " - " + httpStatus.getReasonPhrase());
				return response;
			}
		} else {
			ProsperaLoginResponse response = new ProsperaLoginResponse();
			response.setResponseCode("SU");
			response.setResponseMessage("ESB WEBSERVICE UNAVAILABLE");
			return response;
		}
	}

	protected RestTemplate getRestTemplate() {
		// Create a trust manager that does not validate certificate chains
		TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
			public java.security.cert.X509Certificate[] getAcceptedIssuers() {
				return null;
			}

			@Override
			public void checkClientTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
					throws CertificateException {
			}

			@Override
			public void checkServerTrusted(java.security.cert.X509Certificate[] arg0, String arg1)
					throws CertificateException {
			}
		} };

		// Install the all-trusting trust manager
		SSLContext sc = null;
		try {
			sc = SSLContext.getInstance("SSL");
		} catch (NoSuchAlgorithmException e1) {
			LOGGER.debug("SSL INSTANCE FETCHING FAILED..., " + e1.getMessage());
		}
		try {
			if(sc != null){
				sc.init(null, trustAllCerts, new java.security.SecureRandom());
				HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
			}
		} catch (KeyManagementException e) {
			LOGGER.debug("SSL CONTEXT INITIALIZING FAILED..., " + e.getMessage());
		}

		// Create all-trusting host name verifier
		final HostnameVerifier allHostsValid = new HostnameVerifier() {
			public boolean verify(String hostname, SSLSession session) {
				return true;
			}
		};

		// Install the all-trusting host verifier
		HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
		RestTemplate restTemplate = new RestTemplate(new SimpleClientHttpRequestFactory() {
			@Override
			protected void prepareConnection(HttpURLConnection connection, String httpMethod) throws IOException {
				if (connection instanceof HttpsURLConnection) {
					((HttpsURLConnection) connection).setHostnameVerifier(allHostsValid);
				}
				super.prepareConnection(connection, httpMethod);
			}
		});

		return restTemplate;
	}

	public boolean ldapLogin(String username, String password) {
		if (echoSuccess()) {
			String url = HOST + ":" + PORT + "/btpns/ws/rest/userauth?u=" + username + "&p=" + password + "&t=plain";
			LOGGER.debug("POST URL : " + HOST + ":" + PORT + "/btpns/ws/rest/userauth?u=" + username);
			try {
				LDAPLoginResponse response = getRestTemplate().postForObject(url, null, LDAPLoginResponse.class);
				LOGGER.debug("Got response..., " + response.toString());
				if ("1".equals(response.getUserauth().getSuccess())) {
					return true;
				} else {
					return false;
				}
			} catch (HttpClientErrorException e) {
				LOGGER.error("POST REQUEST ERROR..., " + e.getMessage());
			} catch (Exception e) {
				LOGGER.error("POST REQUEST ERROR..., " + e.getMessage());
			}
		} else {
			LOGGER.error("POST REQUEST ERROR..., ECHO FAILED...");
			return false;
		}
		return false;
	}
}
