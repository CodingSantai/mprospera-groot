package id.co.telkomsigma.btpns.mprospera.controller;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.service.AuditLogService;
import id.co.telkomsigma.btpns.mprospera.service.UploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
public class ManageUploadHolidayController extends GenericController {

	private static final String UPLOAD_PAGE_NAME = "upload/uploadholidaycsv";

	@Autowired
	private UploadService uploadService;

	@Autowired
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;

	@Autowired
	private AuditLogService auditLogService;

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_UPLOAD_HOLIDAY_MAPPING, method = RequestMethod.GET)
	public String showListPage(@ModelAttribute("uploadForm") UploadForm uploadForm, final Model model,
                               final HttpServletRequest request, final HttpServletResponse response) {
		model.addAttribute("uploadForm", uploadForm);
		if (null != uploadService.getProgressMessage()) {
			model.addAttribute("okMessage", uploadService.getProgressMessage());
			model.addAttribute("enable", uploadService.isEnable());
		} else
			model.addAttribute("enable", true);

		if (uploadService.getErrorBar() != null) {
			model.addAttribute("errorMessage", uploadService.getErrorBar());
		}

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.VIEW_PAGE);
		auditLog.setCreatedBy(user.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setDescription("Buka Halaman Upload file Libur Lebaran");
		auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));

		try {
			auditLogService.insertAuditLog(auditLog);
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.debug("Added Audit Log");

		return getPageContent(UPLOAD_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_UPLOAD_HOLIDAY_MAPPING, method = { RequestMethod.POST })
	public String doUpload(@ModelAttribute("uploadForm") final UploadForm uploadForm, BindingResult errors,
                           final Model model, final HttpServletRequest request, final HttpServletResponse response) {

		String errorMsg = null;
		if (uploadService.isEnable()) {

			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			final User admin = (User) auth.getPrincipal();

			try {

				final List<String> data = new ArrayList<String>();
				BufferedReader br = null;
				String line = "";

				br = new BufferedReader(new InputStreamReader(uploadForm.getFile().getInputStream(), "UTF-8"));
				while ((line = br.readLine()) != null) {
					if (line.equals(""))
						continue;
					data.add(line);
				}

				if (data.size() == 0) {
					errorMsg = "empty";
				} else {
						// panggil Thread
						threadPoolTaskExecutor.execute(new Runnable() {

							@Override
							public void run() {
								try {
									uploadService.insertDbHoliday(data, admin);
								} catch (Exception e) {
									e.printStackTrace();
									model.addAttribute("errorMessage", getMessage("form.error.upload.empty"));
								}
							}
						});
				}
			} catch (Exception e) {
				e.printStackTrace();
				model.addAttribute("errorMessage", getMessage("form.error.upload.empty"));
			}

		} else
			errorMsg = "alreadyRun";

		if (null == errorMsg) {
			model.addAttribute("okMessage", getMessage("form.progress.upload.start"));
			model.addAttribute("enable", false);
		} else if (errorMsg.equals("empty")) {
			model.addAttribute("errorMessage", getMessage("form.error.upload.empty"));
			model.addAttribute("enable", true);
		} else if (errorMsg.equals("notCsv")) {
			model.addAttribute("errorMessage", getMessage("form.error.upload.unFormatted"));
			model.addAttribute("enable", true);
		} else if (errorMsg.equals("alreadyRun")) {
			model.addAttribute("errorMessage", getMessage("form.error.upload.alreadyRun"));
			model.addAttribute("enable", false);
		}

		if (null != uploadService.getProgressMessage()) {
			model.addAttribute("okMessage", uploadService.getProgressMessage());
		}

		return getPageContent(UPLOAD_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_PROGRESS_HOLIDAY_OK, method = RequestMethod.GET)
	public void getOkMessage(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		if (null != uploadService.getProgressMessage()) {
			response.getWriter().write(uploadService.getProgressMessage());
		} else {
			response.getWriter().write("");
		}
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_PROGRESS_HOLIDAY_ERROR, method = RequestMethod.GET)
	public void getErrorMessage(final HttpServletRequest request, final HttpServletResponse response)
			throws IOException {
		if (null != uploadService.getErrorBar()) {
			response.getWriter().write(uploadService.getErrorBar());
		} else {
			response.getWriter().write("");
		}
	}
}
