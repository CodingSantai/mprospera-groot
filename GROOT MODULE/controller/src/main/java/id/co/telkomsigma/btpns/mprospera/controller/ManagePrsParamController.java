package id.co.telkomsigma.btpns.mprospera.controller;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.parameter.PRSParameter;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.pojo.DataTablesPojo;
import id.co.telkomsigma.btpns.mprospera.request.AddPRSParamRequest;
import id.co.telkomsigma.btpns.mprospera.service.AuditLogService;
import id.co.telkomsigma.btpns.mprospera.service.ParameterService;
import id.co.telkomsigma.btpns.mprospera.util.FormValidationUtil;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.html.HTML;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Controller
public class ManagePrsParamController extends GenericController {
	// ----------------------------------------- LIST PARAMETER SECTION
	// ----------------------------------------- //
	private static final String LIST_PAGE_NAME = "manage/prsparam/list";
	private static final String LIST_COMMAND = "redirect:" + WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_PRS_PARAM;

	@Autowired
	private ParameterService paramService;

	@Autowired
	private AuditLogService auditLogService;

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_MAPPING, method = RequestMethod.GET)
	public String showListPage(final Model model, final HttpServletRequest request, final HttpServletResponse response) {
		List<PRSParameter> params = paramService.getPRSParameter();
		List<String> paramGroup = new ArrayList<String>();

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.VIEW_PAGE);
		auditLog.setCreatedBy(user.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setDescription("Buka Halaman Kelola Parameter PRS");
		auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));

		try {
			auditLogService.insertAuditLog(auditLog);
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.debug("Added Audit Log");

		Iterator iterator = params.iterator();
		while (iterator.hasNext()) {
			PRSParameter prsParameter = (PRSParameter) iterator.next();
			if(prsParameter.getParamGroup().equals("prayer_promise")){
				prsParameter.setParamGroup("Doa dan Janji");
			}else if(prsParameter.getParamGroup().equals("people_meet")){
				prsParameter.setParamGroup("Orang yang ditemui");
			}else if(prsParameter.getParamGroup().equals("outstanding_reason")){
				prsParameter.setParamGroup("Alasan menunggak");
			}else if(prsParameter.getParamGroup().equals("not_attend_reason")){
				prsParameter.setParamGroup("Alasan tidak hadir");
			}else if(prsParameter.getParamGroup().equals("early_termination_reason")){
				prsParameter.setParamGroup("Alasan pelunasan dipercepat");
			}else if(prsParameter.getParamGroup().equals("daya_file")){
				prsParameter.setParamGroup("File Daya");
			}
			paramGroup.add(prsParameter.getParamGroup());
		}

		Set<String> grupParam = new HashSet<String>(paramGroup);

		model.addAttribute("prsparam", params);
		model.addAttribute("paramGroup", grupParam);
		return getPageContent(LIST_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_GET_LIST_MAPPING, method = RequestMethod.GET)
	public void doSearch(final Model model, final HttpServletRequest request, final HttpServletResponse response)
			throws IOException {
		if (isAjaxRequest(request)) {
			setResponseAsJson(response);

			DataTablesPojo dataTablesPojo = new DataTablesPojo();
			try {
				dataTablesPojo.setDraw(Integer.parseInt(request.getParameter("draw")));
			} catch (NumberFormatException e) {
				dataTablesPojo.setDraw(0);
			}

			final int orderableColumnCount = 5;
			final StringBuffer sbOrder = new StringBuffer();
			LinkedHashMap<String, String> columnMap = new LinkedHashMap<String, String>();
			columnMap.put("0", "paramGroup");
			columnMap.put("1", "paramName");
			columnMap.put("2", "paramDescription");
			columnMap.put("3", "paramType");
			columnMap.put("4", "paramValue");

			int offset = 0;
			int limit = 50;
			try {
				offset = Integer.parseInt(request.getParameter("start"));
			} catch (NumberFormatException e) {
			}
			try {
				limit = Integer.parseInt(request.getParameter("length"));
			} catch (NumberFormatException e) {
			}

			for (int i = 0; i < orderableColumnCount; i++) {
				String orderColumn = request.getParameter("order[" + i + "][column]");
				if (orderColumn == null || orderColumn.equalsIgnoreCase("")) {
					break;
				} else {
					orderColumn = columnMap.get(orderColumn);
					if (orderColumn == null) {
						break;
					}
					String orderDir = request.getParameter("order[" + i + "][dir]");
					if (orderDir == null || !orderDir.equalsIgnoreCase("desc")) {
						orderDir = "asc";
					}
					sbOrder.append(orderColumn);
					sbOrder.append("-");
					sbOrder.append(orderDir);
					sbOrder.append("+");
				}
			}

			final String keyword = request.getParameter("q");
			String paramGroup = request.getParameter("a");

			try {
				final int finalOffset = offset;
				final int finalLimit = limit;

				if(paramGroup != null){
					if(paramGroup.equals("Doa dan Janji")){
						paramGroup = "prayer_promise";
					}else if(paramGroup.equals("Orang yang ditemui")){
						paramGroup = "people_meet";
					}else if(paramGroup.equals("Alasan menunggak")){
						paramGroup = "outstanding_reason";
					}else if(paramGroup.equals("Alasan tidak hadir")){
						paramGroup = "not_attend_reason";
					}else if(paramGroup.equals("Alasan pelunasan dipercepat")){
						paramGroup = "early_termination_reason";
					}else if(paramGroup.equals("File Daya")){
						paramGroup = "daya_file";
					}
				}
				log.trace("PARAM GROUP : "+paramGroup);
				Page<PRSParameter> paramListPojo = paramService.searchPrsParam(keyword, paramGroup, columnMap,
						finalOffset, finalLimit);

				List<String[]> data = new ArrayList<>();

				for (PRSParameter param : paramListPojo) {

					log.trace("PARAM NAME : "+param.getParamValueString());
					String url = "";

					url = url
							+ "<a href=\""
							+ getServletContext().getContextPath()
							+ WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_ADD_REQUEST.replace("{parameterId}", ""
									+ param.getParameterId())
							+ "\"><i class='fa fa-edit'><div class='action' id='action-edit'>edit</div></i></a> ";

					url = url
							+ "<a href=\"#\" data-toggle=\"modal\" data-target=\"#buttonModal\" data-event=\"delete\" data-username=\""
							+ HtmlUtils.htmlEscape(param.getParamName())
							+ "\" data-link=\""
							+ getServletContext().getContextPath()
							+ WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_DELETE_REQUEST.replace("{parameterId}",
									"" + param.getParameterId())
							+ "\" data-id=\""
							+ param.getParameterId()
							+ "\"><i class='fa fa-trash-o'> <div class='action' id='action-delete'>delete</div></i></a>  ";
					if(param.getParamGroup().equals("prayer_promise")){
						param.setParamGroup("Doa dan Janji");
					}else if(param.getParamGroup().equals("people_meet")){
						param.setParamGroup("Orang yang ditemui");
					}else if(param.getParamGroup().equals("outstanding_reason")){
						param.setParamGroup("Alasan menunggak");
					}else if(param.getParamGroup().equals("not_attend_reason")){
						param.setParamGroup("Alasan tidak hadir");
					}else if(param.getParamGroup().equals("early_termination_reason")){
						param.setParamGroup("Alasan pelunasan dipercepat");
					}else if(param.getParamGroup().equals("daya_file")){
						param.setParamGroup("File Daya");
					}

					data.add(new String[] { HtmlUtils.htmlEscape(param.getParamGroup()), HtmlUtils.htmlEscape(param.getParamName()), HtmlUtils.htmlEscape(param.getParamDescription()),
							HtmlUtils.htmlEscape(param.getParamType()), HtmlUtils.htmlEscape(param.getParamValueString()), url });

				}

				dataTablesPojo.setData(data);
				dataTablesPojo.setRecordsFiltered(Integer.parseInt("" + paramListPojo.getTotalElements()));
				dataTablesPojo.setRecordsTotal(Integer.parseInt("" + paramListPojo.getTotalElements()));

			} catch (HttpClientErrorException e) {
				log.error(e.getStatusCode().toString(), e);
				dataTablesPojo.setError(getMessage("message.datatables.error.fetch.user"));
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				dataTablesPojo.setError(getMessage("message.datatables.error.fetch.user"));
			} finally {
				response.getWriter().write(new JsonUtils().toJson(dataTablesPojo));

			}
		} else {
			response.setStatus(HttpServletResponse.SC_FOUND);
		}
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_DELETE_MAPPING, method = { RequestMethod.GET })
	public String deleteValue(@RequestParam(value = "parameterId", required = true) final Long paramId,
			@ModelAttribute("params") AddPRSParamRequest param, BindingResult errors, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) {

		try {

			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User user = (User) auth.getPrincipal();

			AuditLog auditLog = new AuditLog();
			auditLog.setActivityType(auditLog.SAVE_OR_UPDATE);
			auditLog.setCreatedBy(user.getUsername());
			auditLog.setCreatedDate(new Date());
			auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));

			try {
				PRSParameter deleteParam = paramService.loadPrsParamByParamId(paramId);

				if (!errors.hasErrors()) {
					try {

						paramService.deletePrsParam(deleteParam.getParameterId());
						model.addAttribute("okMessage", getMessage("form.ok.delete.param"));

						auditLog.setDescription("Hapus Parameter " + HtmlUtils.htmlEscape(deleteParam.getParamName()) + " Berhasil");
						try {
							auditLogService.insertAuditLog(auditLog);
						} catch (Exception e) {
							e.printStackTrace();
						}

					} catch (HttpClientErrorException e) {
						log.error(e.getStatusCode().toString(), e);
						auditLog.setDescription("Hapus Parameter " + HtmlUtils.htmlEscape(deleteParam.getParamName()) + " Gagal");
						try {
							auditLogService.insertAuditLog(auditLog);
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					} catch (Exception e) {
						log.error(e.getMessage(), e);
						model.addAttribute("errorMessage",
								getMessage("field.error.param.not.found", HtmlUtils.htmlEscape(param.getParamName())));
						auditLog.setDescription("Hapus Parameter " + HtmlUtils.htmlEscape(deleteParam.getParamName()) + " Gagal");
						try {
							auditLogService.insertAuditLog(auditLog);
						} catch (Exception ex) {
							ex.printStackTrace();
						}
					}
				}
			} catch (HttpClientErrorException e) {
				log.error(e.getStatusCode().toString(), e);
				auditLog.setDescription("Hapus Parameter " + HtmlUtils.htmlEscape(param.getParamName()) + " Gagal");
				try {
					auditLogService.insertAuditLog(auditLog);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				model.addAttribute("errorMessage", getMessage("field.error.param.not.found", param.getParamName()));
				auditLog.setDescription("Hapus Parameter " + HtmlUtils.htmlEscape(param.getParamName()) + " Gagal");
				try {
					auditLogService.insertAuditLog(auditLog);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}

		} catch (UsernameNotFoundException e) {
			log.error(e.getMessage(), e);
			model.addAttribute("errorMessage", getMessage("form.error.delete.param"));
		}
		return getPageContent(LIST_PAGE_NAME);
	}

	private static final String ADD_PAGE_NAME = "manage/prsparam/add";

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_ADD_MAPPING, method = { RequestMethod.GET })
	public String showAddPage(@RequestParam(value = "parameterId", required = false) final Long paramId,
			final Model model, final HttpServletRequest request, final HttpServletResponse response) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User admin = (User) auth.getPrincipal();

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.VIEW_PAGE);
		auditLog.setCreatedBy(admin.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));

		if (paramId != null) {
			try {
				PRSParameter prsParam = paramService.loadPrsParamByParamId(paramId);
				prsParam.setParameterId(paramId);
				;
				model.addAttribute("prsparam", prsParam);
				auditLog.setDescription("Buka Halaman add PRS Parameter  " + HtmlUtils.htmlEscape(prsParam.getParamGroup()) + " berhasil");
				try {
					auditLogService.insertAuditLog(auditLog);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			} catch (HttpClientErrorException e) {
				log.error(e.getStatusCode().toString(), e);
				auditLog.setDescription("Buka Halaman add PRS Parameter gagal");
				try {
					auditLogService.insertAuditLog(auditLog);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				auditLog.setDescription("Buka Halaman edit PRS Parameter gagal");
				try {
					auditLogService.insertAuditLog(auditLog);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			model.addAttribute("prsparam", new PRSParameter());
			auditLog.setDescription("Buka Halaman Tambah PRS Parameter berhasil");
			try {
				auditLogService.insertAuditLog(auditLog);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return getPageContent(ADD_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_ADD_MAPPING, method = { RequestMethod.POST })
	public String doAdd(@RequestParam(value = "parameterId", required = false) final Long paramId,
			@ModelAttribute("prsparam") AddPRSParamRequest prsparam, BindingResult errors, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) {

		if (prsparam.getParamValueString() != null)
			FormValidationUtil.validatingParamValue(errors, "paramValueString", HtmlUtils.htmlEscape(prsparam.getParamValueString()),
					getMessage("label.param.value"), PRSParameter.MAX_LENGTH_PARAM_VALUE);

		if (!errors.hasErrors()) {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User admin = (User) auth.getPrincipal();

			String date = new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date());
			AuditLog auditLog = new AuditLog();
			auditLog.setActivityType(auditLog.SAVE_OR_UPDATE);
			auditLog.setCreatedBy(admin.getUsername());
			auditLog.setCreatedDate(new Date());
			auditLog.setReffNo(date);

			if (paramId == null) {
				PRSParameter prsParam = new PRSParameter();

				try {
					prsParam.setCreatedBy(admin.getUserId());
					prsParam.setCreatedDate(new Date());
					prsParam.setParamGroup(HtmlUtils.htmlEscape(prsparam.getParamGroup()));
					prsParam.setParamName(HtmlUtils.htmlEscape(prsparam.getParamName()));
					prsParam.setParamType(HtmlUtils.htmlEscape(prsparam.getParamType()));
					prsParam.setParamDescription(HtmlUtils.htmlEscape(prsparam.getParamDescription()));
					prsParam.setParamValueString(HtmlUtils.htmlEscape(prsparam.getParamValueString()));
					if (prsparam.getParamValueByte() != null)
						prsParam.setParamValueByte(Base64.encode(prsparam.getParamValueByte().getBytes()));
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				try {
					paramService.insertPrsParam(prsParam);
					auditLog.setDescription("Add prs parameter success");
					try {
						auditLogService.insertAuditLog(auditLog);
						log.debug("Added Audit Log");
					} catch (HttpClientErrorException e) {
						log.debug("Added Audit Log Failed");
					}
					model.addAttribute("prsparam", new PRSParameter());
					model.addAttribute("okMessage", getMessage("form.ok.add.prsparam"));
					return getPageContent(LIST_PAGE_NAME);
				} catch (HttpClientErrorException e) {
					log.error(e.getStatusCode().toString(), e);
					if (e.getStatusCode() == HttpStatus.CONFLICT) {

					} else {
						auditLog.setDescription("Add PRS Parameter Failed");
						try {
							auditLogService.insertAuditLog(auditLog);
							log.debug("Added Audit Log");
						} catch (HttpClientErrorException e2) {
							log.debug("Added Audit Log Failed");
						}
						model.addAttribute("errorMessage", getMessage("form.error.add.prsparam"));
					}
				} catch (Exception e) {
					log.error(e.getMessage(), e);
					auditLog.setDescription("Add PRS Parameter Failed");
					try {
						auditLogService.insertAuditLog(auditLog);
						log.debug("Added Audit Log");
					} catch (HttpClientErrorException e3) {
						log.debug("Added Audit Log Failed");
					}
					model.addAttribute("errorMessage", getMessage("form.error.add.prsparam"));
				}
			} else {
				try {
					PRSParameter fromDb = paramService.findByParameterId(paramId);
					if (prsparam.getParamValueByte() == null) {
						paramService.updatePRSParam(fromDb, prsparam.getParamValueString(), null,
								prsparam.getParamDescription());
					} else {
						paramService.updatePRSParam(fromDb, prsparam.getParamValueString(), prsparam
								.getParamValueByte().getBytes(), prsparam.getParamDescription());
					}
					model.addAttribute("prsparam", prsparam);
					auditLog.setDescription("Update PRS Parameter success");
					try {
						auditLogService.insertAuditLog(auditLog);
						log.debug("Added Audit Log");
					} catch (HttpClientErrorException e) {
						log.debug("Added Audit Log Failed");
					}
					model.addAttribute("okMessage", getMessage("form.ok.edit.prsparam"));
					return getPageContent(LIST_PAGE_NAME);
				} catch (HttpClientErrorException e) {
					log.error(e.getStatusCode().toString(), e);
					auditLog.setDescription("Update PRS Parameter Failed");
					try {
						auditLogService.insertAuditLog(auditLog);
						log.debug("Added Audit Log");
					} catch (HttpClientErrorException e1) {
						log.debug("Added Audit Log Failed");
					}
					model.addAttribute("errorMessage", getMessage("form.error.edit.prsparam"));
				} catch (Exception e) {
					// user.setEnabled(originalUser.isEnabled());
					auditLog.setDescription("Update Parameter PRS Failed");
					try {
						auditLogService.insertAuditLog(auditLog);
						log.debug("Added Audit Log");
					} catch (HttpClientErrorException e2) {
						log.debug("Added Audit Log Failed");
					}
					log.error(e.getMessage(), e);
					model.addAttribute("errorMessage", getMessage("form.error.edit.prsparam"));
				}
			}
		}

		return getPageContent(ADD_PAGE_NAME);
	}
}
