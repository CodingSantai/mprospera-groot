package id.co.telkomsigma.btpns.mprospera.authhandler;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.menu.Menu;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.user.Role;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.service.AuditLogService;
import id.co.telkomsigma.btpns.mprospera.service.MenuService;
import id.co.telkomsigma.btpns.mprospera.service.RoleService;
import id.co.telkomsigma.btpns.mprospera.service.UserService;
import id.co.telkomsigma.btpns.mprospera.util.UserUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Component("authenticationSuccessHandler")
public class AuthenticationSuccessHandler extends GenericController implements
		org.springframework.security.web.authentication.AuthenticationSuccessHandler {
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	@Autowired
	private RoleService roleService;

	@Autowired
	private MenuService menuService;

	@Autowired
	private AuditLogService auditLogService;

	@Autowired
	private UserService userServiceWithCache;

	@Autowired
	private SessionRegistry sessionRegistry;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth)
			throws IOException, ServletException {

		if (response.isCommitted()) {
			return;
		}
		Log log = LogFactory.getLog(this.getClass());
		User user = (User) auth.getPrincipal();
		Set<Role> rolesOwnedByUser = user.getRoles();
		List<Role> allRoles = roleService.getRoles();
		List<Menu> header = new ArrayList<Menu>();
		List<Menu> menu = new ArrayList<Menu>();
		Set<Long> checkHeader = new HashSet<>();
		Set<Long> checkMenu = new HashSet<>();
		boolean canAccessWeb = false;
		for (final Role role : rolesOwnedByUser) {
			for (Role role2 : allRoles) {
				if (role2.getAuthority().equals(role.getAuthority())) {
					List<Menu> menuByRole = menuService.getMenuByRole(role2.getRoleId());
					for (Menu menus : menuByRole) {
						if (menus.getMenuPath().contains("LABEL")) {
						} else {
							if (!checkMenu.contains(menus.getMenuId())) {
								menu.add(menus);
								checkMenu.add(menus.getMenuId());
								if (menus.getMenuParentId() != null) {
									Menu parentMenu = menuService.getMenuByMenuId(menus.getMenuParentId());
									if (!checkHeader.contains(parentMenu.getMenuId())) {
										header.add(parentMenu);
										checkHeader.add(parentMenu.getMenuId());
									}
								}
							}
						}
					}
				}
			}
			request.getSession(true).setAttribute("headers", header);
			request.getSession(true).setAttribute("menus", menu);
			request.getSession().setMaxInactiveInterval(5*60);
		}

		log.info("Logged in using " + user.getUserId() + " : " + user.getUsername() + " : "
				+ request.getSession().getId());
		String date = new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date());
		for (final Role role : rolesOwnedByUser) {
			if (role.getCanAccessWeb()) {
				canAccessWeb = true;
				break;
			}
		}
		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(1);
		auditLog.setCreatedBy(user.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setDescription("Login success");
		auditLog.setReffNo(date);
		auditLogService.insertAuditLog(auditLog);
		log.debug("Added Audit Log");
		if (!"0".equals(user.getCustomProp(WebGuiConstant.PARAMETER_USER_CURRENT_FAILED_ATTEMPT))) {
			user.setCustomProp(WebGuiConstant.PARAMETER_USER_CURRENT_FAILED_ATTEMPT, "0");
			user.getCustomProp(WebGuiConstant.PARAMETER_USER_CURRENT_FAILED_ATTEMPT);
			userServiceWithCache.updateUser(user);
		}
		List<String> grantedRoles = UserUtil.checkGrantedRoles(auth);
		if (UserUtil.isSystemUser(grantedRoles)) {
			// reject user system from login
			new SecurityContextLogoutHandler().logout(request, response, auth);
			redirectStrategy.sendRedirect(request, response, "/login?error=true");
		} else {
			if (canAccessWeb) {
				String currSessionId = request.getSession().getId();
				for (Object principal : sessionRegistry.getAllPrincipals()) {
					for (SessionInformation sessionInfo : sessionRegistry.getAllSessions(principal, true)) {
						User userData = (User) sessionInfo.getPrincipal();
						if (userData.getUsername().equals(user.getUsername())
								&& !currSessionId.equals(sessionInfo.getSessionId())) {
							log.info("Force Signout previous user login for user " + userData.getUsername()
									+ " of session ID " + sessionInfo.getSessionId());
							sessionInfo.expireNow();
						}
					}
				}
				redirectStrategy.sendRedirect(request, response, WebGuiConstant.AFTER_LOGIN_PATH_HOME_PAGE);
			} else {
				new SecurityContextLogoutHandler().logout(request, response, auth);
				redirectStrategy.sendRedirect(request, response, "/login?error=true");
			}
		}
	}
}