package id.co.telkomsigma.btpns.mprospera.controller;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.service.AuditLogService;
import id.co.telkomsigma.btpns.mprospera.service.UserService;
import id.co.telkomsigma.btpns.mprospera.util.FormValidationUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by daniel on 4/23/15.
 */
@Controller("userProfileController")
public class ProfileController extends GenericController {
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserService userServiceWithCache;

	@Autowired
	private AuditLogService auditLogService;

	private static final String PROFILE_PAGE_NAME = "profile";

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_PROFILE_PAGE, method = { RequestMethod.GET })
	public String showChangeProfilePage(final Model model, final HttpServletRequest request,
			final HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		model.addAttribute("user", (User) auth.getPrincipal());

		User user = (User) auth.getPrincipal();
		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.VIEW_PAGE);
		auditLog.setCreatedBy(user.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setDescription("Buka Halaman Profile");
		auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));

		try {
			auditLogService.insertAuditLog(auditLog);
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.debug("Added Audit Log");

		return getPageContent(PROFILE_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_PROFILE_PAGE, method = { RequestMethod.POST })
	public String doChangeProfile(@ModelAttribute("user") User user, BindingResult errors, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User originalUser = (User) auth.getPrincipal();
		user.setUserId(originalUser.getUserId());

		// validating name
		FormValidationUtil.validatingName(errors, "name", user.getName(), getMessage("label.name"),
				User.MAX_LENGTH_NAME);

		// validating address
		FormValidationUtil.validatingAddress(errors, "address", user.getAddress(), getMessage("label.address"),
				User.MAX_LENGTH_ADDRESS);

		// validating city
		FormValidationUtil.validatingCity(errors, "city", user.getCity(), getMessage("label.city"),
				User.MAX_LENGTH_CITY);

		// validating province
		FormValidationUtil.validatingProvince(errors, "province", user.getProvince(), getMessage("label.province"),
				User.MAX_LENGTH_PROVINCE);

		// validating phone mobile
		FormValidationUtil.validatingPhoneMobile(errors, "phoneMobile", user.getPhoneMobile(),
				getMessage("label.phone.mobile"), User.MAX_LENGTH_PHONE_MOBILE, User.MIN_LENGTH_PHONE_MOBILE);

		// validating phone
		FormValidationUtil.validatingPhone(errors, "phone", user.getPhone(), getMessage("label.phone.home"),
				User.MAX_LENGTH_PHONE, User.MIN_LENGTH_PHONE);

		user.setUsername(originalUser.getUsername());
		user.setEmail(originalUser.getEmail());
		user.setOfficeCode(originalUser.getOfficeCode());

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.SAVE_OR_UPDATE);
		auditLog.setCreatedBy(user.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));

		if (!errors.hasErrors()) {
			userServiceWithCache.updateProfile(user);
			originalUser.setName(user.getName());
			originalUser.setAddress(user.getAddress());
			originalUser.setCity(user.getCity());
			originalUser.setProvince(user.getProvince());
			originalUser.setPhoneMobile(user.getPhoneMobile());
			originalUser.setPhone(user.getPhone());

			Authentication authentication = new UsernamePasswordAuthenticationToken(originalUser,
					originalUser.getPassword(), originalUser.getAuthorities());
			SecurityContextHolder.getContext().setAuthentication(authentication);

			model.addAttribute("okMessage", getMessage("form.ok.change.profile"));
			auditLog.setDescription("Edit Profile berhasil");
			try {
				auditLogService.insertAuditLog(auditLog);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		return getPageContent(PROFILE_PAGE_NAME);
	}
}
