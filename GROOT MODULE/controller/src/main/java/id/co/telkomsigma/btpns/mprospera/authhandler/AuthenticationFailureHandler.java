package id.co.telkomsigma.btpns.mprospera.authhandler;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.service.AuditLogService;
import id.co.telkomsigma.btpns.mprospera.service.ParameterService;
import id.co.telkomsigma.btpns.mprospera.service.UserService;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.*;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Component("authenticationFailureHandler")
public class AuthenticationFailureHandler implements
		org.springframework.security.web.authentication.AuthenticationFailureHandler {
	private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

	@Autowired
	private UserService userServiceWithCache;

	@Autowired
	private ParameterService paramService;

	@Autowired
	private AuditLogService auditLogService;

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException authExc) throws IOException, ServletException {
		String username = request.getParameter("username");

		Log log = LogFactory.getLog(this.getClass());
		log.debug("Login fail with: " + username + " " + authExc.getMessage());
		if (authExc instanceof LockedException) {
			redirectStrategy.sendRedirect(request, response, "/login?error=locked");
		} else if (authExc instanceof DisabledException) {
			redirectStrategy.sendRedirect(request, response, "/login?error=disabled");
		} else if (authExc instanceof CredentialsExpiredException) {
			redirectStrategy.sendRedirect(request, response, "/login?error=expired-credential");
		} else if (authExc instanceof AccountExpiredException) {
			redirectStrategy.sendRedirect(request, response, "/login?error=expired-account");
		} else if (authExc instanceof BadCredentialsException) {

			UserDetails userDetail = userServiceWithCache.loadUserByUsernameNoException(username);
			if (userDetail != null) {
				User user = (User) userDetail;
				user.setPassword(null);
				String failedAttempt = user.getCustomProp(WebGuiConstant.PARAMETER_USER_CURRENT_FAILED_ATTEMPT);
				if (failedAttempt != null) {
					Integer currentAttempt = Integer.parseInt(failedAttempt);
					Integer maxAttempt = Integer.parseInt(paramService.loadParamByParamName(
							WebGuiConstant.PARAMETER_NAME_MAX_FAILED_ATTEMPT,
							WebGuiConstant.PARAMETER_MAX_FAILED_ATTEMPT_DEFAULT_VALUE));
					currentAttempt++;
					if (currentAttempt > maxAttempt) {
						user.setAccountNonLocked(false);
						Integer lockDurationInMinutes = Integer.parseInt(paramService.loadParamByParamName(
								WebGuiConstant.PARAMETER_NAME_LOCK_DURATION,
								WebGuiConstant.PARAMETER_LOCK_DURATION_DEFAULT_VALUE));
						Date lockUntil = DateUtils.addMinutes(new Date(), lockDurationInMinutes);
						user.setCustomProp(WebGuiConstant.PARAMETER_USER_CURRENT_LOCK_UNTIL, "" + lockUntil.getTime());
						userServiceWithCache.failedAttempt(user);
					} else {
						user.setCustomProp(WebGuiConstant.PARAMETER_USER_CURRENT_FAILED_ATTEMPT, "" + currentAttempt);
						userServiceWithCache.failedAttempt(user);
					}

				} else {
					user.setCustomProp(WebGuiConstant.PARAMETER_USER_CURRENT_FAILED_ATTEMPT, "1");
					userServiceWithCache.failedAttempt(user);
				}
				userServiceWithCache.clearUserCache();
				String date = new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date());

				AuditLog auditLog = new AuditLog();
				auditLog.setActivityType(1);
				auditLog.setCreatedBy(user.getUsername());
				auditLog.setCreatedDate(new Date());
				auditLog.setDescription("Login failed bad credential");
				auditLog.setReffNo(date);

				auditLogService.insertAuditLog(auditLog);
				log.debug("Added Audit Log");
			}
			redirectStrategy.sendRedirect(request, response, "/login?error=bad-credential");
		} else {
			redirectStrategy.sendRedirect(request, response, "/login?error");
		}
	}

}