package id.co.telkomsigma.btpns.mprospera.controller;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.menu.Menu;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.user.Role;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.pojo.DataTablesPojo;
import id.co.telkomsigma.btpns.mprospera.service.AuditLogService;
import id.co.telkomsigma.btpns.mprospera.service.MenuService;
import id.co.telkomsigma.btpns.mprospera.service.RoleService;
import id.co.telkomsigma.btpns.mprospera.service.UserService;
import id.co.telkomsigma.btpns.mprospera.util.FormValidationUtil;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.text.html.HTML;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

@Controller
public class ManageRoleController extends GenericController {

	// ----------------------------------------- LIST ROLE SECTION
	// ----------------------------------------- //

	private static final String LIST_PAGE_NAME = "manage/role/list";
	private static final String LIST_COMMAND = "redirect:" + WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_ROLE;

	@Autowired
	private RoleService roleService;

	@Autowired
	private UserService userService;

	@Autowired
	private MenuService menuService;

	@Autowired
	private AuditLogService auditLogService;

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_ROLE_MAPPING, method = RequestMethod.GET)
	public String showListPage(final Model model, final HttpServletRequest request, final HttpServletResponse response) {
		List<Role> roles = roleService.getRoles();
		model.addAttribute("roles", roles);

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();

		String date = new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date());

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.VIEW_PAGE);
		auditLog.setCreatedBy(user.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setDescription("Buka Halaman Kelola Role");
		auditLog.setReffNo(date);

		try {
			auditLogService.insertAuditLog(auditLog);
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.debug("Added Audit Log");

		return getPageContent(LIST_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_ROLE_GET_LIST_MAPPING, method = RequestMethod.GET)
	public void doSearch(final HttpServletRequest request, final HttpServletResponse response) throws IOException {

		if (isAjaxRequest(request)) {
			setResponseAsJson(response);

			DataTablesPojo dataTablesPojo = new DataTablesPojo();
			try {
				dataTablesPojo.setDraw(Integer.parseInt(request.getParameter("draw")));
			} catch (NumberFormatException e) {
				dataTablesPojo.setDraw(0);
			}

			final int orderableColumnCount = 2;
			final StringBuffer sbOrder = new StringBuffer();
			LinkedHashMap<String, String> columnMap = new LinkedHashMap<String, String>();
			columnMap.put("0", "authority");
			columnMap.put("1", "description");

			int offset = 0;
			int limit = 50;
			try {
				offset = Integer.parseInt(request.getParameter("start"));
			} catch (NumberFormatException e) {
			}
			try {
				limit = Integer.parseInt(request.getParameter("length"));
			} catch (NumberFormatException e) {
			}

			for (int i = 0; i < orderableColumnCount; i++) {
				String orderColumn = request.getParameter("order[" + i + "][column]");
				if (orderColumn == null || orderColumn.equalsIgnoreCase("")) {
					break;
				} else {
					orderColumn = columnMap.get(orderColumn);
					if (orderColumn == null) {
						break;
					}
					String orderDir = request.getParameter("order[" + i + "][dir]");
					if (orderDir == null || !orderDir.equalsIgnoreCase("desc")) {
						orderDir = "asc";
					}
					sbOrder.append(orderColumn);
					sbOrder.append("-");
					sbOrder.append(orderDir);
					sbOrder.append("+");
				}
			}

			final String authority = request.getParameter("q");

			try {
				final int finalOffset = offset;
				final int finalLimit = limit;

				Page<Role> roleListPojo = roleService.search(authority, columnMap, finalOffset, finalLimit);

				List<String[]> data = new ArrayList<>();

				for (Role role : roleListPojo) {
					data.add(new String[] {
							HtmlUtils.htmlEscape(role.getAuthority().toUpperCase()),
							HtmlUtils.htmlEscape(role.getDescription()),
							"<a href=\""
									+ getServletContext().getContextPath()
									+ WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_ROLE_ADD_REQUEST.replace("{roleId}", ""
											+ role.getRoleId())
									+ "\"><i class='fa fa-pencil'><div class='action' id='action-pencil'>ubah role</div></i></a>" });
				}

				dataTablesPojo.setData(data);
				dataTablesPojo.setRecordsFiltered(Integer.parseInt("" + roleListPojo.getTotalElements()));
				dataTablesPojo.setRecordsTotal(Integer.parseInt("" + roleListPojo.getTotalElements()));

			} catch (HttpClientErrorException e) {
				log.error(e.getStatusCode().toString(), e);
				dataTablesPojo.setError(getMessage("message.datatables.error.fetch.role"));
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				dataTablesPojo.setError(getMessage("message.datatables.error.fetch.role"));
			} finally {
				response.getWriter().write(new JsonUtils().toJson(dataTablesPojo));
			}
		} else {
			response.setStatus(HttpServletResponse.SC_FOUND);
		}

	}

	// ----------------------------------------- ADD/EDIT ROLE SECTION
	// ----------------------------------------- //

	private static final String ADD_PAGE_NAME = "manage/role/add";

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_ROLE_ADD_MAPPING, method = { RequestMethod.GET })
	public String showAddPage(@RequestParam(value = "roleId", required = false) final Long roleId, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.VIEW_PAGE);
		auditLog.setCreatedBy(user.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setDescription("Buka Halaman Ubah atau Tambah Role ");
		auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));

		try {
			auditLogService.insertAuditLog(auditLog);
		} catch (Exception e) {
			e.printStackTrace();
		}

		List<Menu> menus = menuService.getAllMenus();
		model.addAttribute("allMenus", menus);

		if (roleId != null) {
			Role role = roleService.loadUserByRoleId(roleId);

			for (Menu menu : role.getMenus()) {
				if (!menu.getMenuPath().contains("LABEL"))
					role.getStrMenus().add(menu.getMenuId() + "");
			}
			role.setRoleId(roleId);
			model.addAttribute("role", role);
		} else
			model.addAttribute("role", new Role());

		return getPageContent(ADD_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_ROLE_ADD_MAPPING, method = { RequestMethod.POST })
	public String doAdd(@RequestParam(value = "roleId", required = false) final Long roleId,
			@ModelAttribute("role") Role role, BindingResult errors, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) {
		// validating role
		if (roleId == null)
			FormValidationUtil.validatingAuthority(errors, "authority", HtmlUtils.htmlEscape(role.getAuthority()),
					getMessage("label.role.name"), Role.MAX_LENGTH_AUTHORITY, Role.MIN_LENGTH_AUTHORITY);

		// validating description
		FormValidationUtil.validatingRoleDescription(errors, "description", HtmlUtils.htmlEscape(role.getDescription()),
				getMessage("label.role.description"), Role.MAX_LENGTH_DESCRIPTION);

		// validating MENU
		FormValidationUtil.validatingMenus(errors, "menus", role.getStrMenus(), getMessage("label.role.menu"));

		List<Menu> allMenus = menuService.getAllMenus();

		if (!errors.hasErrors()) {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User admin = (User) auth.getPrincipal();

			Long userId = admin.getUserId();

			// add auditLog

			AuditLog auditLog = new AuditLog();
			auditLog.setActivityType(auditLog.SAVE_OR_UPDATE);
			auditLog.setCreatedBy(admin.getUsername());
			auditLog.setCreatedDate(new Date());
			auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));

			role.setUpdatedBy(admin);
			role.setUpdatedDate(new Date());
			for (String strMenuId : role.getStrMenus()) {
				Menu menu = new Menu();
				menu.setMenuId(Long.parseLong(strMenuId));
				role.addMenu(menu);
			}

			if (roleId == null) {// for add
				role.setUserIdCreated(userId);
				role.setCreatedBy(admin);
				role.setCreatedDate(new Date());
				try {
					roleService.createRole(role);
					model.addAttribute("role", new Role());
					model.addAttribute("okMessage", getMessage("form.ok.add.role"));
					auditLog.setDescription("Tambah Role Berhasil");
					try {
						auditLogService.insertAuditLog(auditLog);
					} catch (Exception e) {
						e.printStackTrace();
					}

				} catch (Exception e) {
					log.error(e.getMessage(), e);
					model.addAttribute("errorMessage", getMessage("form.error.add.user"));
					auditLog.setDescription("Tambah Role Gagal ");
					try {
						auditLogService.insertAuditLog(auditLog);
					} catch (Exception ex) {
						ex.printStackTrace();
					}

				}
			} else {
				// for edit
				try {
					Role fromDb = roleService.loadUserByRoleId(roleId);
					role.setAuthority(fromDb.getAuthority());
					role.setUpdatedBy(admin);
					role.setUpdatedDate(new Date());
					role.setUserIdUpdated(userId);

					roleService.updateRole(role);
					model.addAttribute("okMessage", getMessage("form.ok.edit.role"));
					auditLog.setDescription("Ubah Role " + role.getAuthority() + " Sukses ");
					try {
						auditLogService.insertAuditLog(auditLog);
					} catch (Exception e) {
						e.printStackTrace();
					}
				} catch (HttpClientErrorException e) {

					log.error(e.getStatusCode().toString(), e);
					model.addAttribute("errorMessage", getMessage("form.error.edit.role"));
					auditLog.setDescription("Ubah Role " + role.getAuthority() + " Gagal ");
					try {
						auditLogService.insertAuditLog(auditLog);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				} catch (Exception e) {
					log.error(e.getMessage(), e);
					model.addAttribute("errorMessage", getMessage("form.error.edit.role"));
					auditLog.setDescription("Ubah Role " + role.getAuthority() + " Gagal ");
					try {
						auditLogService.insertAuditLog(auditLog);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
			roleService.clearAllRolesCache();
		}
		// cek kalau pas edit, ada error, kewenangan tidak kosong
		if (roleId != null) {
			Role fromDb;
			try {
				fromDb = roleService.loadUserByRoleId(roleId);
				role.setAuthority(fromDb.getAuthority());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		model.addAttribute("allMenus", allMenus);
		return getPageContent(ADD_PAGE_NAME);
	}

}
