package id.co.telkomsigma.btpns.mprospera.controller;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.pojo.DataTablesPojo;
import id.co.telkomsigma.btpns.mprospera.service.AreaService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalService;
import id.co.telkomsigma.btpns.mprospera.service.UploadService;
import id.co.telkomsigma.btpns.mprospera.service.UserService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by daniel on 4/23/15.
 */
@Controller("homeController")
public class HomeController extends GenericController {
	private static final String HOME_PAGE_NAME = "home";

	@Autowired
	private TerminalService terminalService;

	@Autowired
	private UserService userService;

	@Autowired
	private AreaService areaService;
	
	@Autowired
	private UploadService uploadService;

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_HOME_PAGE, method = { RequestMethod.GET, RequestMethod.POST })
	public String showHomepageDashboard(final Model model, final HttpServletRequest request,
			final HttpServletResponse response) {
		List<String> statusGroup = new ArrayList<String>();
		uploadService.clearBar();
		statusGroup.add("LOGGED IN");
		statusGroup.add("OFF");
		uploadService.clearBar();
		model.addAttribute("statusGroup", statusGroup);
		return getPageContent(HOME_PAGE_NAME);
	}

	public String showRequestResendActivationPage(final Model model, final HttpServletRequest request,
			final HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		model.addAttribute("user", (User) auth.getPrincipal());
		return getPageContent(HOME_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_HOME_GET_LIST_USER, method = RequestMethod.GET)
	public void doList(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		if (isAjaxRequest(request)) {
			setResponseAsJson(response);

			DataTablesPojo dataTablesPojo = new DataTablesPojo();
			try {
				dataTablesPojo.setDraw(Integer.parseInt(request.getParameter("draw")));
			} catch (NumberFormatException e) {
				dataTablesPojo.setDraw(0);
			}

			final int orderableColumnCount = 3;
			final StringBuffer sbOrder = new StringBuffer();
			LinkedHashMap<String, String> columnMap = new LinkedHashMap<String, String>();
			columnMap.put("0", "user");
			columnMap.put("1", "wisma");
			columnMap.put("2", "status");

			int offset = 0;
			int limit = 50;
			try {
				offset = Integer.parseInt(request.getParameter("start"));
			} catch (NumberFormatException e) {
			}
			try {
				limit = Integer.parseInt(request.getParameter("length"));
			} catch (NumberFormatException e) {
			}

			for (int i = 0; i < orderableColumnCount; i++) {
				String orderColumn = request.getParameter("order[" + i + "][column]");
				if (orderColumn == null || orderColumn.equalsIgnoreCase("")) {
					break;
				} else {
					orderColumn = columnMap.get(orderColumn);
					if (orderColumn == null) {
						break;
					}
					String orderDir = request.getParameter("order[" + i + "][dir]");
					if (orderDir == null || !orderDir.equalsIgnoreCase("desc")) {
						orderDir = "asc";
					}
					sbOrder.append(orderColumn);
					sbOrder.append("-");
					sbOrder.append(orderDir);
					sbOrder.append("+");
				}
			}

			final String keyword = request.getParameter("q");
			final String status = request.getParameter("a");

			try {
				final int finalOffset = offset;
				final int finalLimit = limit;
				Page<User> userListPojo = userService.searchUserLogin(keyword, status, columnMap, finalOffset,
						finalLimit);

				List<String[]> data = new ArrayList<>();

				for (User user : userListPojo) {
					String userStatus = "OFF";
					if (user.getSessionKey() != null)
						userStatus = "LOGGED IN";
					String statusColored = "";
					if (userStatus.equals("OFF"))
						statusColored = "<img th:src=\"@{/img/status_off.png}\" style=\"width:4px;height:4px;\"><font color=\"red\"><b>"
								+ userStatus + "</b></font>";
					else if (userStatus.equals("LOGGED IN"))
						statusColored = "<img th:src=\"@{/img/status_on.png}\" style=\"width:4px;height:4px;\"><font color=\"green\"><b>"
								+ userStatus + "</b></font>";

					if (userStatus.equals(status) || status == null || "".equals(status))
						if (user.getOfficeCode() == null) {
							String userLocation = "";
							data.add(new String[] { HtmlUtils.htmlEscape(user.getUsername()), HtmlUtils.htmlEscape(userLocation), statusColored });
						} else {
							String userLocation = areaService.findLocationCodeByLocationId(user.getOfficeCode());
							data.add(new String[] { HtmlUtils.htmlEscape(user.getUsername()), HtmlUtils.htmlEscape(userLocation), statusColored });
						}
				}
				dataTablesPojo.setData(data);
				dataTablesPojo.setRecordsFiltered(Integer.parseInt("" + userListPojo.getTotalElements()));
				dataTablesPojo.setRecordsTotal(Integer.parseInt("" + userListPojo.getTotalElements()));
			} catch (HttpClientErrorException e) {
				log.error(e.getStatusCode().toString(), e);
				dataTablesPojo.setError(getMessage("message.datatables.error.fetch.terminal"));
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				dataTablesPojo.setError(getMessage("message.datatables.error.fetch.terminal"));
			} finally {
				response.getWriter().write(new JsonUtils().toJson(dataTablesPojo));
			}
		} else {
			response.setStatus(HttpServletResponse.SC_FOUND);
		}

	}

}
