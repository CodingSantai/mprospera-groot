package id.co.telkomsigma.btpns.mprospera.controller;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.exception.UserProfileException;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.user.Role;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.pojo.DataTablesPojo;
import id.co.telkomsigma.btpns.mprospera.service.AuditLogService;
import id.co.telkomsigma.btpns.mprospera.service.ParameterService;
import id.co.telkomsigma.btpns.mprospera.service.RoleService;
import id.co.telkomsigma.btpns.mprospera.service.UserService;
import id.co.telkomsigma.btpns.mprospera.util.FormValidationUtil;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import id.co.telkomsigma.btpns.mprospera.util.Select2Pojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by daniel on 4/24/15.
 */
@Controller
public class ManageUserController extends GenericController {
	// ----------------------------------------- LIST USER SECTION
	// ----------------------------------------- //
	private static final String LIST_PAGE_NAME = "manage/user/list";
	private static final String LIST_COMMAND = "redirect:" + WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_USER;

	@Autowired
	private UserService userServiceWithCache;

	@Autowired
	private ParameterService paramService;

	@Autowired
	private RoleService roleService;

	@Autowired
	private AuditLogService auditLogService;

	@Autowired
	private UserService userService;

	List<Long> roleName = new ArrayList<Long>();

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_USER_MAPPING, method = RequestMethod.GET)
	public String showListPage(final Model model, final HttpServletRequest request, final HttpServletResponse response) {
		List<Role> roles = roleService.getRoles();
		String defaultLockDuration = paramService.loadParamByParamName(WebGuiConstant.PARAMETER_NAME_LOCK_DURATION,
				WebGuiConstant.PARAMETER_LOCK_DURATION_DEFAULT_VALUE);
		model.addAttribute("roles", roles);
		model.addAttribute("defaultLockDuration", defaultLockDuration);

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.VIEW_PAGE);
		auditLog.setCreatedBy(user.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setDescription("Buka Halaman Kelola User");
		auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));

		try {
			auditLogService.insertAuditLog(auditLog);
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.debug("Added Audit Log");

		return getPageContent(LIST_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_USER_GET_LIST_MAPPING, method = RequestMethod.GET)
	public void doSearch(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		if (isAjaxRequest(request)) {
			setResponseAsJson(response);

			DataTablesPojo dataTablesPojo = new DataTablesPojo();
			try {
				dataTablesPojo.setDraw(Integer.parseInt(request.getParameter("draw")));
			} catch (NumberFormatException e) {
				dataTablesPojo.setDraw(0);
			}

			final int orderableColumnCount = 5;
			final StringBuffer sbOrder = new StringBuffer();
			LinkedHashMap<String, String> columnMap = new LinkedHashMap<String, String>();

			columnMap.put("0", "email");
			columnMap.put("1", "r.authority");
			columnMap.put("2", "username");

			int offset = 0;
			int limit = 50;
			try {
				offset = Integer.parseInt(request.getParameter("start"));
			} catch (NumberFormatException e) {
			}
			try {
				limit = Integer.parseInt(request.getParameter("length"));
			} catch (NumberFormatException e) {
			}

			for (int i = 0; i < orderableColumnCount; i++) {
				String orderColumn = request.getParameter("order[" + i + "][column]");
				if (orderColumn == null || orderColumn.equalsIgnoreCase("")) {
					break;
				} else {
					orderColumn = columnMap.get(orderColumn);
					if (orderColumn == null) {
						break;
					}
					String orderDir = request.getParameter("order[" + i + "][dir]");
					if (orderDir == null || !orderDir.equalsIgnoreCase("desc")) {
						orderDir = "asc";
					}
					sbOrder.append(orderColumn);
					sbOrder.append("-");
					sbOrder.append(orderDir);
					sbOrder.append("+");
				}
			}

			final String keyword = request.getParameter("q");
			final String authority = request.getParameter("a");

			try {
				final int finalOffset = offset;
				final int finalLimit = limit;

				Page<User> userListPojo = userServiceWithCache.searchUser(keyword, authority, columnMap, finalOffset,
						finalLimit);

				List<String[]> data = new ArrayList<>();

				for (User user : userListPojo) {
					StringBuffer sbRoles = new StringBuffer();
					for (Role role : user.getRoles()) {
						sbRoles.append(role.getAuthority().toUpperCase());
						sbRoles.append(", ");
					}
					if (sbRoles.length() > 0) {
						sbRoles.setLength(sbRoles.length() - 2);
					}
					String url = "";
					// edit
					url = url
							+ " <a href=\""
							+ getServletContext().getContextPath()
							+ WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_USER_ADD_REQUEST.replace("{userId}",
									"" + user.getUserId())
							+ "\"><i class='fa fa-edit'> <div class='action' id='action-edit'>edit</div></i></a>  ";
					// reset password
					/*
					 * url = url +
					 * "<a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\" data-event=\"reset\" data-username=\""
					 * + user.getName() + "\" data-link=\"" +
					 * getServletContext().getContextPath() +
					 * WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_USER_RESET_REQUEST
					 * .replace("{userId}", "" + user.getUserId()) +
					 * "\" data-id=\"" + user.getUserId() +
					 * "\"><i class='fa fa-key'> <div class='action' id='action-key'>reset password</div></i></a>  "
					 * ;
					 */
					// logout
					url = url
							+ "<a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\" data-event=\"logout\" data-username=\""
							+ HtmlUtils.htmlEscape(user.getName())
							+ "\" data-link=\""
							+ getServletContext().getContextPath()
							+ WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_USER_CLEAR_SESSION_REQUEST.replace("{userId}", ""
									+ user.getUserId())
							+ "\" data-id=\""
							+ user.getUserId()
							+ "\"><i class='fa fa-sign-out'> <div class='action' id='action-logout'>clear session</div></i></a>  ";
					// blok
					/*
					 * if (user.isAccountNonLocked()) { url = url +
					 * "<a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\" data-event=\"lock\" data-username=\""
					 * + user.getName() + "\" data-link=\"" +
					 * getServletContext().getContextPath() +
					 * WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_USER_LOCK_REQUEST
					 * .replace("{userId}", "" + user.getUserId()) +
					 * "\" data-id=\"" + user.getUserId() +
					 * "\"><i class='fa fa-lock'> <div class='action' id='action-lock'>lock</div></i></a>  "
					 * ; } else if (!user.isAccountNonLocked()) { url = url +
					 * "<a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\" data-event=\"unlock\" data-username=\""
					 * + user.getName() + "\" data-link=\"" +
					 * getServletContext().getContextPath() +
					 * WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_USER_UNLOCK_REQUEST
					 * .replace("{userId}", "" + user.getUserId()) +
					 * "\" data-id=\"" + user.getUserId() +
					 * "\"><i class='fa fa-unlock'> <div class='action' id='action-unlock'>unlock</div></i></a>  "
					 * ; }
					 */
					// enable & disable
					/*
					 * if (user.isEnabled()) { url = url +
					 * "<a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\" data-event=\"disable\" data-username=\""
					 * + user.getName() + "\" data-link=\"" +
					 * getServletContext().getContextPath() +
					 * WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_USER_DISABLE_REQUEST
					 * .replace("{userId}", "" + user.getUserId()) +
					 * "\" data-id=\"" + user.getUserId() +
					 * "\"><i class='fa fa-ban'> <div class='action' id='action-ban'>nonaktif</div></i></a>  "
					 * ; } else if (!user.isEnabled()) { url = url +
					 * "<a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\" data-event=\"enable\" data-username=\""
					 * + user.getName() + "\" data-link=\"" +
					 * getServletContext().getContextPath() +
					 * WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_USER_ENABLE_REQUEST
					 * .replace("{userId}", "" + user.getUserId()) +
					 * "\" data-id=\"" + user.getUserId() +
					 * "\"><i class='fa fa-check'> <div class='action' id='action-check'>aktif</div></i></a>  "
					 * ; }
					 */
					data.add(new String[] { HtmlUtils.htmlEscape(user.getUsername()), HtmlUtils.htmlEscape(user.getEmail()), HtmlUtils.htmlEscape(sbRoles.toString()), HtmlUtils.htmlEscape(user.getName()),
							url });
				}

				dataTablesPojo.setData(data);
				dataTablesPojo.setRecordsFiltered(Integer.parseInt("" + userListPojo.getTotalElements()));
				dataTablesPojo.setRecordsTotal(Integer.parseInt("" + userListPojo.getTotalElements()));
			} catch (HttpClientErrorException e) {
				log.error(e.getStatusCode().toString(), e);
				dataTablesPojo.setError(getMessage("message.datatables.error.fetch.user"));
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				dataTablesPojo.setError(getMessage("message.datatables.error.fetch.user"));
			} finally {
				response.getWriter().write(new JsonUtils().toJson(dataTablesPojo));
			}
		} else {
			response.setStatus(HttpServletResponse.SC_FOUND);
		}
	}

	// --blok user
	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_USER_LOCK_MAPPING, method = { RequestMethod.GET })
	public String doLock(@RequestParam(value = "userId", required = true) Long userId,
			@RequestParam(value = "lockDuration", required = false) Long lockDuration,
			@ModelAttribute("user") User user, BindingResult errors, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) {

		// audit log
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User admin = (User) auth.getPrincipal();

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.SAVE_OR_UPDATE);
		auditLog.setCreatedBy(admin.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));

		try {
			List<Role> roles = roleService.getRoles();
			String defaultLockDuration = paramService.loadParamByParamName(WebGuiConstant.PARAMETER_NAME_LOCK_DURATION,
					WebGuiConstant.PARAMETER_LOCK_DURATION_DEFAULT_VALUE);
			model.addAttribute("roles", roles);
			model.addAttribute("defaultLockDuration", defaultLockDuration);

			User userToLock = userServiceWithCache.loadUserByUserId(userId);
			FormValidationUtil.validatingLockUser(errors, "", userToLock, "");

			if (!errors.hasErrors()) {

				try {
					if (lockDuration == null)
						lockDuration = Long.parseLong(paramService.loadParamByParamName(
								WebGuiConstant.PARAMETER_NAME_LOCK_DURATION,
								WebGuiConstant.PARAMETER_LOCK_DURATION_DEFAULT_VALUE));
					userServiceWithCache.lockUser(userToLock, lockDuration);
					userServiceWithCache.clearUserCache();
					model.addAttribute("okMessage", getMessage("form.ok.lock.user"));
					auditLog.setDescription("Blok User " + userToLock.getUsername() + " berhasil");
					try {
						auditLogService.insertAuditLog(auditLog);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				} catch (HttpClientErrorException e) {
					log.error(e.getStatusCode().toString(), e);
					auditLog.setDescription("Blok User " + userToLock.getUsername() + " gagal");
					try {
						auditLogService.insertAuditLog(auditLog);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				} catch (Exception e) {
					log.error(e.getMessage(), e);
					model.addAttribute("errorMessage", getMessage("field.error.user.not.found", user.getName()));
					auditLog.setDescription("Blok User " + user.getUsername() + " gagal");
					try {
						auditLogService.insertAuditLog(auditLog);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
		} catch (UsernameNotFoundException e) {
			log.error(e.getMessage(), e);
			model.addAttribute("errorMessage", getMessage("form.error.lock.user"));
			auditLog.setDescription("Blok User " + user.getUsername() + " gagal");
			try {
				auditLogService.insertAuditLog(auditLog);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return getPageContent(LIST_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_USER_UNLOCK_MAPPING, method = { RequestMethod.GET })
	public String doUnlock(@RequestParam(value = "userId", required = true) final Long userId,
			@ModelAttribute("user") User user, BindingResult errors, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) {

		// audit log
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User admin = (User) auth.getPrincipal();

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.SAVE_OR_UPDATE);
		auditLog.setCreatedBy(admin.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));

		try {
			List<Role> roles = roleService.getRoles();
			String defaultLockDuration = paramService.loadParamByParamName(WebGuiConstant.PARAMETER_NAME_LOCK_DURATION,
					WebGuiConstant.PARAMETER_LOCK_DURATION_DEFAULT_VALUE);
			model.addAttribute("roles", roles);
			model.addAttribute("defaultLockDuration", defaultLockDuration);

			User userToUnlock = userServiceWithCache.loadUserByUserId(userId);
			FormValidationUtil.validatingUnLockUser(errors, "", userToUnlock, "");
			if (!errors.hasErrors()) {

				try {
					userServiceWithCache.unlockUser(userToUnlock);
					userServiceWithCache.clearUserCache();
					model.addAttribute("okMessage", getMessage("form.ok.unlock.user"));
					auditLog.setDescription("unBlok User " + userToUnlock.getUsername() + " berhasil");
					try {
						auditLogService.insertAuditLog(auditLog);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				} catch (HttpClientErrorException e) {
					log.error(e.getStatusCode().toString(), e);
					auditLog.setDescription("unBlok User " + userToUnlock.getUsername() + " gagal");
					try {
						auditLogService.insertAuditLog(auditLog);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				} catch (Exception e) {
					log.error(e.getMessage(), e);
					model.addAttribute("errorMessage", getMessage("field.error.user.not.found", user.getName()));
					auditLog.setDescription("unBlok User " + user.getUsername() + " gagal");
					try {
						auditLogService.insertAuditLog(auditLog);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
		} catch (UsernameNotFoundException e) {
			log.error(e.getMessage(), e);
			model.addAttribute("errorMessage", getMessage("form.error.unlock.user"));
			auditLog.setDescription("unBlok User " + user.getUsername() + " gagal");
			try {
				auditLogService.insertAuditLog(auditLog);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return getPageContent(LIST_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_USER_RESET_MAPPING, method = { RequestMethod.GET })
	public String doReset(@RequestParam(value = "userId", required = true) final Long userId,
			@ModelAttribute("user") User user, BindingResult errors, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) {

		// audit log
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User admin = (User) auth.getPrincipal();

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.SAVE_OR_UPDATE);
		auditLog.setCreatedBy(admin.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));

		try {
			List<Role> roles = roleService.getRoles();
			String defaultLockDuration = paramService.loadParamByParamName(WebGuiConstant.PARAMETER_NAME_LOCK_DURATION,
					WebGuiConstant.PARAMETER_LOCK_DURATION_DEFAULT_VALUE);
			model.addAttribute("roles", roles);
			model.addAttribute("defaultLockDuration", defaultLockDuration);

			User userToReset = userServiceWithCache.loadUserByUserId(userId);
			FormValidationUtil.validatingResetPassword(errors, "", userToReset, "");
			if (!errors.hasErrors()) {

				try {
					userServiceWithCache.resetPassword(userToReset);
					userServiceWithCache.clearUserCache();
					model.addAttribute("okMessage", getMessage("form.ok.reset.user"));
					auditLog.setDescription("Reset User " + userToReset.getUsername() + " berhasil");
					try {
						auditLogService.insertAuditLog(auditLog);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				} catch (HttpClientErrorException e) {
					log.error(e.getStatusCode().toString(), e);
					auditLog.setDescription("Reset User " + userToReset.getUsername() + " gagal");
					try {
						auditLogService.insertAuditLog(auditLog);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				} catch (Exception e) {
					log.error(e.getMessage(), e);
					model.addAttribute("errorMessage", getMessage("field.error.user.not.found", user.getName()));
					auditLog.setDescription("Reset User " + user.getUsername() + " gagal");
					try {
						auditLogService.insertAuditLog(auditLog);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
		} catch (UsernameNotFoundException e) {
			log.error(e.getMessage(), e);
			model.addAttribute("errorMessage", getMessage("form.error.reset.user"));
			auditLog.setDescription("Reset User " + user.getUsername() + " gagal");
			try {
				auditLogService.insertAuditLog(auditLog);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return getPageContent(LIST_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_USER_ENABLE_MAPPING, method = { RequestMethod.GET })
	public String doEnable(@RequestParam(value = "userId", required = true) final Long userId,
			@ModelAttribute("user") User user, BindingResult errors, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User admin = (User) auth.getPrincipal();

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.SAVE_OR_UPDATE);
		auditLog.setCreatedBy(admin.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));

		try {
			List<Role> roles = roleService.getRoles();
			String defaultLockDuration = paramService.loadParamByParamName(WebGuiConstant.PARAMETER_NAME_LOCK_DURATION,
					WebGuiConstant.PARAMETER_LOCK_DURATION_DEFAULT_VALUE);
			model.addAttribute("roles", roles);
			model.addAttribute("defaultLockDuration", defaultLockDuration);

			User userToEnable = userServiceWithCache.loadUserByUserId(userId);
			FormValidationUtil.validatingEnableUser(errors, "", userToEnable, "");
			if (!errors.hasErrors()) {

				try {
					userServiceWithCache.enableUser(userToEnable);
					userServiceWithCache.clearUserCache();
					model.addAttribute("okMessage", getMessage("form.ok.enable.user"));
					auditLog.setDescription("Enable User " + userToEnable.getUsername() + " berhasil");
					try {
						auditLogService.insertAuditLog(auditLog);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				} catch (HttpClientErrorException e) {
					log.error(e.getStatusCode().toString(), e);
					auditLog.setDescription("Enable User " + userToEnable.getUsername() + " gagal");
					try {
						auditLogService.insertAuditLog(auditLog);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				} catch (Exception e) {
					log.error(e.getMessage(), e);
					model.addAttribute("errorMessage", getMessage("field.error.user.not.found", user.getName()));
					auditLog.setDescription("Enable User " + user.getUsername() + " gagal");
					try {
						auditLogService.insertAuditLog(auditLog);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
		} catch (UsernameNotFoundException e) {
			log.error(e.getMessage(), e);
			model.addAttribute("errorMessage", getMessage("form.error.enable.user"));
			auditLog.setDescription("Enable User " + user.getUsername() + " gagal");
			try {
				auditLogService.insertAuditLog(auditLog);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return getPageContent(LIST_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_USER_DISABLE_MAPPING, method = { RequestMethod.GET })
	public String doDisable(@RequestParam(value = "userId", required = true) final Long userId,
			@ModelAttribute("user") User user, BindingResult errors, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User admin = (User) auth.getPrincipal();

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.SAVE_OR_UPDATE);
		auditLog.setCreatedBy(admin.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));

		try {
			List<Role> roles = roleService.getRoles();
			String defaultLockDuration = paramService.loadParamByParamName(WebGuiConstant.PARAMETER_NAME_LOCK_DURATION,
					WebGuiConstant.PARAMETER_LOCK_DURATION_DEFAULT_VALUE);
			model.addAttribute("roles", roles);
			model.addAttribute("defaultLockDuration", defaultLockDuration);

			User userToDisable = userServiceWithCache.loadUserByUserId(userId);
			FormValidationUtil.validatingDisableUser(errors, "", userToDisable, "");
			if (!errors.hasErrors()) {

				try {
					userServiceWithCache.disableUser(userToDisable);
					userServiceWithCache.clearUserCache();
					model.addAttribute("okMessage", getMessage("form.ok.disable.user"));
					auditLog.setDescription("Disable User " + userToDisable.getUsername() + " gagal");
					try {
						auditLogService.insertAuditLog(auditLog);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				} catch (HttpClientErrorException e) {
					log.error(e.getStatusCode().toString(), e);
					auditLog.setDescription("Disable User " + userToDisable.getUsername() + " gagal");
					try {
						auditLogService.insertAuditLog(auditLog);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				} catch (Exception e) {
					log.error(e.getMessage(), e);
					model.addAttribute("errorMessage", getMessage("field.error.user.not.found", user.getName()));
					auditLog.setDescription("Disable User " + user.getUsername() + " gagal");
					try {
						auditLogService.insertAuditLog(auditLog);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
		} catch (UsernameNotFoundException e) {
			log.error(e.getMessage(), e);
			model.addAttribute("errorMessage", getMessage("form.error.disable.user"));
			auditLog.setDescription("Disable User " + user.getUsername() + " gagal");
			try {
				auditLogService.insertAuditLog(auditLog);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return getPageContent(LIST_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_USER_CLEAR_SESSION_MAPPING, method = { RequestMethod.GET })
	public String doClear(@RequestParam(value = "userId", required = true) final Long userId,
			@ModelAttribute("user") User user, BindingResult errors, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User admin = (User) auth.getPrincipal();

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.SAVE_OR_UPDATE);
		auditLog.setCreatedBy(admin.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));

		try {
			User userToClear = userService.loadUserByUserId(userId);
			if (!errors.hasErrors()) {

				try {
					userService.logout(userToClear);
					model.addAttribute("okMessage", getMessage("form.ok.logout.user"));
					auditLog.setDescription("Force Logout User " + userToClear.getUsername() + " success");
					try {
						auditLogService.insertAuditLog(auditLog);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				} catch (HttpClientErrorException e) {
					log.error(e.getStatusCode().toString(), e);
					auditLog.setDescription("Force Logout User " + userToClear.getUsername() + " failed");
					try {
						auditLogService.insertAuditLog(auditLog);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				} catch (Exception e) {
					log.error(e.getMessage(), e);
					model.addAttribute("errorMessage", getMessage("field.error.user.not.found", user.getName()));
					auditLog.setDescription("Force Logout User " + userToClear.getUsername() + " failed");
					try {
						auditLogService.insertAuditLog(auditLog);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
		} catch (UsernameNotFoundException e) {
			log.error(e.getMessage(), e);
			model.addAttribute("errorMessage", getMessage("form.error.logout.user"));
			auditLog.setDescription("Force Logout User " + user.getUsername() + " failed");
			try {
				auditLogService.insertAuditLog(auditLog);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return getPageContent(LIST_PAGE_NAME);
	}

	// ----------------------------------------- ADD USER SECTION
	// ----------------------------------------- //
	private static final String ADD_PAGE_NAME = "manage/user/add";

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_USER_ADD_MAPPING, method = { RequestMethod.GET })
	public String showAddPage(@RequestParam(value = "userId", required = false) final Long userId, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) {
		List<Role> roles = roleService.getRoles();
		model.addAttribute("allRoles", roles);

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User admin = (User) auth.getPrincipal();

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.VIEW_PAGE);
		auditLog.setCreatedBy(admin.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));

		if (userId != null) {
			try {
				User user = userServiceWithCache.loadUserByUserId(userId);
				for (Role role : user.getRoles()) {
					user.getStrRoles().add(role.getRoleId() + "");
				}
				user.setUserId(userId);
				model.addAttribute("user", user);
				auditLog.setDescription("Buka Halaman edit User  " + user.getUsername() + " berhasil");
				try {
					auditLogService.insertAuditLog(auditLog);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			} catch (HttpClientErrorException e) {
				log.error(e.getStatusCode().toString(), e);
				auditLog.setDescription("Buka Halaman edit User gagal");
				try {
					auditLogService.insertAuditLog(auditLog);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				auditLog.setDescription("Buka Halaman edit User gagal");
				try {
					auditLogService.insertAuditLog(auditLog);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		} else {
			model.addAttribute("user", new User());
			auditLog.setDescription("Buka Halaman Tambah User berhasil");
			try {
				auditLogService.insertAuditLog(auditLog);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return getPageContent(ADD_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_USER_ADD_MAPPING, method = { RequestMethod.POST })
	public String doAdd(@RequestParam(value = "userId", required = false) final Long userId,
			@ModelAttribute("user") User user, BindingResult errors, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) {

		// validating username
		if (userId == null)
			FormValidationUtil.validatingUsername(errors, "username", HtmlUtils.htmlEscape(user.getUsername()), getMessage("label.username"),
					User.MAX_LENGTH_USERNAME, User.MIN_LENGTH_USERNAME);

		// validating email
		FormValidationUtil.validatingEmail(errors, "email", HtmlUtils.htmlEscape(user.getEmail()), getMessage("label.email"),
				User.MAX_LENGTH_EMAIL);

		// validating password
		/*
		 * if (userId == null) FormValidationUtil.validatingPassword(errors,
		 * "password", user.getPassword(), getMessage("label.password"),
		 * "passwordConfirm", user.getPasswordConfirm(),
		 * getMessage("label.password.confirm"), User.MAX_LENGTH_PASSWORD,
		 * User.MIN_LENGTH_PASSWORD);
		 */

		// validating name
		FormValidationUtil.validatingName(errors, "name", HtmlUtils.htmlEscape(user.getName()), getMessage("label.name"),
				User.MAX_LENGTH_NAME);

		// validating address
		FormValidationUtil.validatingAddress(errors, "address", HtmlUtils.htmlEscape(user.getAddress()), getMessage("label.address"),
				User.MAX_LENGTH_ADDRESS);

		// validating city
		FormValidationUtil.validatingCity(errors, "city", HtmlUtils.htmlEscape(user.getCity()), getMessage("label.city"),
				User.MAX_LENGTH_CITY);

		// validating province
		// FormValidationUtil.validatingProvince(errors, "province",
		// user.getProvince(), getMessage("label.province"),
		// User.MAX_LENGTH_PROVINCE);

		// validating phone mobile
		FormValidationUtil.validatingPhoneMobile(errors, "phoneMobile", HtmlUtils.htmlEscape(user.getPhoneMobile()),
				getMessage("label.phone.mobile"), User.MAX_LENGTH_PHONE_MOBILE, User.MIN_LENGTH_PHONE_MOBILE);

		// validating phone
		// FormValidationUtil.validatingPhone(errors, "phone", user.getPhone(),
		// getMessage("label.phone.home"), User.MAX_LENGTH_PHONE,
		// User.MIN_LENGTH_PHONE);

		// validating role
		FormValidationUtil.validatingRole(errors, "roles", user.getStrRoles(), getMessage("label.role"));

		// validating role
		// FormValidationUtil.validatingNotNullLocation(errors, "strLocation",
		// user.getStrLocation(), getMessage("label.location"));

		List<Role> allRoles = roleService.getRoles();

		if (!errors.hasErrors()) {
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			User admin = (User) auth.getPrincipal();
			user.setUpdatedBy(admin.getUserId());
			user.setUpdatedDate(new Date());

			for (String strRoleId : user.getStrRoles()) {
				Role role = new Role();
				role.setRoleId(Long.parseLong(strRoleId));
				user.addRole(role);
			}

			// Location location =
			// locationService.getLocationById(user.getStrLocation());
			user.setOfficeCode(null);
			String date = new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date());
			AuditLog auditLog = new AuditLog();
			auditLog.setActivityType(auditLog.SAVE_OR_UPDATE);
			auditLog.setCreatedBy(user.getUsername());
			auditLog.setCreatedDate(new Date());
			auditLog.setReffNo(date);

			if (userId == null) {
				user.setCreatedBy(admin.getUserId());
				user.setCreatedDate(new Date());
				user.setPassword("000000");
				user.setPasswordConfirm("000000");
				try {
					userServiceWithCache.createUser(user);
					auditLog.setDescription("Add user success");
					try {
						auditLogService.insertAuditLog(auditLog);
						log.debug("Added Audit Log");
					} catch (HttpClientErrorException e) {
						log.debug("Added Audit Log Failed");
					}
					model.addAttribute("user", new User());
					model.addAttribute("okMessage", getMessage("form.ok.add.user"));
					return getPageContent(LIST_PAGE_NAME);
				} catch (HttpClientErrorException e) {
					log.error(e.getStatusCode().toString(), e);
					if (e.getStatusCode() == HttpStatus.CONFLICT) {
						if (e.getResponseBodyAsString().equalsIgnoreCase(
								UserProfileException.NOT_UNIQUE_USERNAME_AND_EMAIL)) {
							errors.rejectValue("username", FormValidationUtil.FIELD_ERROR_NOT_UNIQUE_USERNAME,
									new Object[] { getMessage("label.username"), user.getUsername() }, "");
							errors.rejectValue("email", FormValidationUtil.FIELD_ERROR_NOT_UNIQUE_EMAIL, new Object[] {
									getMessage("label.email"), user.getEmail() }, "");
						} else if (e.getResponseBodyAsString().equalsIgnoreCase(
								UserProfileException.NOT_UNIQUE_USERNAME)) {
							errors.rejectValue("username", FormValidationUtil.FIELD_ERROR_NOT_UNIQUE_USERNAME,
									new Object[] { getMessage("label.username"), user.getUsername() }, "");
						} else if (e.getResponseBodyAsString().equalsIgnoreCase(UserProfileException.NOT_UNIQUE_EMAIL)) {
							errors.rejectValue("email", FormValidationUtil.FIELD_ERROR_NOT_UNIQUE_EMAIL, new Object[] {
									getMessage("label.email"), user.getEmail() }, "");
						} else {
							auditLog.setDescription("Add user Failed");
							try {
								auditLogService.insertAuditLog(auditLog);
								log.debug("Added Audit Log");
							} catch (HttpClientErrorException e1) {
								log.debug("Added Audit Log Failed");
							}
							model.addAttribute("errorMessage", getMessage("form.error.add.user"));
						}
					} else {
						auditLog.setDescription("Add user Failed");
						try {
							auditLogService.insertAuditLog(auditLog);
							log.debug("Added Audit Log");
						} catch (HttpClientErrorException e2) {
							log.debug("Added Audit Log Failed");
						}
						model.addAttribute("errorMessage", getMessage("form.error.add.user"));
					}
				} catch (Exception e) {
					log.error(e.getMessage(), e);
					auditLog.setDescription("Add user Failed");
					try {
						auditLogService.insertAuditLog(auditLog);
						log.debug("Added Audit Log");
					} catch (HttpClientErrorException e3) {
						log.debug("Added Audit Log Failed");
					}
					model.addAttribute("errorMessage", getMessage("form.error.add.user"));
				}
			} else {
				try {
					User fromDb = userServiceWithCache.loadUserByUserId(userId);
					user.setUsername(fromDb.getUsername());

					user.setPassword("000000");
					user.setPasswordConfirm("000000");
					user.setOfficeCode(null);
					userServiceWithCache.updateUser(user);
					userServiceWithCache.clearUserCache();
					model.addAttribute("user", user);
					auditLog.setDescription("Update user success");
					try {
						auditLogService.insertAuditLog(auditLog);
						log.debug("Added Audit Log");
					} catch (HttpClientErrorException e) {
						log.debug("Added Audit Log Failed");
					}
					model.addAttribute("okMessage", getMessage("form.ok.edit.user"));
					return getPageContent(LIST_PAGE_NAME);
				} catch (HttpClientErrorException e) {
					// user.setEnabled(originalUser.isEnabled());
					log.error(e.getStatusCode().toString(), e);
					auditLog.setDescription("Update user Failed");
					try {
						auditLogService.insertAuditLog(auditLog);
						log.debug("Added Audit Log");
					} catch (HttpClientErrorException e1) {
						log.debug("Added Audit Log Failed");
					}
					model.addAttribute("errorMessage", getMessage("form.error.edit.user"));
				} catch (Exception e) {
					// user.setEnabled(originalUser.isEnabled());
					auditLog.setDescription("Update user Failed");
					try {
						auditLogService.insertAuditLog(auditLog);
						log.debug("Added Audit Log");
					} catch (HttpClientErrorException e2) {
						log.debug("Added Audit Log Failed");
					}
					log.error(e.getMessage(), e);
					model.addAttribute("errorMessage", getMessage("form.error.edit.user"));
				}
			}
		}
		model.addAttribute("allRoles", allRoles);

		return getPageContent(ADD_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_USER_AJAX_SEARCH_LOCATION_MAPPING, method = RequestMethod.GET)
	public void ajaxSearchLocation(final HttpServletRequest request, final HttpServletResponse response)
			throws IOException {
		if (isAjaxRequest(request)) {
			setResponseAsJson(response);
			List<Select2Pojo> dataList = new ArrayList<>();

			/*
			 * String keyword = request.getParameter("q"); if(keyword!=null)
			 * if(keyword.trim().length()>0) { List<Location> locationList =
			 * locationService
			 * .getLocationBySearch("%"+keyword.toLowerCase()+"%");
			 * if(locationList!=null) if(locationList.size()>0) { for(Location
			 * loc:locationList) { Select2Pojo test = new Select2Pojo();
			 * test.setId(""+loc.getLocationId());
			 * test.setText(loc.getLocationId()+"-"+loc.getName());
			 * dataList.add(test); } } }
			 */
			response.getWriter().write(new JsonUtils().toJson(dataList));
		}
	}
}