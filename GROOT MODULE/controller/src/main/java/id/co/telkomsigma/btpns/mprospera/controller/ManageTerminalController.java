package id.co.telkomsigma.btpns.mprospera.controller;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.messageLogs.MessageLogs;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.terminal.Terminal;
import id.co.telkomsigma.btpns.mprospera.model.terminal.TerminalActivity;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.pojo.DataTablesPojo;
import id.co.telkomsigma.btpns.mprospera.service.AuditLogService;
import id.co.telkomsigma.btpns.mprospera.service.MessageLogsService;
import id.co.telkomsigma.btpns.mprospera.service.TerminalService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.HtmlUtils;

import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

@Controller
public class ManageTerminalController extends GenericController {

	protected final Log LOGGER = LogFactory.getLog(getClass());
	private static final String LIST_PAGE_NAME = "terminal/list";
	private static final String LIST_COMMAND = "redirect:" + WebGuiConstant.AFTER_LOGIN_PATH_TERMINAL;

	@Autowired
	private TerminalService terminalService;

	@Autowired
	private AuditLogService auditLogService;

	@Autowired
	private MessageLogsService messageLogsService;
	
	@Autowired
	private EntityManagerFactory entityManagerFactory;

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_TERMINAL_MAPPING, method = RequestMethod.GET)
	public String showListPage(final Model model, final HttpServletRequest request, final HttpServletResponse response) {
		List<Terminal> terminal = terminalService.getAllTerminal();

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();

		String date = new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date());

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.VIEW_PAGE);
		auditLog.setCreatedBy(user.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setDescription("Buka Halaman Daftar Terminal");
		auditLog.setReffNo(date);

		try {
			auditLogService.insertAuditLog(auditLog);
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.debug("Added Audit Log");

		model.addAttribute("terminal", terminal);
		return getPageContent(LIST_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_TERMINAL_GET_LIST_MAPPING, method = RequestMethod.GET)
	public void doSearch(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		if (isAjaxRequest(request)) {
			setResponseAsJson(response);

			DataTablesPojo dataTablesPojo = new DataTablesPojo();
			try {
				dataTablesPojo.setDraw(Integer.parseInt(request.getParameter("draw")));
			} catch (NumberFormatException e) {
				dataTablesPojo.setDraw(0);
			}

			final int orderableColumnCount = 2;
			final StringBuffer sbOrder = new StringBuffer();
			LinkedHashMap<String, String> columnMap = new LinkedHashMap<String, String>();
			columnMap.put("0", "imei");
			columnMap.put("1", "modelNumber");
			columnMap.put("2", "androidVersion");
			columnMap.put("4", "sessionCreatedDate");

			int offset = 0;
			int limit = 50;
			try {
				offset = Integer.parseInt(request.getParameter("start"));
			} catch (NumberFormatException e) {
			}
			try {
				limit = Integer.parseInt(request.getParameter("length"));
			} catch (NumberFormatException e) {
			}

			for (int i = 0; i < orderableColumnCount; i++) {
				String orderColumn = request.getParameter("order[" + i + "][column]");
				if (orderColumn == null || orderColumn.equalsIgnoreCase("")) {
					break;
				} else {
					orderColumn = columnMap.get(orderColumn);
					if (orderColumn == null) {
						break;
					}
					String orderDir = request.getParameter("order[" + i + "][dir]");
					if (orderDir == null || !orderDir.equalsIgnoreCase("desc")) {
						orderDir = "asc";
					}
					sbOrder.append(orderColumn);
					sbOrder.append("-");
					sbOrder.append(orderDir);
					sbOrder.append("+");
				}
			}

			final String keyword = request.getParameter("q");

			try {
				final int finalOffset = offset;
				final int finalLimit = limit;
				Page<Terminal> terminalListPojo = terminalService.search(keyword, columnMap, finalOffset, finalLimit);

				List<String[]> data = new ArrayList<>();

				SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss dd/MM/yyyy");

				for (Terminal terminal : terminalListPojo) {
					data.add(new String[] {
							HtmlUtils.htmlEscape(terminal.getImei()),
							HtmlUtils.htmlEscape(terminal.getModelNumber()),
							HtmlUtils.htmlEscape(terminal.getAndroidVersion()),
							HtmlUtils.htmlEscape(formatter.format(terminal.getCreatedDate())),
							"<a href=\""
									+ getServletContext().getContextPath()
									+ WebGuiConstant.AFTER_LOGIN_PATH_TERMINAL_DETAILS_REQUEST.replace("{terminalId}",
											"" + terminal.getTerminalId())
									+ "\"><i class='fa fa-eye'><div class='action' id='action-eye'>lihat aktifitas</div></i></a>" });
				}

				dataTablesPojo.setData(data);
				dataTablesPojo.setRecordsFiltered(Integer.parseInt("" + terminalListPojo.getTotalElements()));
				dataTablesPojo.setRecordsTotal(Integer.parseInt("" + terminalListPojo.getTotalElements()));
			} catch (HttpClientErrorException e) {
				log.error(e.getStatusCode().toString(), e);
				dataTablesPojo.setError(getMessage("message.datatables.error.fetch.terminal"));
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				dataTablesPojo.setError(getMessage("message.datatables.error.fetch.terminal"));
			} finally {
				response.getWriter().write(new JsonUtils().toJson(dataTablesPojo));
			}
		} else {
			response.setStatus(HttpServletResponse.SC_FOUND);
		}

	}

	/***********
	 * For terminal activity
	 *
	 */

	private static final String DETAILS_PAGE_NAME = "terminal/listActivity";

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_TERMINAL_DETAILS_MAPPING, method = { RequestMethod.GET })
	public String showDetailsPage(@RequestParam("terminalId") Long terminalId, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) {
		model.addAttribute("terminalId", terminalId);

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();

		String date = new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date());

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.VIEW_PAGE);
		auditLog.setCreatedBy(user.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setDescription("Buka Halaman Daftar Aktifitas Terminal");
		auditLog.setReffNo(date);

		try {
			auditLogService.insertAuditLog(auditLog);
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.debug("Added Audit Log");

		return getPageContent(DETAILS_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_TERMINAL_GET_LIST_DETAILS_BY_ID_MAPPING, method = RequestMethod.GET)
	public void doSearchDetails(final HttpServletRequest request, final Model model, final HttpServletResponse response)
			throws IOException {
		if (isAjaxRequest(request)) {
			setResponseAsJson(response);

			DataTablesPojo dataTablesPojo = new DataTablesPojo();
			try {
				dataTablesPojo.setDraw(Integer.parseInt(request.getParameter("draw")));
			} catch (NumberFormatException e) {
				dataTablesPojo.setDraw(0);
			}

			final int orderableColumnCount = 2;
			final StringBuffer sbOrder = new StringBuffer();
			LinkedHashMap<String, String> columnMap = new LinkedHashMap<String, String>();
			columnMap.put("0", "raw");

			int offset = 0;
			int limit = 50;
			try {
				offset = Integer.parseInt(request.getParameter("start"));
			} catch (NumberFormatException e) {
			}
			try {
				limit = Integer.parseInt(request.getParameter("length"));
			} catch (NumberFormatException e) {
			}

			for (int i = 0; i < orderableColumnCount; i++) {
				String orderColumn = request.getParameter("order[" + i + "][column]");
				if (orderColumn == null || orderColumn.equalsIgnoreCase("")) {
					break;
				} else {
					orderColumn = columnMap.get(orderColumn);
					if (orderColumn == null) {
						break;
					}
					String orderDir = request.getParameter("order[" + i + "][dir]");
					if (orderDir == null || !orderDir.equalsIgnoreCase("desc")) {
						orderDir = "asc";
					}
					sbOrder.append(orderColumn);
					sbOrder.append("-");
					sbOrder.append(orderDir);
					sbOrder.append("+");
				}
			}

			String keyword = request.getParameter("q");
			String terminalId = request.getParameter("terminalId");
			String startedDate = request.getParameter("c");
			String endedDate = request.getParameter("b");

			if (keyword == null)
				keyword = "";
			if (startedDate == null)
				startedDate = "";
			if (endedDate == null)
				endedDate = "";

			try {
				final int finalOffset = offset;
				final int finalLimit = limit;
				Page<TerminalActivity> terminalActivityListPojo = terminalService.searchActivity(keyword,
						Long.parseLong(terminalId), startedDate, endedDate, columnMap, finalOffset, finalLimit);

				List<String[]> data = new ArrayList<>();

				Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				for (TerminalActivity terminalActivity : terminalActivityListPojo) {
					data.add(new String[] {
							HtmlUtils.htmlEscape(formatter.format(terminalActivity.getCreatedDate()).toString()),
							HtmlUtils.htmlEscape(terminalActivity.getActivityTypeLabel()),
							"Activity ID:" + HtmlUtils.htmlEscape(terminalActivity.getTerminalActivityId()) + ", Reference Number:"
									+ HtmlUtils.htmlEscape(terminalActivity.getReffNo()),
							"<a href=\""
									+ getServletContext().getContextPath()
									+ WebGuiConstant.AFTER_LOGIN_PATH_TERMINAL_DETAILS_MESSAGE_REQUEST.replace(
											"{terminalActivityId}", "" + terminalActivity.getTerminalActivityId())
											.replace(
													"{date}",
													""
															+ new SimpleDateFormat("yyyy-MM-dd")
																	.format(terminalActivity.getCreatedDate()))
									+ "\"><i class='fa fa-eye'><div class='action' id='action-eye'>lihat message logs</div></i></a>" });
				}

				dataTablesPojo.setData(data);
				dataTablesPojo.setRecordsFiltered(terminalActivityListPojo.getContent().size());
				dataTablesPojo.setRecordsTotal(Integer.parseInt("" + terminalActivityListPojo.getTotalElements()));
			} catch (HttpClientErrorException e) {
				log.error(e.getStatusCode().toString(), e);
				dataTablesPojo.setError(getMessage("message.datatables.error.fetch.messageLogs"));
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				dataTablesPojo.setError(getMessage("message.datatables.error.fetch.messageLogs"));
			} finally {
				response.getWriter().write(new JsonUtils().toJson(dataTablesPojo));
			}
		} else {
			response.setStatus(HttpServletResponse.SC_FOUND);
		}

	}

	/***********
	 * For message logs terminal activity
	 *
	 */

	private static final String DETAILS_MESSAGE_LOGS_PAGE_NAME = "terminal/message_logs/list";

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_TERMINAL_DETAILS_MESSAGE_MAPPING, method = { RequestMethod.GET })
	public String showMessagesPage(@RequestParam("terminalActivityId") String terminalActivityId,
			@RequestParam("date") String date, final Model model, final HttpServletRequest request,
			final HttpServletResponse response) {
		model.addAttribute("terminalActivityId", terminalActivityId);
		model.addAttribute("date", date);

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.VIEW_PAGE);
		auditLog.setCreatedBy(user.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setDescription("Buka Halaman Daftar Message Logs Aktifitas Terminal");
		auditLog.setReffNo(date);

		try {
			auditLogService.insertAuditLog(auditLog);
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.debug("Added Audit Log");

		return getPageContent(DETAILS_MESSAGE_LOGS_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_TERMINAL_DETAILS_GET_LIST_MESSAGE_BY_ID_MAPPING, method = RequestMethod.GET)
	public void doSearchMessages(final HttpServletRequest request, final Model model, final HttpServletResponse response)
			throws IOException {
		if (isAjaxRequest(request)) {
			setResponseAsJson(response);

			DataTablesPojo dataTablesPojo = new DataTablesPojo();
			try {
				dataTablesPojo.setDraw(Integer.parseInt(request.getParameter("draw")));
			} catch (NumberFormatException e) {
				dataTablesPojo.setDraw(0);
			}

			final int orderableColumnCount = 2;
			final StringBuffer sbOrder = new StringBuffer();
			LinkedHashMap<String, String> columnMap = new LinkedHashMap<String, String>();
			columnMap.put("0", "raw");

			int offset = 0;
			int limit = 50;
			try {
				offset = Integer.parseInt(request.getParameter("start"));
			} catch (NumberFormatException e) {
			}
			try {
				limit = Integer.parseInt(request.getParameter("length"));
			} catch (NumberFormatException e) {
			}

			for (int i = 0; i < orderableColumnCount; i++) {
				String orderColumn = request.getParameter("order[" + i + "][column]");
				if (orderColumn == null || orderColumn.equalsIgnoreCase("")) {
					break;
				} else {
					orderColumn = columnMap.get(orderColumn);
					if (orderColumn == null) {
						break;
					}
					String orderDir = request.getParameter("order[" + i + "][dir]");
					if (orderDir == null || !orderDir.equalsIgnoreCase("desc")) {
						orderDir = "asc";
					}
					sbOrder.append(orderColumn);
					sbOrder.append("-");
					sbOrder.append(orderDir);
					sbOrder.append("+");
				}
			}

			String date = request.getParameter("date");
			String terminalActivityId = request.getParameter("terminalActivityId");

			try {
				final int finalOffset = offset;
				final int finalLimit = limit;
				Page<MessageLogs> messageLogsListPojo = messageLogsService.search(terminalActivityId, date, columnMap,
						finalOffset, finalLimit);

				List<String[]> data = new ArrayList<>();

				Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				for (MessageLogs messageLogs : messageLogsListPojo) {
					data.add(new String[] { HtmlUtils.htmlEscape(formatter.format(messageLogs.getCreatedDate()).toString()),
							HtmlUtils.htmlEscape(messageLogs.getEndpointCode()), HtmlUtils.htmlEscape(messageLogs.getRequestLabel()),
							HtmlUtils.htmlEscape(new String(messageLogs.getMessageRaw())) });
				}

				dataTablesPojo.setData(data);
				dataTablesPojo.setRecordsFiltered(messageLogsListPojo.getContent().size());
				dataTablesPojo.setRecordsTotal(Integer.parseInt("" + messageLogsListPojo.getTotalElements()));
			} catch (HttpClientErrorException e) {
				log.error(e.getStatusCode().toString(), e);
				dataTablesPojo.setError(getMessage("message.datatables.error.fetch.terminal.activity"));
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				dataTablesPojo.setError(getMessage("message.datatables.error.fetch.terminal.activity"));
			} finally {
				response.getWriter().write(new JsonUtils().toJson(dataTablesPojo));
			}
		} else {
			response.setStatus(HttpServletResponse.SC_FOUND);
		}

	}

}
