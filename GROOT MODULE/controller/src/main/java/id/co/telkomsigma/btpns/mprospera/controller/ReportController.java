package id.co.telkomsigma.btpns.mprospera.controller;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.location.Location;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.service.*;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Controller
public class ReportController extends GenericController {

	private static final String LIST_PAGE_NAME = "manage/report/list";

	@Autowired
	private ReportService reportService;

	@Autowired
	private AuditLogService auditLogService;

	@Autowired
	private SentraService sentraService;
	
	@Autowired
	private AreaService locationService;

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_LIST_GENERATE_REPORT_MAPPING, method = RequestMethod.GET)
	public String showListPage(@ModelAttribute("reportForm") final ReportForm reportForm, BindingResult errors,
		    final Model model, final HttpServletRequest request, final HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.VIEW_PAGE);
		auditLog.setCreatedBy(user.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setDescription("Buka Halaman Generate Report PRS dan NonPRS");
		auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));

		List<Location> kfoList = locationService.findAllKfo();
		model.addAttribute("kfoList", kfoList);
		model.addAttribute("reportForm", new ReportForm());

		try {
			auditLogService.insertAuditLog(auditLog);
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.debug("Added Audit Log");

		return getPageContent(LIST_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_GENERATE_RMK_REPORT_MAPPING, method = RequestMethod.POST)
	public void doDownloadRmkFromOutside(@ModelAttribute("reportForm") final ReportForm reportForm, BindingResult errors,
		    final Model model, final HttpServletRequest request, final HttpServletResponse response) {
		super.setResponseAsZip(response);
		log.debug(reportForm.toString());
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.GENERATE_REPORT);
		auditLog.setCreatedBy(user.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setDescription("Generate Report RMK M-Prospera for MMS " + reportForm.getMmsName() + " and date " + new Date());
		auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));
		String fileName = "report_RMK_M-Prospera_" + reportForm.getMmsName();
		response.addHeader("content-disposition", "attachment; filename=\"" + fileName + ".zip\"");
		log.debug("File PDF : "+fileName);
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			List<Location> locationList = new ArrayList<Location>();

			Location kfoLocation = locationService.findLocationById(reportForm.getKfoLocationId());
			if(reportForm.getMmsLocationId().trim().equalsIgnoreCase("0"))
				locationList.addAll(locationService.findByParentLocation(reportForm.getKfoLocationId()));
			else
				locationList.add(locationService.findLocationById(reportForm.getMmsLocationId()));
			
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date prsDate = formatter.parse(reportForm.getInputDate()+" 00:00:00");
	
			OutputStream os = response.getOutputStream();
			ZipOutputStream zip = new ZipOutputStream(os);
				
			for(int i=0;i<locationList.size();i++) {
				Location mmsLocation = locationList.get(i);
				parameters.put("mms", mmsLocation.getLocationCode()+" - "+mmsLocation.getName());
				parameters.put("kfo", kfoLocation.getLocationCode()+" - "+kfoLocation.getName());
				parameters.put("mmsId", mmsLocation.getLocationId());
				parameters.put("startDate", new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));			
				parameters.put("prsDate",prsDate);
				for(Map.Entry<String, Object> entry : parameters.entrySet())
					log.debug("Key : "+entry.getKey()+", Value : "+entry.getValue());
								
				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
				if(reportForm.getExtType().equalsIgnoreCase("pdf"))
					reportService.generatePdf("reportRMK.jrxml", parameters, byteArrayOutputStream);
				else if(reportForm.getExtType().equalsIgnoreCase("csv"))
					reportService.generateCsv("reportRMK.jrxml", parameters, byteArrayOutputStream);

				byte[] bniBytes = byteArrayOutputStream.toByteArray();
				ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bniBytes);
				if(reportForm.getExtType().equalsIgnoreCase("pdf"))
					zip.putNextEntry(new ZipEntry(mmsLocation.getName() + ".pdf"));
				else if(reportForm.getExtType().equalsIgnoreCase("csv"))
					zip.putNextEntry(new ZipEntry(mmsLocation.getName() + ".csv"));

				int bytesRead;
				byte[] buffer = new byte[1024];
				while ((bytesRead = byteArrayInputStream.read(buffer)) > 0) {
					zip.write(buffer, 0, bytesRead);
				}
				zip.closeEntry();
			}
			zip.close();
			os.flush();
			os.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_GENERATE_RMK_REPORT_AJAX_MAPPING, method = RequestMethod.GET)
	public void ajaxGetMmsRmkByKfo(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		if (isAjaxRequest(request)) {
			setResponseAsJson(response);
			String kfoId = request.getParameter("kfo");
			Location all = new Location();
			all.setName("All MMS");
			all.setLocationId("0");
			List<Location> mmsList = new ArrayList<Location>();
			mmsList.add(all);
			mmsList.addAll(locationService.findByParentLocation(kfoId));
			response.getWriter().write(new JsonUtils().toJson(mmsList));
		} else {
			response.setStatus(HttpServletResponse.SC_FOUND);
		}
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_GENERATE_DMK_REPORT_MAPPING, method = RequestMethod.POST)
	public void doDownloadDmkFromOutside(@ModelAttribute("reportForm") final ReportForm reportForm, BindingResult errors,
		    final Model model, final HttpServletRequest request, final HttpServletResponse response) {
		super.setResponseAsZip(response);
		log.debug(reportForm.toString());
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();
		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.GENERATE_REPORT);
		auditLog.setCreatedBy(user.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setDescription("Generate Report DMK M-Prospera for MMS " + reportForm.getMmsName() + " and date " + new Date());
		auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));
		String fileName = "report_DMK_M-Prospera_" + reportForm.getMmsName();
		response.addHeader("content-disposition", "attachment; filename=\"" + fileName + ".zip\"");
		try {
			Map<String, Object> parameters = new HashMap<String, Object>();
			List<Location> locationList = new ArrayList<Location>();

			Location kfoLocation = locationService.findLocationById(reportForm.getKfoLocationId());
			if(reportForm.getMmsLocationId().trim().equalsIgnoreCase("0"))
				locationList.addAll(locationService.findByParentLocation(reportForm.getKfoLocationId()));
			else
				locationList.add(locationService.findLocationById(reportForm.getMmsLocationId()));
			
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date prsDate = formatter.parse(reportForm.getInputDate()+" 00:00:00");

			OutputStream os = response.getOutputStream();
			ZipOutputStream zip = new ZipOutputStream(os);
			
			for(int i=0;i<locationList.size();i++) {
				Location mmsLocation = locationList.get(i);
				parameters.put("mms", mmsLocation.getLocationCode()+" - "+mmsLocation.getName());
				parameters.put("kfo", kfoLocation.getLocationCode()+" - "+kfoLocation.getName());
				parameters.put("mmsId", mmsLocation.getLocationId());
				parameters.put("startDate", new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date()));			
				parameters.put("prsDate",prsDate);
				for(Map.Entry<String, Object> entry : parameters.entrySet())
					log.debug("Key : "+entry.getKey()+", Value : "+entry.getValue());
								
				ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
				if(reportForm.getExtType().equalsIgnoreCase("pdf"))
					reportService.generatePdf("reportDMK.jrxml", parameters, byteArrayOutputStream);
				else if(reportForm.getExtType().equalsIgnoreCase("csv"))
					reportService.generateCsv("reportDMK.jrxml", parameters, byteArrayOutputStream);

				byte[] bniBytes = byteArrayOutputStream.toByteArray();
				ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bniBytes);
				if(reportForm.getExtType().equalsIgnoreCase("pdf"))
					zip.putNextEntry(new ZipEntry(mmsLocation.getName() + ".pdf"));
				else if(reportForm.getExtType().equalsIgnoreCase("csv"))
					zip.putNextEntry(new ZipEntry(mmsLocation.getName() + ".csv"));

				int bytesRead;
				byte[] buffer = new byte[1024];
				while ((bytesRead = byteArrayInputStream.read(buffer)) > 0) {
					zip.write(buffer, 0, bytesRead);
				}
				zip.closeEntry();
			}
			zip.close();
			os.flush();
			os.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_GENERATE_DMK_REPORT_AJAX_MAPPING, method = RequestMethod.GET)
	public void ajaxGetMmsDmkByKfo(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		if (isAjaxRequest(request)) {
			setResponseAsJson(response);
			String kfoId = request.getParameter("kfo");			
			Location all = new Location();
			all.setName("All MMS");
			all.setLocationId("0");
			List<Location> mmsList = new ArrayList<Location>();
			mmsList.add(all);
			mmsList.addAll(locationService.findByParentLocation(kfoId));
			response.getWriter().write(new JsonUtils().toJson(mmsList));			
		} else {
			response.setStatus(HttpServletResponse.SC_FOUND);
		}
	}
}
