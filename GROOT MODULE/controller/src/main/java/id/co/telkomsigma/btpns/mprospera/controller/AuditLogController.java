package id.co.telkomsigma.btpns.mprospera.controller;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.controller.GenericController;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.pojo.DataTablesPojo;
import id.co.telkomsigma.btpns.mprospera.service.AuditLogService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Dzulfiqar on 11/13/15.
 */
@Controller
public class AuditLogController extends GenericController {
    // ----------------------------------------- LIST PARAMETER SECTION
    // ----------------------------------------- //
    private static final String LIST_PAGE_NAME = "auditLog/list";
    private static final String LIST_COMMAND = "redirect:" + WebGuiConstant.AFTER_LOGIN_PATH_AUDIT_LOG;

    @Autowired
    private AuditLogService auditLogService;

    @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_AUDIT_LOG_MAPPING, method = RequestMethod.GET)
    public String showListPage(final Model model, final HttpServletRequest request, final HttpServletResponse response) {
        List<AuditLog> auditLog = auditLogService.getLogs();
        model.addAttribute("auditLog", auditLog);

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) auth.getPrincipal();

        String date = new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date());

        AuditLog auditLogs = new AuditLog();
        auditLogs.setActivityType(auditLogs.VIEW_PAGE);
        auditLogs.setCreatedBy(user.getUsername());
        auditLogs.setCreatedDate(new Date());
        auditLogs.setDescription("Buka Halaman AuditLog");
        auditLogs.setReffNo(date);

        try {
            auditLogService.insertAuditLog(auditLogs);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        log.debug("Added Audit Log");

        return getPageContent(LIST_PAGE_NAME);
    }

    @RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_AUDIT_LOG_GET_LIST_MAPPING, method = RequestMethod.GET)
    public void doSearch(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
        if (isAjaxRequest(request)) {
            setResponseAsJson(response);

            DataTablesPojo dataTablesPojo = new DataTablesPojo();
            try {
                dataTablesPojo.setDraw(Integer.parseInt(request.getParameter("draw")));
            } catch (NumberFormatException e) {
                dataTablesPojo.setDraw(0);
            }

            final int orderableColumnCount = 5;
            final StringBuffer sbOrder = new StringBuffer();
            LinkedHashMap<String, String> columnMap = new LinkedHashMap<String, String>();
            columnMap.put("0", "activity_type");
            columnMap.put("1", "description");
            columnMap.put("2", "createdBy");
            columnMap.put("3", "createdDate");
            columnMap.put("4", "reffNo");
            int offset = 0;
            int limit = 50;
            try {
                offset = Integer.parseInt(request.getParameter("start"));
            } catch (NumberFormatException e) {
            }
            try {
                limit = Integer.parseInt(request.getParameter("length"));
            } catch (NumberFormatException e) {
            }

            for (int i = 0; i < orderableColumnCount; i++) {
                String orderColumn = request.getParameter("order[" + i + "][column]");
                if (orderColumn == null || orderColumn.equalsIgnoreCase("")) {
                    break;
                } else {
                    orderColumn = columnMap.get(orderColumn);
                    if (orderColumn == null) {
                        break;
                    }
                    String orderDir = request.getParameter("order[" + i + "][dir]");
                    if (orderDir == null || !orderDir.equalsIgnoreCase("desc")) {
                        orderDir = "asc";
                    }
                    sbOrder.append(orderColumn);
                    sbOrder.append("-");
                    sbOrder.append(orderDir);
                    sbOrder.append("+");
                }
            }

            Log log = LogFactory.getLog(this.getClass());
            String keyword = request.getParameter("q");
            log.debug("keyword " + keyword);
            String startDate = request.getParameter("c");
            log.debug("startDate " + startDate);
            String endDate = request.getParameter("b");
            log.debug("endDate " + endDate);
            String activityType = request.getParameter("a");
            log.debug("activityType " + activityType);

            if (keyword == null)
                keyword = "";
            if (startDate == null)
                startDate = "";
            if (endDate == null)
                endDate = "";
            if (activityType == null)
                activityType = "";

            /*
             * SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
             * if(startDate != null && endDate != null){ try { Long tanggalMulai
             * = format.parse(startDate).getTime(); Long tanggalAkhir =
             * format.parse(endDate).getTime(); if(tanggalMulai > tanggalAkhir){
             * log.debug("Wrong Date Range");
             * dataTablesPojo.setError(getMessage("field.validation.date")); } }
             * catch (ParseException e1) { // TODO Auto-generated catch block
             * e1.printStackTrace(); } }
             */

            try {
                final int finalOffset = offset;
                final int finalLimit = limit;

                Page<AuditLog> auditLogPojo = auditLogService.searchAuditLog(keyword, startDate, endDate, activityType,
                        columnMap, finalOffset, finalLimit);

                List<String[]> data = new ArrayList<>();
                Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                for (AuditLog audit : auditLogPojo) {

                    int tipeAktifitas = audit.getActivityType();
                    String namaAktifitas = new String();
                    if (tipeAktifitas == 1)
                        namaAktifitas = "Login/Logout Web";
                    else if (tipeAktifitas == 2)
                        namaAktifitas = "Buka Halaman";
                    else if (tipeAktifitas == 3)
                        namaAktifitas = "Tambah atau Ubah data";
                    else if (tipeAktifitas == 4)
                        namaAktifitas = "Hapus Cache";
                    else if (tipeAktifitas == 5)
                        namaAktifitas = "Login/Logout Mobile";
                    else if (tipeAktifitas == 6)
                        namaAktifitas = "Modifikasi Data SDA";
                    else if (tipeAktifitas == 7)
                        namaAktifitas = "Modifikasi Data PM";
                    else if (tipeAktifitas == 8)
                        namaAktifitas = "Modifikasi Data SW";
                    else if (tipeAktifitas == 12)
                        namaAktifitas = "Modifikasi Data Nasabah";
                    else if (tipeAktifitas == 13)
                        namaAktifitas = "Modifikasi Data Loan";
                    else if (tipeAktifitas == 17)
                        namaAktifitas = "Reset Password";
                    else if (tipeAktifitas == 11)
                        namaAktifitas = "Modifikasi Data PDK";
                    else if (tipeAktifitas == 10)
                        namaAktifitas = "Modifikasi Data Grup Sentra";
                    else if (tipeAktifitas == 9)
                        namaAktifitas = "Modifikasi Data Sentra";
                    else if (tipeAktifitas == 15)
                        namaAktifitas = "Modifikasi Data Survey";
                    else if (tipeAktifitas == 16)
                        namaAktifitas = "Request Token";
                    else if (tipeAktifitas == 14)
                        namaAktifitas = "Modifikasi Data MM";
                    else if (tipeAktifitas == 19)
                        namaAktifitas = "Generating Report";
                    data.add(new String[]{HtmlUtils.htmlEscape(namaAktifitas), HtmlUtils.htmlEscape(audit.getDescription()), HtmlUtils.htmlEscape(audit.getCreatedBy()),
                            HtmlUtils.htmlEscape(formatter.format(audit.getCreatedDate()).toString()), HtmlUtils.htmlEscape(audit.getReffNo())});
                }

                dataTablesPojo.setData(data);
                dataTablesPojo.setRecordsFiltered(Integer.parseInt("" + auditLogPojo.getTotalElements()));
                dataTablesPojo.setRecordsTotal(Integer.parseInt("" + auditLogPojo.getTotalElements()));

            } catch (HttpClientErrorException e) {
                log.error(e.getStatusCode().toString(), e);
                dataTablesPojo.setError(getMessage("message.datatables.error.fetch.user"));
            } catch (Exception e) {
                log.error(e.getMessage(), e);
                dataTablesPojo.setError(getMessage("message.datatables.error.fetch.user"));
            } finally {
                response.getWriter().write(new JsonUtils().toJson(dataTablesPojo));

            }
        } else {
            response.setStatus(HttpServletResponse.SC_FOUND);
        }
    }
}