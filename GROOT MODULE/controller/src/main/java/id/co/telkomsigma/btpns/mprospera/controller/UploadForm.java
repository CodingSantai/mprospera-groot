package id.co.telkomsigma.btpns.mprospera.controller;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by fujitsuPC on 5/16/2017.
 */
public class UploadForm {

    private String name;
    private MultipartFile file;

    public UploadForm(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
        this.name = file.getOriginalFilename();
    }
}
