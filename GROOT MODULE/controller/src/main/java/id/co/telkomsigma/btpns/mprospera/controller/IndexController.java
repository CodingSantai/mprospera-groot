package id.co.telkomsigma.btpns.mprospera.controller;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.user.Role;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by daniel on 3/31/15.
 */
@Controller
public class IndexController {
	@Autowired
	private RoleService roleService;

	@RequestMapping({ "/", "/index.html" })
	public String index(final Model model, final HttpServletRequest request, final HttpServletResponse response) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = null;
		if (auth.getPrincipal() instanceof User) {
			user = (User) auth.getPrincipal();
		}
		if (user == null) {
			return "content/index";
		}
		List<Role> roles = roleService.getRoles();
		model.addAttribute("roles", roles);

		String homePageUrl = WebGuiConstant.AFTER_LOGIN_PATH_HOME_PAGE;

		return "redirect:" + homePageUrl;
	}

}