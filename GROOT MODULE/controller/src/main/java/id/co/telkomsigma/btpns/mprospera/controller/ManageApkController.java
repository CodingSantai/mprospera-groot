package id.co.telkomsigma.btpns.mprospera.controller;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.manageApk.ManageApk;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.pojo.DataTablesPojo;
import id.co.telkomsigma.btpns.mprospera.pojo.UploadApkForm;
import id.co.telkomsigma.btpns.mprospera.service.AuditLogService;
import id.co.telkomsigma.btpns.mprospera.service.ManageApkService;
import id.co.telkomsigma.btpns.mprospera.util.FormValidationUtil;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

@Controller
public class ManageApkController extends GenericController {

	private static final String MANAGE_APK_PAGE_NAME = "manage/apk/list";

	@Autowired
	private ManageApkService manageApkService;

	@Autowired
	private AuditLogService auditLogService;

	@Autowired
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_APK_MAPPING, method = RequestMethod.GET)
	public String showPage(final Model model, final HttpServletRequest request, final HttpServletResponse response) {

		List<ManageApk> manageApk = manageApkService.getAll();

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();

		String date = new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date());

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.VIEW_PAGE);
		auditLog.setCreatedBy(user.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setDescription("Buka Halaman Manage Apk ");
		auditLog.setReffNo(date);

		try {
			auditLogService.insertAuditLog(auditLog);
		} catch (Exception e) {
			e.printStackTrace();
		}
		model.addAttribute("manageApk", manageApk);
		model.addAttribute("enable", true);
		return getPageContent(MANAGE_APK_PAGE_NAME);

	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_APK_GET_LIST_MAPPING, method = RequestMethod.GET)
	public void doSearch(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		if (isAjaxRequest(request)) {
			setResponseAsJson(response);

			DataTablesPojo dataTablesPojo = new DataTablesPojo();
			try {
				dataTablesPojo.setDraw(Integer.parseInt(request.getParameter("draw")));
			} catch (NumberFormatException e) {
				dataTablesPojo.setDraw(0);
			}

			final int orderableColumnCount = 1;
			final StringBuffer sbOrder = new StringBuffer();
			LinkedHashMap<String, String> columnMap = new LinkedHashMap<String, String>();
			columnMap.put("0", "createdDate");

			int offset = 0;
			int limit = 50;
			try {
				offset = Integer.parseInt(request.getParameter("start"));
			} catch (NumberFormatException e) {
			}
			try {
				limit = Integer.parseInt(request.getParameter("length"));
			} catch (NumberFormatException e) {
			}

			for (int i = 0; i < orderableColumnCount; i++) {
				String orderColumn = request.getParameter("order[" + i + "][column]");
				if (orderColumn == null || orderColumn.equalsIgnoreCase("")) {
					break;
				} else {
					orderColumn = columnMap.get(orderColumn);
					if (orderColumn == null) {
						break;
					}
					String orderDir = request.getParameter("order[" + i + "][dir]");
					if (orderDir == null || !orderDir.equalsIgnoreCase("desc")) {
						orderDir = "asc";
					}
					sbOrder.append(orderColumn);
					sbOrder.append("-");
					sbOrder.append(orderDir);
					sbOrder.append("+");
				}
			}

			try {
				final int finalOffset = offset;
				final int finalLimit = limit;
				Page<ManageApk> manageApkListPojo = manageApkService.search(columnMap, finalOffset, finalLimit);

				List<String[]> data = new ArrayList<>();

				Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

				for (ManageApk manageApk : manageApkListPojo) {
					String url = "";
					if (manageApk.isAllowedVersion() == false) {
						url = url
								+ "<a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\" data-event=\"disable\" data-username=\""
								+ HtmlUtils.htmlEscape(manageApk.getApkVersion())
								+ "\" data-link=\""
								+ getServletContext().getContextPath()
								+ WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_APK_ALLOWED_REQUEST.replace("{apkId}", ""
										+ manageApk.getApkId())
								+ "\" data-id=\""
								+ manageApk.getApkId()
								+ "\"><i class='fa fa-ban'> <div class='action' id='action-ban'>nonaktif</div></i></a>  ";
					} else if (manageApk.isAllowedVersion() == true) {
						url = url
								+ "<a href=\"#\" data-toggle=\"modal\" data-target=\"#myModal\" data-event=\"enable\" data-username=\""
								+ HtmlUtils.htmlEscape(manageApk.getApkVersion())
								+ "\" data-link=\""
								+ getServletContext().getContextPath()
								+ WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_APK_NOT_ALLOWED_REQUEST.replace("{apkId}", ""
										+ manageApk.getApkId())
								+ "\" data-id=\""
								+ manageApk.getApkId()
								+ "\"><i class='fa fa-check'> <div class='action' id='action-check'>aktif</div></i></a>  ";
					}
					data.add(new String[] {
							"<a href=\""
									+ getServletContext().getContextPath()
									+ WebGuiConstant.AFTER_LOGIN_PATH_APK_GET_APK_REQUEST.replace("{apkId}", ""
											+ manageApk.getApkId()) + "\">" + HtmlUtils.htmlEscape(manageApk.getApkVersion()) + "</a>",
									HtmlUtils.htmlEscape(manageApk.getCreatedBy().getUsername()), HtmlUtils.htmlEscape(formatter.format(manageApk.getCreatedDate())),
									HtmlUtils.htmlEscape(manageApk.getChangeDescription()), url });
				}

				dataTablesPojo.setData(data);
				dataTablesPojo.setRecordsFiltered(Integer.parseInt("" + manageApkListPojo.getTotalElements()));
				dataTablesPojo.setRecordsTotal(Integer.parseInt("" + manageApkListPojo.getTotalElements()));
			} catch (HttpClientErrorException e) {
				log.error(e.getStatusCode().toString(), e);
				dataTablesPojo.setError(getMessage("message.datatables.error.fetch.manageApk"));
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				dataTablesPojo.setError(getMessage("message.datatables.error.fetch.manageApk"));
			} finally {
				response.getWriter().write(new JsonUtils().toJson(dataTablesPojo));
			}
		} else {
			response.setStatus(HttpServletResponse.SC_FOUND);
		}

	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_APK_MAPPING, method = { RequestMethod.POST })
	public String doUpload(@ModelAttribute("uploadApkForm") final UploadApkForm uploadApkForm, BindingResult errors,
			final Model model, final HttpServletRequest request, final HttpServletResponse response) {

		String error = "";
		String errorMsg = null;

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		final User admin = (User) auth.getPrincipal();

		final String version;

		if (uploadApkForm.getFile().getOriginalFilename().isEmpty()) {
			errorMsg = "noData";
		} else {
			version = uploadApkForm.getVersion();

			ManageApk manageApk = manageApkService.getByVersion(version);

			if (null != manageApk) {
				errorMsg = "dataExist";
				error = version;

			} else {
				try {
					final File apkFile = new File(uploadApkForm.getFile().getOriginalFilename());

					// cek filenya ada .apk bukan
					String ext = FilenameUtils.getExtension(uploadApkForm.getFile().getOriginalFilename());
					if (!ext.equals("apk")) {
						errorMsg = "notApk";
						error = uploadApkForm.getFile().getOriginalFilename();
					} else {
						apkFile.createNewFile();
						try(FileOutputStream fos = new FileOutputStream(apkFile)){
							fos.write(uploadApkForm.getFile().getBytes());
						}catch (Exception e){
							e.printStackTrace();
						}

						final List<String> data = new ArrayList<String>();
						final byte[] apkBytes = uploadApkForm.getFile().getBytes();

						final String description = uploadApkForm.getDescription();
						String apkFileName = apkFile.getName();
						String line = "";
						try(BufferedReader br = new BufferedReader(new FileReader(apkFileName))){
							while ((line = br.readLine()) != null) {
								if (line.equals(""))
									continue;
								data.add(line);
							}
						}catch (Exception e){
							e.printStackTrace();
						}

						if (data.size() == 0) {
							errorMsg = "empty";
						} else if (uploadApkForm.getVersion().isEmpty()) {
							errorMsg = "emptyVersion";
						} else if (uploadApkForm.getDescription().isEmpty()) {
							errorMsg = "emptyDesc";
						} else {

							// panggil Thread
							threadPoolTaskExecutor.execute(new Runnable() {

								@Override
								public void run() {
									try {
										manageApkService.uploadApk(apkBytes, version, description, admin);
									} catch (Exception e) {
										e.printStackTrace();
										model.addAttribute("errorMessage", getMessage("form.error.upload.empty"));
									}
								}
							});
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
					model.addAttribute("errorMessage", getMessage("form.error.upload.empty"));
				}
			}

		}

		if (null == errorMsg) {
			model.addAttribute("okMessage", getMessage("form.progress.upload.apk.start"));
			model.addAttribute("enable", true);
		} else if (errorMsg.equals("notApk")) {
			model.addAttribute("errorMessage", getMessage("form.error.upload.apk.notApk", error));
			model.addAttribute("enable", true);
		} else if (errorMsg.equals("empty")) {
			model.addAttribute("errorMessage", getMessage("form.error.upload.apk.empty"));
			model.addAttribute("enable", true);
		} else if (errorMsg.equals("dataExist")) {
			model.addAttribute("errorMessage", getMessage("form.error.upload.apk.dataExist", error));
			model.addAttribute("enable", true);
		} else if (errorMsg.equals("emptyVersion")) {
			model.addAttribute("errorMessage", getMessage("form.error.upload.apk.emptyVersion"));
			model.addAttribute("enable", true);
		} else if (errorMsg.equals("emptyDesc")) {
			model.addAttribute("errorMessage", getMessage("form.error.upload.apk.emptyDesc"));
			model.addAttribute("enable", true);
		} else if (errorMsg.equals("noData")) {
			model.addAttribute("errorMessage", getMessage("form.error.upload.apk.noData"));
			model.addAttribute("enable", true);
		}

		return getPageContent(MANAGE_APK_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_APK_GET_APK_MAPPING, method = { RequestMethod.GET })
	public void doDownload(final HttpServletRequest request, final HttpServletResponse response,
			@RequestParam(value = "apkId", required = true) Long apkId) {
		setResponseAsZip(response);
		OutputStream os;
		try {
			os = response.getOutputStream();
			ManageApk manageApk = manageApkService.getById(apkId);
			manageApk.setName("mProspera-v" + HtmlUtils.htmlEscape(manageApk.getApkVersion()) + ".apk");
			response.addHeader("content-disposition", "attachment;filename=" + HtmlUtils.htmlEscape(manageApk.getName()));
			os.write(manageApk.getApkFile());
			os.flush();
			os.close();
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}

	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_APK_NOT_ALLOWED_MAPPING, method = { RequestMethod.GET })
	public String doNotAllowed(@RequestParam(value = "apkId", required = true) final Long apkId,
			@ModelAttribute("apk") ManageApk apk, BindingResult errors, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User admin = (User) auth.getPrincipal();

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.SAVE_OR_UPDATE);
		auditLog.setCreatedBy(admin.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));

		try {

			ManageApk apkToEdit = manageApkService.getById(apkId);
			FormValidationUtil.validatingNotAllowedApk(errors, "", apkToEdit, "");
			apkToEdit.setAllowedVersion(false);
			manageApkService.update(apkToEdit);

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			model.addAttribute("errorMessage", getMessage("form.error.notAllowedVer.apk"));
			auditLog.setDescription("Men-non-allow apk versi  " + apk.getApkVersion() + " gagal");
			try {
				auditLogService.insertAuditLog(auditLog);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return getPageContent(MANAGE_APK_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_APK_ALLOWED_MAPPING, method = { RequestMethod.GET })
	public String doAllowed(@RequestParam(value = "apkId", required = true) final Long apkId,
			@ModelAttribute("apk") ManageApk apk, BindingResult errors, final Model model,
			final HttpServletRequest request, final HttpServletResponse response) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User admin = (User) auth.getPrincipal();

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.SAVE_OR_UPDATE);
		auditLog.setCreatedBy(admin.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));

		try {

			ManageApk apkToEdit = manageApkService.getById(apkId);
			FormValidationUtil.validatingAllowedApk(errors, "", apkToEdit, "");
			apkToEdit.setAllowedVersion(true);
			manageApkService.update(apkToEdit);

		} catch (Exception e) {
			log.error(e.getMessage(), e);
			model.addAttribute("errorMessage", getMessage("form.error.allowedVer.apk"));
			auditLog.setDescription("Men-allow apk versi  " + apk.getApkVersion() + " gagal");
			try {
				auditLogService.insertAuditLog(auditLog);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		return getPageContent(MANAGE_APK_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_APK_GET_APK_MAPPING_OUTSIDE, method = { RequestMethod.GET })
	public void doDownloadFromOutside(final HttpServletRequest request, final HttpServletResponse response,
			@RequestParam(value = "version", required = true) String version) {
		setResponseAsZip(response);
		OutputStream os;
		try {
			os = response.getOutputStream();
			ManageApk manageApk = manageApkService.getByVersion(version);
			manageApk.setName("mProspera-v" + HtmlUtils.htmlEscape(manageApk.getApkVersion()) + ".apk");
			response.addHeader("content-disposition", "attachment;filename=" + HtmlUtils.htmlEscape(manageApk.getName()));
			os.write(manageApk.getApkFile());
			os.flush();
			os.close();
		} catch (IOException e) {
			log.error(e.getMessage(), e);
		}
	}
}
