package id.co.telkomsigma.btpns.mprospera.controller;

import com.hazelcast.core.IMap;
import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.springbatch.BatchJobExecution;
import id.co.telkomsigma.btpns.mprospera.model.springbatch.BatchStepExecution;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.pojo.DataTablesPojo;
import id.co.telkomsigma.btpns.mprospera.service.AuditLogService;
import id.co.telkomsigma.btpns.mprospera.service.EODService;
import id.co.telkomsigma.btpns.mprospera.service.SpringBatchService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.data.domain.Page;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller("dashboardETLController")
public class DashboardETLController extends GenericController {

	private static final String JobListExecutionPage = "manage/dashboard/listJobExecution";
	private static final String StepListExecutionPage = "manage/dashboard/listStepExecution";

	@Autowired
	private SpringBatchService springBatchService;

	@Autowired
	private AuditLogService auditLogService;

	@Autowired
	private EODService eodService;


	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_DASHBOARDETL_JOBEXECUTION_MAPPING, method = RequestMethod.GET)
	public String showJobListExecutionPage(final Model model, final HttpServletRequest request,
			final HttpServletResponse response) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.VIEW_PAGE);
		auditLog.setCreatedBy(user.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setDescription("Buka Halaman Dashboard ETL");
		auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));

		try {
			auditLogService.insertAuditLog(auditLog);
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.debug("Added Audit Log");
		addEodStatus(model);
		return getPageContent(JobListExecutionPage);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_DASHBOARDETL_JOBEXECUTION_GET_LIST_MAPPING, method = RequestMethod.GET)
	public void doSearchJobExecution(final Model model, final HttpServletRequest request,
			final HttpServletResponse response) throws IOException {
		if (isAjaxRequest(request)) {
			setResponseAsJson(response);
			DataTablesPojo dataTablesPojo = new DataTablesPojo();
			try {
				dataTablesPojo.setDraw(Integer.parseInt(request.getParameter("draw")));
			} catch (NumberFormatException e) {
				dataTablesPojo.setDraw(0);
			}

			String startDate = request.getParameter("startDate");
			if ("".equals(startDate))
				startDate = null;
			String endDate = request.getParameter("endDate");
			if ("".equals(endDate))
				endDate = null;
			int offset = 0;
			int limit = 50;
			try {
				offset = Integer.parseInt(request.getParameter("start"));
			} catch (NumberFormatException e) {
			}
			try {
				limit = Integer.parseInt(request.getParameter("length"));
			} catch (NumberFormatException e) {
			}

			List<String[]> data = new ArrayList<>();

			Page<BatchJobExecution> batchJobList;
			try {
				final int finalOffset = offset;
				final int finalLimit = limit;
				batchJobList = springBatchService.getAllBatchJobExecution(startDate, endDate, finalOffset, finalLimit);
				Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				for (BatchJobExecution batchJob : batchJobList) {
					String startTime = "";
					String endTime = "";
					if (batchJob.getStartTime() != null)
						startTime = formatter.format(batchJob.getStartTime());
					if (batchJob.getEndTime() != null)
						endTime = formatter.format(batchJob.getEndTime());
					data.add(new String[] {
							HtmlUtils.htmlEscape(batchJob.getJobInstance().getJobName()),
							HtmlUtils.htmlEscape(startTime),
							HtmlUtils.htmlEscape(endTime),
							HtmlUtils.htmlEscape(batchJob.getStatus()),
							"<a href=\""
									+ getServletContext().getContextPath()
									+ WebGuiConstant.AFTER_LOGIN_PATH_DASHBOARDETL_STEPEXECUTION_REQUEST.replace(
											"{jobExecutionId}", "" + batchJob.getJobExecutionId())
									+ "\"><i class='fa fa-pencil'><div class='action' id='action-pencil'>View Detail</div></i></a>" });
				}

				dataTablesPojo.setData(data);
				dataTablesPojo.setRecordsFiltered(Integer.parseInt("" + batchJobList.getTotalElements()));
				dataTablesPojo.setRecordsTotal(Integer.parseInt("" + batchJobList.getTotalElements()));
			} catch (Exception e) {
				log.error(e);
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			} finally {
				response.getWriter().write(new JsonUtils().toJson(dataTablesPojo));
			}
		} else {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_DASHBOARDETL_STEPEXECUTION_MAPPING, method = RequestMethod.GET)
	public String showStepListExecutionPage(final Model model, final HttpServletRequest request,
			final HttpServletResponse response, @RequestParam(value = "jobExecutionId") String jobExecutionId) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.VIEW_PAGE);
		auditLog.setCreatedBy(user.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setDescription("Buka Halaman Detail Job Dashboard ETL");
		auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));

		try {
			auditLogService.insertAuditLog(auditLog);
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.debug("Added Audit Log");
		model.addAttribute("jobExecutionId", jobExecutionId);
		return getPageContent(StepListExecutionPage);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_DASHBOARDETL_STEPEXECUTION_GET_LIST_MAPPING, method = RequestMethod.GET)
	public void doSearchStepExecution(final Model model, final HttpServletRequest request,
			final HttpServletResponse response) throws IOException {
		if (isAjaxRequest(request)) {
			setResponseAsJson(response);
			DataTablesPojo dataTablesPojo = new DataTablesPojo();
			try {
				dataTablesPojo.setDraw(Integer.parseInt(request.getParameter("draw")));
			} catch (NumberFormatException e) {
				dataTablesPojo.setDraw(0);
			}

			Long jobExecutionId = Long.parseLong(request.getParameter("jobExecutionId"));
			int offset = 0;
			int limit = 50;
			try {
				offset = Integer.parseInt(request.getParameter("start"));
			} catch (NumberFormatException e) {
			}
			try {
				limit = Integer.parseInt(request.getParameter("length"));
			} catch (NumberFormatException e) {
			}

			List<String[]> data = new ArrayList<>();

			Page<BatchStepExecution> batchJobList;
			try {
				batchJobList = springBatchService.getStepExecution(jobExecutionId, offset, limit);
				Format formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				for (BatchStepExecution batchJob : batchJobList) {
					String startTime = "";
					String endTime = "";
					if (batchJob.getStartTime() != null)
						startTime = formatter.format(batchJob.getStartTime());
					if (batchJob.getEndTime() != null)
						endTime = formatter.format(batchJob.getEndTime());
					data.add(new String[] { HtmlUtils.htmlEscape(batchJob.getStepName()), HtmlUtils.htmlEscape(startTime), HtmlUtils.htmlEscape(endTime), HtmlUtils.htmlEscape(""+batchJob.getReadCount()),
							HtmlUtils.htmlEscape("" + batchJob.getWriteCount()), HtmlUtils.htmlEscape(batchJob.getStatus()), HtmlUtils.htmlEscape(batchJob.getExitMessage()) });
				}

				dataTablesPojo.setData(data);
				dataTablesPojo.setRecordsFiltered(Integer.parseInt("" + batchJobList.getTotalElements()));
				dataTablesPojo.setRecordsTotal(Integer.parseInt("" + batchJobList.getTotalElements()));
			} catch (Exception e) {
				log.error(e);
				response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			} finally {
				response.getWriter().write(new JsonUtils().toJson(dataTablesPojo));
			}
		} else {
			response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
		}
	}

	@Autowired
	CacheManager cacheManager;


	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_DASHBOARDETL_JOBEXECUTION_RERUN_MAPPING, method = RequestMethod.POST)
	public String rerunETL(final Model model, final HttpServletRequest request, final HttpServletResponse response) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.VIEW_PAGE);
		auditLog.setCreatedBy(user.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setDescription("Manual Run Process EOD mPROSPERA");
		auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));

		try {
			auditLogService.insertAuditLog(auditLog);
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.debug("Added Audit Log");

		if (!this.isEodRunning()) {
			log.trace("Run EOD");
			eodService.runEod();
		}

		addEodStatus(model);
		return getPageContent(JobListExecutionPage);
	}

	private boolean isEodRunning() {
		IMap map = (IMap) cacheManager.getCache("eod").getNativeCache();
		String state = "" + map.get("state");
		if (state == null || "".equals(state) || "null".equals(state))
			state = "not-running";
		if ("running".equals(state))
			return true;
		return false;
	}

	private void addEodStatus(final Model model) {
		IMap map = (IMap) cacheManager.getCache("eod").getNativeCache();
		String state = "" + map.get("state");
		if (state == null || "".equals(state) || "null".equals(state))
			state = "not-running";
		model.addAttribute("eodState", state);
	}
}
