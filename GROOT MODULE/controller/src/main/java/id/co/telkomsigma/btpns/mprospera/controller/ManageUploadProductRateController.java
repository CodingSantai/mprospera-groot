package id.co.telkomsigma.btpns.mprospera.controller;

import id.co.telkomsigma.btpns.mprospera.constant.WebGuiConstant;
import id.co.telkomsigma.btpns.mprospera.model.security.AuditLog;
import id.co.telkomsigma.btpns.mprospera.model.sw.LoanProduct;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.pojo.DataTablesPojo;
import id.co.telkomsigma.btpns.mprospera.service.AuditLogService;
import id.co.telkomsigma.btpns.mprospera.service.LoanMappingProductService;
import id.co.telkomsigma.btpns.mprospera.service.UploadService;
import id.co.telkomsigma.btpns.mprospera.util.JsonUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.util.HtmlUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;

@Controller
public class ManageUploadProductRateController extends GenericController {

	private static final String UPLOAD_PAGE_NAME = "upload/uploadproductratecsv";

	@Autowired
	private UploadService uploadService;
	
    @Autowired
	private LoanMappingProductService loanMappingProductService;
    
    @Autowired
	private LoanMappingProductService loanMappingProductServiceWithCache;

	@Autowired
	private ThreadPoolTaskExecutor threadPoolTaskExecutor;

	@Autowired
	private AuditLogService auditLogService;	
	

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_UPLOAD_PRODUCT_RATE_MAPPING, method = RequestMethod.GET)
	public String showListPage(@ModelAttribute("uploadForm") UploadForm uploadForm, final Model model,
                               final HttpServletRequest request, final HttpServletResponse response) {
		model.addAttribute("uploadForm", uploadForm);
		uploadService.getErrorBar();
		uploadService.clearBar();
		if (null != uploadService.getProgressMessage()) {
			model.addAttribute("okMessage", uploadService.getProgressMessage());
			model.addAttribute("enable", uploadService.isEnable());
		} else
			model.addAttribute("enable", true);

		if (uploadService.getErrorBar() != null) {
			model.addAttribute("errorMessage", uploadService.getErrorBar());
		}
		
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		User user = (User) auth.getPrincipal();

		AuditLog auditLog = new AuditLog();
		auditLog.setActivityType(auditLog.VIEW_PAGE);
		auditLog.setCreatedBy(user.getUsername());
		auditLog.setCreatedDate(new Date());
		auditLog.setDescription("Buka Halaman Upload file");
		auditLog.setReffNo(new SimpleDateFormat("yyyyMMDDhhmmss").format(new Date()));

		try {
			auditLogService.insertAuditLog(auditLog);
		} catch (Exception e) {
			e.printStackTrace();
		}
		log.debug("Added Audit Log");

		return getPageContent(UPLOAD_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_UPLOAD_PRODUCT_RATE_MANAGE_PRODUKMAPPING_GET_LIST_MAPPING, method = RequestMethod.GET)
	public void doSearch(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		if(isAjaxRequest(request)) {
			setResponseAsJson(response);
			
			DataTablesPojo dataTablesPojo = new DataTablesPojo();
			try {
				dataTablesPojo.setDraw(Integer.parseInt(request.getParameter("draw")));
			} catch (Exception e) {
			 dataTablesPojo.setDraw(0);
			}
			
			final int orderableColumnCount = 9;
			final StringBuffer sbOrder = new StringBuffer();
			
			LinkedHashMap<String, String> columnMap = new LinkedHashMap<String, String>();

			columnMap.put("0", "productName");
			columnMap.put("1", "jenisNasabah");
			columnMap.put("2", "tujuanpembiayaan");
			columnMap.put("3", "regularPiloting");
			columnMap.put("4", "updateDate");
			columnMap.put("5", "productCode");
			columnMap.put("6", "typeNasabah");
			columnMap.put("7", "tenorBulan");

			int offset = 0;
			int limit = 50;
			try {
				offset = Integer.parseInt(request.getParameter("start"));
			} catch (NumberFormatException e) {
			}
			try {
				limit = Integer.parseInt(request.getParameter("length"));
			} catch (NumberFormatException e) {
			}
			
			for (int i = 0; i< orderableColumnCount;i++) {
				String orderColumn = request.getParameter("order[" + i + "][column]");
				if (orderColumn == null || orderColumn.equalsIgnoreCase("")) {
					break;
				} else {
					orderColumn = columnMap.get(orderColumn);
					if (orderColumn == null) {
						break;
					}
					String orderDir = request.getParameter("order[" + i + "][dir]");
					if (orderDir == null || !orderDir.equalsIgnoreCase("desc")) {
						orderDir = "asc";
					}
					sbOrder.append(orderColumn);
					sbOrder.append("-");
					sbOrder.append(orderDir);
					sbOrder.append("+");
				}
			}
			final String keyword = request.getParameter("q");
			//final String authority = request.getParameter("a");
			try {
				final int finalOffset = offset;
				final int finalLimit = limit;
				
				Page<LoanProduct> listLoanProductMapping = loanMappingProductServiceWithCache.search(keyword, columnMap, finalOffset, finalLimit);
				
				List<String[]> data = new ArrayList<>();
				
				for(LoanProduct loanProduct : listLoanProductMapping ) {
					if(loanProduct.getTenorBulan() == null) {
						loanProduct.setTenorBulan(0);
						}
						data.add(new String[] {
								HtmlUtils.htmlEscape(loanProduct.getProductCode().toUpperCase()),
								HtmlUtils.htmlEscape(loanProduct.getUpdateDate().toString()),
								HtmlUtils.htmlEscape(loanProduct.getProductName().toUpperCase()),
								HtmlUtils.htmlEscape(loanProduct.getTenorBulan().toString()),
								HtmlUtils.htmlEscape(loanProduct.getJenisNasabah().toUpperCase()),
								HtmlUtils.htmlEscape(loanProduct.getTujuanpembiayaan().toUpperCase()),
								HtmlUtils.htmlEscape(loanProduct.getTypeNasabah()),
								HtmlUtils.htmlEscape(loanProduct.getRegularPiloting()),
								
								"<a href=\""
										+ getServletContext().getContextPath()
										+ WebGuiConstant.AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT_ADD_REQUEST.replace("{productId}", ""
												+ loanProduct.getProductId())
										+ "\"><i class='fa fa-pencil'><div class='action' id='action-pencil'>ubah produk</div></i></a>" });
				}
				
				dataTablesPojo.setData(data);
				dataTablesPojo.setRecordsFiltered(Integer.parseInt("" + listLoanProductMapping.getTotalElements()));
				dataTablesPojo.setRecordsTotal(Integer.parseInt("" + listLoanProductMapping.getTotalElements()));
			} catch (Exception e) {
				log.error(e.getMessage(), e);
				dataTablesPojo.setError(getMessage("message.datatables.error.fetch.user"));
			} finally {
				response.getWriter().write(new JsonUtils().toJson(dataTablesPojo));
			}
			
		}else {
			response.setStatus(HttpServletResponse.SC_FOUND);
		}
	}
	
	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_UPLOAD_PRODUCT_RATE_MAPPING, method = { RequestMethod.POST })
	public String doUpload(@ModelAttribute("uploadForm") final UploadForm uploadForm, BindingResult errors,
                           final Model model, final HttpServletRequest request, final HttpServletResponse response) {

		String errorMsg = null;
		if (uploadService.isEnable()) {

			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			final User admin = (User) auth.getPrincipal();

			try {

				final List<String> data = new ArrayList<String>();
				BufferedReader br = null;
				String line = "";

				String fileName = uploadForm.getName();
				if(fileName != null){
					String fileExtentionName = getFileExtension(fileName);
					log.info("File Type : "+fileExtentionName);
					if(!"csv".equals(fileExtentionName)){
						errorMsg = "notCsv";
					}else{
						br = new BufferedReader(new InputStreamReader(uploadForm.getFile().getInputStream(), "UTF-8"));
						while ((line = br.readLine()) != null) {
							if (line.equals(""))
								continue;
							data.add(line);
						}

						if (data.size() == 0) {
							errorMsg = "empty";
						} else {
							// panggil Thread
							threadPoolTaskExecutor.execute(new Runnable() {

								@Override
								public void run() {
									try {
										uploadService.insertDbProductRate(data, admin);
									} catch (Exception e) {
										e.printStackTrace();
										model.addAttribute("errorMessage", getMessage("form.error.upload.empty"));
									}
								}
							});
						}
					}
				}else{
					errorMsg = "notCsv";
				}
			} catch (Exception e) {
				e.printStackTrace();
				model.addAttribute("errorMessage", getMessage("form.error.upload.empty"));
			}
		} else
			errorMsg = "alreadyRun";

		if (null == errorMsg) {
			model.addAttribute("okMessage", getMessage("form.progress.upload.start"));
			model.addAttribute("enable", false);
			uploadService.getErrorBar();
			uploadService.clearBar();
		} else if (errorMsg.equals("empty")) {
			model.addAttribute("errorMessage", getMessage("form.error.upload.empty"));
			model.addAttribute("enable", true);
			uploadService.getErrorBar();
			uploadService.clearBar();
		} else if (errorMsg.equals("notCsv")) {
			model.addAttribute("errorMessage", getMessage("form.error.upload.unFormatted"));
			model.addAttribute("enable", true);
			uploadService.getErrorBar();
			uploadService.clearBar();
		} else if (errorMsg.equals("alreadyRun")) {
			model.addAttribute("errorMessage", getMessage("form.error.upload.alreadyRun"));
			model.addAttribute("enable", false);
			uploadService.getErrorBar();
			uploadService.clearBar();
		}

		return getPageContent(UPLOAD_PAGE_NAME);
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_PROGRESS_PRODUCT_RATE_OK, method = RequestMethod.GET)
	public void getOkMessage(final HttpServletRequest request, final HttpServletResponse response) throws IOException {
		if (null != uploadService.getProgressMessage()) {
			response.getWriter().write(uploadService.getProgressMessage());
		} else {
			response.getWriter().write("");
		}
	}

	@RequestMapping(value = WebGuiConstant.AFTER_LOGIN_PATH_PROGRESS_PRODUCT_RATE_ERROR, method = RequestMethod.GET)
	public void getErrorMessage(final HttpServletRequest request, final HttpServletResponse response)
			throws IOException {
		if (null != uploadService.getErrorBar()) {
			response.getWriter().write(uploadService.getErrorBar());
		} else {
			response.getWriter().write("");
		}
	}

	public static String getFileExtension(String fullName) {
		String fileName = new File(fullName).getName();
		int dotIndex = fileName.lastIndexOf('.');
		return (dotIndex == -1) ? "" : fileName.substring(dotIndex + 1);
	}

}