package id.co.telkomsigma.btpns.mprospera.configuration;

import id.co.telkomsigma.btpns.mprospera.controller.webservice.RESTClient;
import id.co.telkomsigma.btpns.mprospera.model.user.User;
import id.co.telkomsigma.btpns.mprospera.service.ParameterService;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@Configuration
public class LdapRestAuthenticationProvider implements AuthenticationProvider {

	protected final Log log = LogFactory.getLog(getClass());

	@Autowired
	private UserDetailsService userService;

	@Autowired
	private ParameterService parameterService;

	@Autowired
	private RESTClient restClient;

	@Override
	public Authentication authenticate(Authentication auth) throws AuthenticationException {
		User fromDb = (User) userService.loadUserByUsername((String) auth.getPrincipal());
		String providedPassword = (String) auth.getCredentials();

		if (fromDb == null) {
			throw new UsernameNotFoundException("User not found.");
		} else {
			if (!restClient.ldapLogin(fromDb.getUsername(), providedPassword)) {
				log.debug("Username/Password does not match for " + auth.getPrincipal());
				throw new BadCredentialsException("Username/Password does not match for " + auth.getPrincipal());
			} else {
				return new UsernamePasswordAuthenticationToken(fromDb, null, fromDb.getAuthorities());
			}
		}
	}

	@Override
	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return true;
	}

}
