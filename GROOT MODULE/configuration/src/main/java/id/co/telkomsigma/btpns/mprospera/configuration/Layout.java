package id.co.telkomsigma.btpns.mprospera.configuration;

import java.lang.annotation.*;

/**
 * Created by daniel on 3/26/15.
 */
@Target({ ElementType.METHOD, ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Layout {
	String value();
}