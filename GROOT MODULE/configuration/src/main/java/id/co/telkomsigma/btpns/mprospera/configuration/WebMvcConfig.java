package id.co.telkomsigma.btpns.mprospera.configuration;

import com.github.isrsal.logging.LoggingFilter;
import id.co.telkomsigma.btpns.mprospera.authhandler.MenuValidation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.handler.SimpleUrlHandlerMapping;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;
import org.springframework.web.servlet.resource.ResourceHttpRequestHandler;

import javax.servlet.Filter;
import java.util.*;

/**
 * Created by daniel on 3/26/15.
 */
@Configuration
public class WebMvcConfig extends WebMvcConfigurationSupport {
	@Value("${default.locale.lang}")
	private String localeLanguage;
	@Value("${default.locale.country}")
	private String localeCountry;

	@Value("${default.timezone.lang}")
	private String timezoneLanguage;
	@Value("${default.timezone.country}")
	private String timezoneCountry;

	private static Locale language;
	private static Locale timezone;

	@Autowired
	ApplicationContext applicationContext;

	@Bean(name = "language")
	public Locale getLanguage() {
		if (language == null) {
			language = new Locale(localeLanguage, localeCountry);
		}
		return language;
	}

	@Bean(name = "timezone")
	public Locale getTimezone() {
		if (timezone == null) {
			timezone = new Locale(timezoneLanguage, timezoneCountry);
		}
		return timezone;
	}

	@Bean(name = "localeResolver")
	public LocaleResolver localeResolver() {
		SessionLocaleResolver slr = new SessionLocaleResolver();
		slr.setDefaultLocale(getLanguage());
		return slr;
	}

	@Bean
	public LocaleChangeInterceptor localeChangeInterceptor() {
		LocaleChangeInterceptor lci = new LocaleChangeInterceptor();
		lci.setParamName("lang");
		return lci;
	}

	@Override
	protected void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(localeChangeInterceptor());
		registry.addInterceptor(new ThymeleafLayoutInterceptor());
		registry.addInterceptor(new MenuValidation()).addPathPatterns("/home/**");
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/css/**").setCachePeriod(Integer.MAX_VALUE)
				.addResourceLocations("classpath:/static/css/");
		registry.addResourceHandler("/fonts/**").setCachePeriod(Integer.MAX_VALUE)
				.addResourceLocations("classpath:/static/fonts/");
		registry.addResourceHandler("/img/**").setCachePeriod(Integer.MAX_VALUE)
				.addResourceLocations("classpath:/static/img/");
		registry.addResourceHandler("/js/**").setCachePeriod(Integer.MAX_VALUE)
				.addResourceLocations("classpath:/static/js/");
		// registry.addResourceHandler("/apk/**").setCachePeriod(Integer.MAX_VALUE).addResourceLocations("classpath:/static/apk/");
	}

	@Bean
	public FilterRegistrationBean logFilterRegistration() {
		FilterRegistrationBean filterRegBean = new FilterRegistrationBean();
		filterRegBean.setFilter(logFilter());
		filterRegBean.addUrlPatterns("/webservice/*");
		filterRegBean.setEnabled(true);
		return filterRegBean;
	}

	@Bean
	public Filter logFilter() {
		return new LoggingFilter();
	}

	@Bean
	protected ResourceHttpRequestHandler uncacheableResourceHandler() {
		StaticResourceHttpRequestHandler handler = new StaticResourceHttpRequestHandler();
		Map<String, String> headers = new HashMap<>();
		headers.put("Cache-Control", "no-cache, no-store, must-revalidate");
		headers.put("Pragma", "no-cache");
		headers.put("Expires", "0");
		headers.put("Content-Disposition", "attachment;");
		handler.setHeaders(headers);
		handler.setLocations(Arrays.asList(applicationContext.getResource("classpath:/static/apk/")));
		return handler;
	}

	@Bean
	public SimpleUrlHandlerMapping sampleServletMapping() {
		SimpleUrlHandlerMapping mapping = new SimpleUrlHandlerMapping();
		// By default the static resource handler mapping is
		// Ordered.LOWEST_PRECEDENCE
		// -1 to run before defaultHandler
		mapping.setOrder(Ordered.LOWEST_PRECEDENCE - 1);
		Properties urlProperties = new Properties();
		urlProperties.put("/apk/**", "uncacheableResourceHandler");
		mapping.setMappings(urlProperties);
		return mapping;

	}
}