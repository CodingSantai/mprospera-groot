package id.co.telkomsigma.btpns.mprospera.request;

public class SavingPRSRequest {

	private String accountId;
	private String accountNumber;
	private String withdrawalAmount;
	private String nextWithdrawalAmountPlan;
	private String isCloseSavingAccount;
	private String clearBalance;
	private String depositAmount;

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getWithdrawalAmount() {
		return withdrawalAmount;
	}

	public void setWithdrawalAmount(String withdrawalAmount) {
		this.withdrawalAmount = withdrawalAmount;
	}

	public String getNextWithdrawalAmountPlan() {
		return nextWithdrawalAmountPlan;
	}

	public void setNextWithdrawalAmountPlan(String nextWithdrawalAmountPlan) {
		this.nextWithdrawalAmountPlan = nextWithdrawalAmountPlan;
	}

	public String getIsCloseSavingAccount() {
		return isCloseSavingAccount;
	}

	public void setIsCloseSavingAccount(String isCloseSavingAccount) {
		this.isCloseSavingAccount = isCloseSavingAccount;
	}

	public String getClearBalance() {
		return clearBalance;
	}

	public void setClearBalance(String clearBalance) {
		this.clearBalance = clearBalance;
	}

	public String getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(String depositAmount) {
		this.depositAmount = depositAmount;
	}

}
