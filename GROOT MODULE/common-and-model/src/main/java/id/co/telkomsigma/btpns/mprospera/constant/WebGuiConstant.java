package id.co.telkomsigma.btpns.mprospera.constant;

/**
 * Created by daniel on 4/17/15.
 */
public final class WebGuiConstant {

    /**
     * URL INTERNAL SERVICE
     */
    public static final String INTERNAL_WSVALIDATION = "/internal/wsValidation**";
    public static final String INTERNAL_LOGINVALIDATION = "/internal/loginValidation**";

    /**
     * URL CAPTCHA
     */
    private static final String CAPTCHA_PATH = "/captcha";
    public static final String CAPTCHA_REGISTRATION_PATH_MAPPING = CAPTCHA_PATH + "-registration*";
    public static final String CAPTCHA_RESEND_ACTIVATION_LINK_PATH_MAPPING = CAPTCHA_PATH + "-resend-activation*";
    public static final String CAPTCHA_RESET_PASSWORD_PATH_MAPPING = CAPTCHA_PATH + "-reset-password*";
    public static final String CAPTCHA_PATH_SECURITY_MAPPING = CAPTCHA_PATH + "**";

    /**
     * URL GLOBAL
     */
    private static final String AFTER_LOGIN_PATH = "/home";
    public static final String TERMINAL_ECHO_PATH = "/webservice/echo**";
    public static final String AFTER_LOGIN_PATH_HOME_PAGE = "/home";
    public static final String AFTER_LOGIN_PATH_SECURITY_MAPPING = AFTER_LOGIN_PATH + "/**";
    public static final String AFTER_LOGIN_PATH_PROFILE_PAGE = AFTER_LOGIN_PATH + "/profile";
    public static final String AFTER_LOGIN_PATH_PARAM_PAGE = "/home/param";
    public static final String AFTER_LOGIN_PATH_HOME_GET_LIST_USER = AFTER_LOGIN_PATH + "/list*";

    /**
     * URL MENU DASHBOARD EOD
     */
    public static final String AFTER_LOGIN_PATH_DASHBOARDETL_JOBEXECUTION = AFTER_LOGIN_PATH
            + "/manage/dashboard/jobExecution";
    public static final String AFTER_LOGIN_PATH_DASHBOARDETL_JOBEXECUTION_MAPPING = AFTER_LOGIN_PATH_DASHBOARDETL_JOBEXECUTION
            + "*";
    public static final String AFTER_LOGIN_PATH_DASHBOARDETL_JOBEXECUTION_RERUN_MAPPING = AFTER_LOGIN_PATH_DASHBOARDETL_JOBEXECUTION
            + "/rerun*";
    public static final String AFTER_LOGIN_PATH_DASHBOARDETL_JOBEXECUTION_GET_LIST_MAPPING = AFTER_LOGIN_PATH_DASHBOARDETL_JOBEXECUTION
            + "/get-list-jobexec*";
    public static final String AFTER_LOGIN_PATH_DASHBOARDETL_STEPEXECUTION = AFTER_LOGIN_PATH
            + "/manage/dashboard/jobExecution/stepExecution";
    public static final String AFTER_LOGIN_PATH_DASHBOARDETL_STEPEXECUTION_MAPPING = AFTER_LOGIN_PATH_DASHBOARDETL_STEPEXECUTION
            + "*";
    public static final String AFTER_LOGIN_PATH_DASHBOARDETL_STEPEXECUTION_REQUEST = AFTER_LOGIN_PATH_DASHBOARDETL_STEPEXECUTION
            + "?jobExecutionId={jobExecutionId}";
    public static final String AFTER_LOGIN_PATH_DASHBOARDETL_STEPEXECUTION_GET_LIST_MAPPING = AFTER_LOGIN_PATH_DASHBOARDETL_STEPEXECUTION
            + "/get-list-stepexec*";

    /**
     * URL MENU AUDIT LOG
     */
    public static final String AFTER_LOGIN_PATH_AUDIT_LOG = AFTER_LOGIN_PATH + "/auditLog";
    public static final String AFTER_LOGIN_PATH_AUDIT_LOG_MAPPING = AFTER_LOGIN_PATH_AUDIT_LOG + "*";
    public static final String AFTER_LOGIN_PATH_AUDIT_LOG_GET_LIST_MAPPING = AFTER_LOGIN_PATH_AUDIT_LOG + "/get-list*";
    public static final String AFTER_LOGIN_PATH_AUDIT_LOG_SEARCH_MAPPING = AFTER_LOGIN_PATH_AUDIT_LOG + "/search*";

    /**
     * URL MENU KELOLA USER
     */
    public static final String AFTER_LOGIN_PATH_MANAGE_USER = AFTER_LOGIN_PATH + "/manage/user";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_GET_LIST_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER
            + "/get-list*";
    private static final String AFTER_LOGIN_PATH_MANAGE_USER_ADD = AFTER_LOGIN_PATH_MANAGE_USER + "/add";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_ADD_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER_ADD + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_ADD_REQUEST = AFTER_LOGIN_PATH_MANAGE_USER_ADD
            + "?userId={userId}";
    private static final String AFTER_LOGIN_PATH_MANAGE_USER_LOCK = AFTER_LOGIN_PATH_MANAGE_USER + "/lock";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_LOCK_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER_LOCK + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_LOCK_REQUEST = AFTER_LOGIN_PATH_MANAGE_USER_LOCK
            + "?userId={userId}";
    private static final String AFTER_LOGIN_PATH_MANAGE_USER_UNLOCK = AFTER_LOGIN_PATH_MANAGE_USER + "/unlock";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_UNLOCK_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER_UNLOCK + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_UNLOCK_REQUEST = AFTER_LOGIN_PATH_MANAGE_USER_UNLOCK
            + "?userId={userId}";
    private static final String AFTER_LOGIN_PATH_MANAGE_USER_ENABLE = AFTER_LOGIN_PATH_MANAGE_USER + "/enable";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_ENABLE_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER_ENABLE + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_ENABLE_REQUEST = AFTER_LOGIN_PATH_MANAGE_USER_ENABLE
            + "?userId={userId}";
    private static final String AFTER_LOGIN_PATH_MANAGE_USER_DISABLE = AFTER_LOGIN_PATH_MANAGE_USER + "/disable";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_DISABLE_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER_DISABLE
            + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_DISABLE_REQUEST = AFTER_LOGIN_PATH_MANAGE_USER_DISABLE
            + "?userId={userId}";
    private static final String AFTER_LOGIN_PATH_MANAGE_USER_RESET = AFTER_LOGIN_PATH_MANAGE_USER + "/reset";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_RESET_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER_RESET + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_RESET_REQUEST = AFTER_LOGIN_PATH_MANAGE_USER_RESET
            + "?userId={userId}";
    private static final String AFTER_LOGIN_PATH_MANAGE_USER_CLEAR_SESSION = AFTER_LOGIN_PATH_MANAGE_USER
            + "/clearSession";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_CLEAR_SESSION_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER_CLEAR_SESSION
            + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_CLEAR_SESSION_REQUEST = AFTER_LOGIN_PATH_MANAGE_USER_CLEAR_SESSION
            + "?userId={userId}";
    public static final String AFTER_LOGIN_PATH_MANAGE_USER_AJAX_SEARCH_LOCATION_MAPPING = AFTER_LOGIN_PATH_MANAGE_USER_ADD
            + "/ajax/location*";

    /**
     * URL MENU KELOLA PARAMETER
     */
    public static final String AFTER_LOGIN_PATH_MANAGE_PARAM = AFTER_LOGIN_PATH + "/manage/parameter";
    public static final String AFTER_LOGIN_PATH_MANAGE_PARAM_MAPPING = AFTER_LOGIN_PATH_MANAGE_PARAM + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_PARAM_GET_LIST_MAPPING = AFTER_LOGIN_PATH_MANAGE_PARAM
            + "/get-list*";
    private static final String AFTER_LOGIN_PATH_MANAGE_PARAM_EDIT = AFTER_LOGIN_PATH_MANAGE_PARAM + "/edit";
    public static final String AFTER_LOGIN_PATH_MANAGE_PARAM_EDIT_MAPPING = AFTER_LOGIN_PATH_MANAGE_PARAM_EDIT + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_PARAM_EDIT_REQUEST = AFTER_LOGIN_PATH_MANAGE_PARAM_EDIT
            + "?paramId={paramId}";

    /**
     * URL MENU KELOLA PRS PARAMETER
     */
    public static final String AFTER_LOGIN_PATH_MANAGE_PRS_PARAM = AFTER_LOGIN_PATH + "/manage/prsparam";
    public static final String AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_MAPPING = AFTER_LOGIN_PATH_MANAGE_PRS_PARAM + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_GET_LIST_MAPPING = AFTER_LOGIN_PATH_MANAGE_PRS_PARAM
            + "/get-list*";
    private static final String AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_EDIT = AFTER_LOGIN_PATH_MANAGE_PRS_PARAM + "/edit";
    public static final String AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_EDIT_MAPPING = AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_EDIT
            + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_EDIT_REQUEST = AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_EDIT
            + "?prsParamId={prsParamId}";

    private static final String AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_ADD = AFTER_LOGIN_PATH_MANAGE_PRS_PARAM + "/add";
    public static final String AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_ADD_MAPPING = AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_ADD
            + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_ADD_REQUEST = AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_ADD
            + "?parameterId={parameterId}";

    private static final String AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_DELETE = AFTER_LOGIN_PATH_MANAGE_PRS_PARAM
            + "/delete";
    public static final String AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_DELETE_MAPPING = AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_DELETE
            + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_DELETE_REQUEST = AFTER_LOGIN_PATH_MANAGE_PRS_PARAM_DELETE
            + "?parameterId={parameterId}";

    /**
     * URL MENU KELOLA ROLE
     */
    public static final String AFTER_LOGIN_PATH_MANAGE_ROLE = AFTER_LOGIN_PATH + "/manage/role";
    public static final String AFTER_LOGIN_PATH_MANAGE_ROLE_MAPPING = AFTER_LOGIN_PATH_MANAGE_ROLE + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_ROLE_GET_LIST_MAPPING = AFTER_LOGIN_PATH_MANAGE_ROLE
            + "/get-list*";
    private static final String AFTER_LOGIN_PATH_MANAGE_ROLE_ADD = AFTER_LOGIN_PATH_MANAGE_ROLE + "/add";
    public static final String AFTER_LOGIN_PATH_MANAGE_ROLE_ADD_MAPPING = AFTER_LOGIN_PATH_MANAGE_ROLE_ADD + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_ROLE_ADD_REQUEST = AFTER_LOGIN_PATH_MANAGE_ROLE_ADD
            + "?roleId={roleId}";
    private static final String AFTER_LOGIN_PATH_MANAGE_ROLE_EDIT = AFTER_LOGIN_PATH_MANAGE_ROLE + "/edit";
    public static final String AFTER_LOGIN_PATH_MANAGE_ROLE_EDIT_MAPPING = AFTER_LOGIN_PATH_MANAGE_ROLE_EDIT + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_ROLE_EDIT_REQUEST = AFTER_LOGIN_PATH_MANAGE_ROLE_EDIT
            + "?u={rolename}";

    /**
     * URL MENU UPLOAD MAPPING PRODUCT
     */
    public static final String AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT = AFTER_LOGIN_PATH + "/manage/mappingproduct";
    public static final String AFTER_LOGIN_PATH_MANAGE_PRODUKMAPPING_MAPPING = AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_PRODUKMAPPING_GET_LIST_MAPPING = AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT
            + "/get-list*";
    private static final String AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT_ADD = AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT + "/add";
    public static final String AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT_ADD_MAPPING = AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT_ADD + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT_ADD_REQUEST = AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT_ADD
            + "?productId={productId}";
    private static final String AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT_EDIT = AFTER_LOGIN_PATH_MANAGE_ROLE + "/edit";
    public static final String AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT_MAPPING = AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT_EDIT + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT_REQUEST = AFTER_LOGIN_PATH_MANAGE_MAPPING_PRODUCT_EDIT
            + "?p={productId}";

    /**
     * URL MENU KELOLA TERMINAL
     */
    public static final String AFTER_LOGIN_PATH_TERMINAL = AFTER_LOGIN_PATH + "/terminal";
    public static final String AFTER_LOGIN_PATH_TERMINAL_MAPPING = AFTER_LOGIN_PATH_TERMINAL + "*";
    public static final String AFTER_LOGIN_PATH_TERMINAL_GET_LIST_MAPPING = AFTER_LOGIN_PATH_TERMINAL
            + "/get-list-terminal*";

    private static final String AFTER_LOGIN_PATH_TERMINAL_DETAILS = AFTER_LOGIN_PATH_TERMINAL + "/show-details";
    public static final String AFTER_LOGIN_PATH_TERMINAL_DETAILS_MAPPING = AFTER_LOGIN_PATH_TERMINAL_DETAILS + "*";
    public static final String AFTER_LOGIN_PATH_TERMINAL_DETAILS_REQUEST = AFTER_LOGIN_PATH_TERMINAL_DETAILS
            + "?terminalId={terminalId}";

    public static final String AFTER_LOGIN_PATH_TERMINAL_DETAILS_BY_ID_MAPPING = AFTER_LOGIN_PATH_TERMINAL + "*";
    public static final String AFTER_LOGIN_PATH_TERMINAL_GET_LIST_DETAILS_BY_ID_MAPPING = AFTER_LOGIN_PATH_TERMINAL
            + "/get-list-terminal-details*";

    private static final String AFTER_LOGIN_PATH_TERMINAL_DETAILS_MESSAGE = AFTER_LOGIN_PATH_TERMINAL_DETAILS
            + "/show-message-details";
    public static final String AFTER_LOGIN_PATH_TERMINAL_DETAILS_MESSAGE_MAPPING = AFTER_LOGIN_PATH_TERMINAL_DETAILS_MESSAGE
            + "*";
    public static final String AFTER_LOGIN_PATH_TERMINAL_DETAILS_MESSAGE_REQUEST = AFTER_LOGIN_PATH_TERMINAL_DETAILS_MESSAGE
            + "?terminalActivityId={terminalActivityId}&date={date}";

    public static final String AFTER_LOGIN_PATH_TERMINAL_DETAILS_GET_MESSAGE_BY_ID_MAPPING = AFTER_LOGIN_PATH_TERMINAL_DETAILS
            + "*";
    public static final String AFTER_LOGIN_PATH_TERMINAL_DETAILS_GET_LIST_MESSAGE_BY_ID_MAPPING = AFTER_LOGIN_PATH_TERMINAL_DETAILS
            + "/get-list-message-details*";

    /**
     * URL ADMIN
     */
    private static final String ADMIN_AFTER_LOGIN_PATH = "/admin";
    public static final String ADMIN_AFTER_LOGIN_PATH_HOME_PAGE = ADMIN_AFTER_LOGIN_PATH + "/home";
    public static final String ADMIN_AFTER_LOGIN_PATH_SECURITY_MAPPING = ADMIN_AFTER_LOGIN_PATH + "/**";

    public static final String ADMIN_AFTER_LOGIN_PATH_PROFILE_PAGE = ADMIN_AFTER_LOGIN_PATH + "/profile";

    public static final String ADMIN_AFTER_LOGIN_PATH_MANAGE_USER = ADMIN_AFTER_LOGIN_PATH + "/manage/user";
    public static final String ADMIN_AFTER_LOGIN_PATH_MANAGE_USER_MAPPING = ADMIN_AFTER_LOGIN_PATH_MANAGE_USER + "*";
    public static final String ADMIN_AFTER_LOGIN_PATH_MANAGE_USER_GET_LIST_MAPPING = ADMIN_AFTER_LOGIN_PATH_MANAGE_USER
            + "/get-list*";
    private static final String ADMIN_AFTER_LOGIN_PATH_MANAGE_USER_ADD = ADMIN_AFTER_LOGIN_PATH_MANAGE_USER + "/add";
    public static final String ADMIN_AFTER_LOGIN_PATH_MANAGE_USER_ADD_MAPPING = ADMIN_AFTER_LOGIN_PATH_MANAGE_USER_ADD
            + "*";
    private static final String ADMIN_AFTER_LOGIN_PATH_MANAGE_USER_EDIT = ADMIN_AFTER_LOGIN_PATH_MANAGE_USER + "/edit";
    public static final String ADMIN_AFTER_LOGIN_PATH_MANAGE_USER_EDIT_MAPPING = ADMIN_AFTER_LOGIN_PATH_MANAGE_USER_EDIT
            + "*";
    public static final String ADMIN_AFTER_LOGIN_PATH_MANAGE_USER_EDIT_REQUEST = ADMIN_AFTER_LOGIN_PATH_MANAGE_USER_EDIT
            + "?u={username}";

    /**
     * URL MENU KELOLA CACHE
     */
    public static final String AFTER_LOGIN_PATH_CACHE = AFTER_LOGIN_PATH + "/manage/cache";
    
    /**
     * URL MENU Clear Cache
     */
    public static final String PATH_CACHE_CLEAR_ALL = "/webservice/clearAllCache";
    
    /**
     * URL MENU Clear Session Key
     */
    public static final String PATH_CACHE_CLEAR_SESSION_KEY = "/webservice/clearSessionKey";
    
    /**
     * URL MENU KELOLA APK
     */
    public static final String AFTER_LOGIN_PATH_APK = AFTER_LOGIN_PATH + "/manage/apk";
    public static final String AFTER_LOGIN_PATH_APK_MAPPING = AFTER_LOGIN_PATH_APK + "*";
    public static final String AFTER_LOGIN_PATH_APK_GET_LIST_MAPPING = AFTER_LOGIN_PATH_APK + "/get-list-apk*";

    public static final String AFTER_LOGIN_PATH_APK_GET_APK = AFTER_LOGIN_PATH_APK + "/get-apk";
    public static final String AFTER_LOGIN_PATH_APK_GET_APK_MAPPING = AFTER_LOGIN_PATH_APK_GET_APK + "*";
    public static final String AFTER_LOGIN_PATH_APK_GET_APK_REQUEST = AFTER_LOGIN_PATH_APK_GET_APK + "?apkId={apkId}";

    public static final String AFTER_LOGIN_PATH_UPLOAD_APK_PROGRESS_OK = AFTER_LOGIN_PATH_APK + "/progress/ok";
    public static final String AFTER_LOGIN_PATH_UPLOAD_APK_PROGRESS_OK_MAPPING = AFTER_LOGIN_PATH_UPLOAD_APK_PROGRESS_OK
            + "*";
    public static final String AFTER_LOGIN_PATH_UPLOAD_APK_PROGRESS_ERROR = AFTER_LOGIN_PATH_APK + "/progress/error";
    public static final String AFTER_LOGIN_PATH_UPLOAD_APK_PROGRESS_ERROR_MAPPING = AFTER_LOGIN_PATH_UPLOAD_APK_PROGRESS_ERROR
            + "*";

    private static final String AFTER_LOGIN_PATH_MANAGE_APK_NOT_ALLOWED = AFTER_LOGIN_PATH_APK + "/notAllowed";
    public static final String AFTER_LOGIN_PATH_MANAGE_APK_NOT_ALLOWED_MAPPING = AFTER_LOGIN_PATH_MANAGE_APK_NOT_ALLOWED
            + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_APK_NOT_ALLOWED_REQUEST = AFTER_LOGIN_PATH_MANAGE_APK_NOT_ALLOWED
            + "?apkId={apkId}";

    private static final String AFTER_LOGIN_PATH_MANAGE_APK_ALLOWED = AFTER_LOGIN_PATH_APK + "/allowed";
    public static final String AFTER_LOGIN_PATH_MANAGE_APK_ALLOWED_MAPPING = AFTER_LOGIN_PATH_MANAGE_APK_ALLOWED + "*";
    public static final String AFTER_LOGIN_PATH_MANAGE_APK_ALLOWED_REQUEST = AFTER_LOGIN_PATH_MANAGE_APK_ALLOWED
            + "?apkId={apkId}";

    private static final String AFTER_LOGIN_PATH_PUBLIC = "/apk";

    public static final String AFTER_LOGIN_PATH_APK_GET_APK_OUTSIDE = AFTER_LOGIN_PATH_PUBLIC + "/get-apk";
    public static final String AFTER_LOGIN_PATH_APK_GET_APK_MAPPING_OUTSIDE = AFTER_LOGIN_PATH_APK_GET_APK_OUTSIDE
            + "*";
    public static final String AFTER_LOGIN_PATH_APK_GET_APK_REQUEST_OUTSIDE = AFTER_LOGIN_PATH_APK_GET_APK_OUTSIDE
            + "?version={version:.+}";

    /**
     * URL REPORT
     */
    public static final String AFTER_LOGIN_PATH_REPORT = AFTER_LOGIN_PATH + "/report";
    public static final String AFTER_LOGIN_PATH_REPORT_MAPPING = AFTER_LOGIN_PATH_REPORT + "*";
    public static final String AFTER_LOGIN_PATH_REPORT_GET_LIST_MAPPING = AFTER_LOGIN_PATH_REPORT + "/get-list-report*";
    public static final String AFTER_LOGIN_PATH_REPORT_GET_PDF = AFTER_LOGIN_PATH_REPORT + "/get-pdf";
    public static final String AFTER_LOGIN_PATH_REPORT_GET_PDF_MAPPING = AFTER_LOGIN_PATH_REPORT_GET_PDF + "*";
    public static final String AFTER_LOGIN_PATH_REPORT_GET_PDF_REQUEST = AFTER_LOGIN_PATH_REPORT_GET_PDF
            + "?downloadId={downloadId}";

    /**
     * URL REPORT DMK RMK
     */
    public static final String AFTER_LOGIN_PATH_GENERATE_RMK_REPORT = AFTER_LOGIN_PATH + "/manage/report/generateReportRmk";
    public static final String AFTER_LOGIN_PATH_GENERATE_RMK_REPORT_MAPPING = AFTER_LOGIN_PATH_GENERATE_RMK_REPORT + "*";
    public static final String AFTER_LOGIN_PATH_LIST_GENERATE_REPORT_MAPPING = AFTER_LOGIN_PATH + "/manage/report*";
    public static final String AFTER_LOGIN_PATH_GENERATE_RMK_REPORT_AJAX_MAPPING = AFTER_LOGIN_PATH_GENERATE_RMK_REPORT
            + "/ajaxGetMms*";

    public static final String AFTER_LOGIN_PATH_GENERATE_DMK_REPORT = AFTER_LOGIN_PATH + "/manage/report/generateReportDmk";
    public static final String AFTER_LOGIN_PATH_GENERATE_DMK_REPORT_MAPPING = AFTER_LOGIN_PATH_GENERATE_DMK_REPORT + "*";
    public static final String AFTER_LOGIN_PATH_LIST_GENERATE_DMK_REPORT_MAPPING = AFTER_LOGIN_PATH + "/manage/report*";
    public static final String AFTER_LOGIN_PATH_GENERATE_DMK_REPORT_AJAX_MAPPING = AFTER_LOGIN_PATH_GENERATE_DMK_REPORT
            + "/ajaxGetMms*";

    /**
     * CONSTANT MENU
     */
    public static final String HEADER_AFTER_LOGIN_PATH = "/header";

    /**
     * OTHER CONSTANT
     */
    public static final long DEFAULT_VALUE_COUNTRY_CODE = 94;
    public static final String DATE_FORMAT_ERROR = "field.error.invalid.date.format";

    /**
     * SESSION CONSTANT
     */
    public static final String SESSION_REGISTERED_USER = "registeredUser";
    public static final String SESSION_RESEND_ACTIVATION_LINK_USER = "resendActivationLinkUser";
    public static final String SESSION_REQUEST_RESET_PASSWORD_USER = "requestResetPasswordUser";
    public static final String SESSION_RESET_PASSWORD_USER = "resetPasswordUser";

    public static final String SESSION_CAPTCHA_REGISTRATION = "registeredUser";
    public static final String SESSION_CAPTCHA_REQUEST_RESET_PASSWORD = "requestResetPassword";
    public static final String SESSION_CAPTCHA_RESEND_ACTIVATION_LINK = "resendActivationLink";

    /**
     * AP3R REPORT SCHEDULER CONSTANT
     */
    public static final String GENERATE_AP3R = "Generate AP3R Report";
    public static final String CRON_AP3R = "${ap3r.cron.scheduler}";
    public static final String IP_AP3R = "ap3r.cron.ip.running";
    public static final String SYSTEM_PROPERTY_AP3R = "ap3r.cron.scheduler";
    public static final String SYSTEM_PROPERTY_AP3R_DAYS = "ap3r.cron.days";

    /**
     * RESPONSE CODE CONSTANT
     */
    public static final String RC_SUCCESS = "00";
    public static final String RC_FAILED = "01";
    public static final String RC_USER_LOCK = "02";
    public static final String RC_USER_DISABLE = "03";
    public static final String RC_TERMINAL_NOT_FOUND = "04";
    public static final String RC_INVALID_PASSWORD = "05";
    public static final String RC_INVALID_SESSION_KEY = "06";
    public static final String RC_ROLE_CANNOT_ACCESS_MOBILE = "07";
    public static final String INVALID_SDA_CATEGORY = "08";
    public static final String UNKNOWN_PARENT_AREA = "09";
    public static final String UNKNOWN_SDA_ID = "10";
    public static final String UNKNOWN_SENTRA_ID = "11";
    public static final String UNKNOWN_PDK_ID = "12";
    public static final String UNKNOWN_SW_ID = "13";
    public static final String UNKNOWN_CUSTOMER_ID = "14";
    public static final String INVALID_APK = "15";
    public static final String UNKNOWN_MM_ID = "16";
    public static final String UNKNOWN_PM_ID = "17";
    public static final String UNKNOWN_LOAN_ID = "18";
    public static final String UNKNOWN_SURVEY_ID = "19";
    public static final String RC_USERNAME_NOTEXISTS = "20";
    public static final String RC_IMEI_NOTEXISTS = "21";
    public static final String RC_SESSIONKEY_NOTEXISTS = "22";
    public static final String RC_IMEI_NOTVALID_FORMAT = "23";
    public static final String RC_USER_INVALID_CITY = "24";
    public static final String RC_ALREADY_APPROVED = "25";
    public static final String RC_UNKNOWN_STATUS = "26";
    public static final String RC_ALREADY_CLOSED = "27";
    public static final String RC_ALREADY_REJECTED = "28";
    public static final String RC_ALREADY_CANCEL = "29";
    public static final String RC_SENTRA_NOTAPPROVED = "30";
    public static final String UNKNOWN_APP_ID = "31";
    public static final String RC_USERNAME_USED = "32";
    public static final String RC_SENTRAGROUP_NOTAPPROVED = "33";
    public static final String RC_SENTRAGROUP_NULL = "34";
    public static final String UNKNOWN_CIF_NUMBER = "35";
    public static final String RC_PRS_NULL = "36";
    public static final String RC_PRS_PHOTO_NULL = "37";
    public static final String RC_PRS_ALREADY_SUBMIT = "38";
    public static final String UNKNOWN_LOAN_PRS = "39";
    public static final String SW_ALREADY_EXIST = "40";
    public static final String ROLE_NULL = "41";
    public static final String ID_PHOTO_NULL = "42";
    public static final String SURVEY_PHOTO_NULL = "43";
    public static final String PRS_REJECTED = "44";
    public static final String UNKNOWN_SAVING = "45";
    public static final String NO_LOAN_HIST = "46";
    public static final String NO_SAVING_HIST = "47";
    public static final String INVALID_KECAMATAN = "48";
    public static final String INVALID_KELURAHAN = "49";
    public static final String NULL_AP3R = "50";
    public static final String AP3R_ALREADY_EXIST = "51";
    public static final String SW_PRODUCT_NULL = "52";
    public static final String SW_APPROVED_FAILED = "53";
    public static final String PHOTO_ALREADY_EXIST = "54";
    public static final String USER_CANNOT_APPROVE = "55";
    public static final String UNKNOWN_INSURANCE_CLAIM_ID = "56";
    public static final String RC_FILE_DAYA_NULL = "57";
    public static final String EARLY_TERMINATION_PLAN_NULL = "58";
    public static final String UNREGISTERED_USER = "59";
    public static final String CUSTOMER_WAIT_APPROVED = "60";
    public static final String RC_LOCATION_ID_NOT_FOUND = "61";
    public static final String APPID_NULL = "62";
    public static final String GROUP_LEADER_NULL = "63";
    public static final String INVALID_RRN = "64";
    public static final String AP3R_NULL = "65";
    public static final String DEVIATION_NOT_APPROVED = "66";
    public static final String DEVIATION_NULL = "67";
    public static final String SENTRA_PHOTO_NULL = "68";
    public static final String SW_NOT_APPROVED = "69";
    public static final String DEVIATION_CANNOT_DELETE = "70";
    public static final String RC_GENERAL_ERROR = "XX";

    /**
     * LOCATION TYPE CONSTANT
     */
    public static final String SDA_PROVINSI = "PROVINSI";
    public static final String SDA_KABUPATEN = "KABUPATEN";
    public static final String SDA_KECAMATAN = "KECAMATAN";
    public static final String SDA_KELURAHAN = "KELURAHAN";
    public static final String SDA_RW = "RW";
    public static final String SDA_RT = "RT";

    /**
     * ENDPOINT CONSTANT
     */
    public static final String ENDPOINT_MPROSPERA = "MPROSPERA";

    /**
     * STATUS CONSTANT
     */
    public static final String STATUS_DRAFT = "DRAFT";
    public static final String STATUS_APPROVED = "APPROVED";
    public static final String STATUS_SUBMIT = "SUBMIT";
    public static final String STATUS_CLOSED = "CLOSED";
    public static final String STATUS_REJECTED = "REJECTED";
    public static final String STATUS_CANCEL = "CANCEL";
    public static final String STATUS_NA = "N/A";
    public static final String STATUS_DONE = "DONE";
    public static final String STATUS_APPROVED_MS = "APPROVED-MS";

    /**
     * PARAMETER CONSTANT
     */
    public static final String PARAMETER_NAME_MAX_FAILED_ATTEMPT = "security.max.failedAttempt";
    public static final String PARAMETER_MAX_FAILED_ATTEMPT_DEFAULT_VALUE = "5";
    public static final String PARAMETER_NAME_LOCK_DURATION = "security.lock.duration";
    public static final String PARAMETER_LOCK_DURATION_DEFAULT_VALUE = "30";
    // custom parameter di tabel user kolom raw
    public static final String PARAMETER_USER_CURRENT_FAILED_ATTEMPT = "current.failedAttempt";
    public static final String PARAMETER_USER_CURRENT_LOCK_UNTIL = "lock.until";
    public static final String PARAMETER_NAME_MAX_GPS_ACCURACT = "android.max.gpsAccuracy";
    public static final String PARAMETER_NAME_MAX_GPS_ACCURACT_DEFAULT_VALUE = "200";
    public static final String PARAMETER_ROLE_USER = "role.user";
    public static final String PARAMETER_ROLE_OTORISASI = "role.otorisasi";
    public static final String PARAMETER_ROLE_NON_MS = "role.non.ms";
    public static final String PARAMETER_LIMIT_BWMP = "limit.bwmp";
    public static final String PARAMETER_CURRENT_VERSION = "android.apk.version.current";
    public static final String PARAMETER_ALLOWED_VERSION = "android.apk.version.allowed";
    public static final String PARAMETER_DOWNLOAD_LINK = "android.apk.url.download";

    /**
     * URL MENU UPLOAD
     */
    public static final String AFTER_LOGIN_PATH_UPLOAD = AFTER_LOGIN_PATH + "/upload";
    public static final String AFTER_LOGIN_PATH_UPLOAD_MAPPING = AFTER_LOGIN_PATH_UPLOAD + "*";
    public static final String AFTER_LOGIN_PATH_UPLOAD_REQUEST = AFTER_LOGIN_PATH_UPLOAD + "?uploadForm={uploadForm}";
    public static final String AFTER_LOGIN_PATH_PROGRESS_OK = AFTER_LOGIN_PATH_UPLOAD + "/progress/ok";
    public static final String AFTER_LOGIN_PATH_PROGRESS_OK_MAPPING = AFTER_LOGIN_PATH_PROGRESS_OK + "*";
    public static final String AFTER_LOGIN_PATH_PROGRESS_ERROR = AFTER_LOGIN_PATH_UPLOAD + "/progress/error";
    public static final String AFTER_LOGIN_PATH_PROGRESS_ERROR_MAPPING = AFTER_LOGIN_PATH_PROGRESS_ERROR + "*";

    public static final String AFTER_LOGIN_PATH_UPLOAD_PRODUCT_RATE = AFTER_LOGIN_PATH + "/uploadProductRate";
    public static final String AFTER_LOGIN_PATH_UPLOAD_PRODUCT_RATE_MAPPING = AFTER_LOGIN_PATH_UPLOAD_PRODUCT_RATE + "*";
    public static final String AFTER_LOGIN_UPLOAD_PRODUCT_RATE_MANAGE_PRODUKMAPPING_GET_LIST_MAPPING = AFTER_LOGIN_PATH_UPLOAD_PRODUCT_RATE
            + "/get-list*";
    public static final String AFTER_LOGIN_PATH_UPLOAD_PRODUCT_RATE_REQUEST = AFTER_LOGIN_PATH_UPLOAD_PRODUCT_RATE + "?uploadForm={uploadForm}";
    public static final String AFTER_LOGIN_PATH_PROGRESS_PRODUCT_RATE_OK = AFTER_LOGIN_PATH_UPLOAD_PRODUCT_RATE + "/progress/ok";
    public static final String AFTER_LOGIN_PATH_PROGRESS_PRODUCT_RATE_OK_MAPPING = AFTER_LOGIN_PATH_PROGRESS_PRODUCT_RATE_OK + "*";
    public static final String AFTER_LOGIN_PATH_PROGRESS_PRODUCT_RATE_ERROR = AFTER_LOGIN_PATH_UPLOAD_PRODUCT_RATE + "/progress/error";
    public static final String AFTER_LOGIN_PATH_PROGRESS_PRODUCT_RATE_ERROR_MAPPING = AFTER_LOGIN_PATH_PROGRESS_PRODUCT_RATE_ERROR + "*";

    public static final String AFTER_LOGIN_PATH_UPLOAD_HOLIDAY = AFTER_LOGIN_PATH + "/uploadHoliday";
    public static final String AFTER_LOGIN_PATH_UPLOAD_HOLIDAY_MAPPING = AFTER_LOGIN_PATH_UPLOAD_HOLIDAY + "*";
    public static final String AFTER_LOGIN_PATH_UPLOAD_HOLIDAY_REQUEST = AFTER_LOGIN_PATH_UPLOAD_HOLIDAY + "?uploadForm={uploadForm}";
    public static final String AFTER_LOGIN_PATH_PROGRESS_HOLIDAY_OK = AFTER_LOGIN_PATH_UPLOAD_HOLIDAY + "/progress/ok";
    public static final String AFTER_LOGIN_PATH_PROGRESS_HOLIDAY_OK_MAPPING = AFTER_LOGIN_PATH_PROGRESS_HOLIDAY_OK + "*";
    public static final String AFTER_LOGIN_PATH_PROGRESS_HOLIDAY_ERROR = AFTER_LOGIN_PATH_UPLOAD_HOLIDAY + "/progress/error";
    public static final String AFTER_LOGIN_PATH_PROGRESS_HOLIDAY_ERROR_MAPPING = AFTER_LOGIN_PATH_PROGRESS_HOLIDAY_ERROR + "*";

    public static final String AFTER_LOGIN_PATH_UPLOAD_JENIS_USAHA = AFTER_LOGIN_PATH + "/uploadJenisUsaha";
    public static final String AFTER_LOGIN_PATH_UPLOAD_JENIS_USAHA_LIST = AFTER_LOGIN_PATH_UPLOAD_JENIS_USAHA + "/list*";
    public static final String AFTER_LOGIN_PATH_UPLOAD_JENIS_USAHA_MAPPING = AFTER_LOGIN_PATH_UPLOAD_JENIS_USAHA + "*";
    public static final String AFTER_LOGIN_PATH_UPLOAD_JENIS_USAHA_REQUEST = AFTER_LOGIN_PATH_UPLOAD_JENIS_USAHA + "?uploadForm={uploadForm}";
    public static final String AFTER_LOGIN_PATH_PROGRESS_JENIS_USAHA_OK = AFTER_LOGIN_PATH_UPLOAD_JENIS_USAHA + "/progress/ok";
    public static final String AFTER_LOGIN_PATH_PROGRESS_JENIS_USAHA_OK_MAPPING = AFTER_LOGIN_PATH_PROGRESS_JENIS_USAHA_OK + "*";
    public static final String AFTER_LOGIN_PATH_PROGRESS_JENIS_USAHA_ERROR = AFTER_LOGIN_PATH_UPLOAD_JENIS_USAHA + "/progress/error";
    public static final String AFTER_LOGIN_PATH_PROGRESS_JENIS_USAHA_ERROR_MAPPING = AFTER_LOGIN_PATH_PROGRESS_JENIS_USAHA_ERROR + "*";

}