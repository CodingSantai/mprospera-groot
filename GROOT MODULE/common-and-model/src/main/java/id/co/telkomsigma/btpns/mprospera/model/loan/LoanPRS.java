package id.co.telkomsigma.btpns.mprospera.model.loan;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;
import id.co.telkomsigma.btpns.mprospera.model.customer.CustomerPRS;
import id.co.telkomsigma.btpns.mprospera.model.prs.PRS;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "T_LOAN_PRS")
public class LoanPRS extends GenericModel {

    /**
     *
     */
    private static final long serialVersionUID = -3443750376719441652L;

    private Long loanPRSId;
    private Loan loanId;
    private CustomerPRS customerId;
    private String appId;
    private Date createdDate;
    private String createdBy;
    private PRS prsId;
    private Date updatedDate;
    private String updatedBy;

    @Id
    @Column(name = "ID", nullable = false, unique = true)
    @GeneratedValue
    public Long getLoanPRSId() {
        return loanPRSId;
    }

    public void setLoanPRSId(Long loanPRSid) {
        this.loanPRSId = loanPRSid;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = Loan.class)
    @JoinColumn(name = "loan_Id", referencedColumnName = "id", nullable = true)
    public Loan getLoanId() {
        return loanId;
    }

    public void setLoanId(Loan loanId) {
        this.loanId = loanId;
    }

    @Column(name = "APP_ID", nullable = true)
    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = CustomerPRS.class)
    @JoinColumn(name = "customer_Id", referencedColumnName = "id", nullable = true)
    public CustomerPRS getCustomerId() {
        return customerId;
    }

    public void setCustomerId(CustomerPRS customerId) {
        this.customerId = customerId;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = PRS.class)
    @JoinColumn(name = "prs_Id", referencedColumnName = "id", nullable = false)
    public PRS getPrsId() {
        return prsId;
    }

    public void setPrsId(PRS prsId) {
        this.prsId = prsId;
    }

    @Column(name = "CREATED_DATE")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "UPDATED_DATE")
    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Column(name = "UPDATED_BY")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

}