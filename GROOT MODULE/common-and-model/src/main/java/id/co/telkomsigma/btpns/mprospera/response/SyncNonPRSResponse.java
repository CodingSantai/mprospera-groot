package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class SyncNonPRSResponse extends BaseResponse {
	
	private List<NonPRSResponseList> sentraList;

	public List<NonPRSResponseList> getSentraList() {
		return sentraList;
	}

	public void setSentraList(List<NonPRSResponseList> sentraList) {
		this.sentraList = sentraList;
	}
	
}
