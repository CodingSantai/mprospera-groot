package id.co.telkomsigma.btpns.mprospera.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class Question {

	private String surveyQuestionId;
	private String questionType;
	private String surveyQuestionValue;

	public String getSurveyQuestionId() {
		return surveyQuestionId;
	}

	public void setSurveyQuestionId(String surveyQuestionId) {
		this.surveyQuestionId = surveyQuestionId;
	}

	public String getQuestionType() {
		return questionType;
	}

	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}

	public String getSurveyQuestionValue() {
		return surveyQuestionValue;
	}

	public void setSurveyQuestionValue(String surveyQuestionValue) {
		this.surveyQuestionValue = surveyQuestionValue;
	}

	@Override
	public String toString() {
		return "ESBQuestion [surveyQuestionId=" + surveyQuestionId + ", questionType=" + questionType
				+ ", surveyQuestionValue=" + surveyQuestionValue + "]";
	}
}
