package id.co.telkomsigma.btpns.mprospera.request;

public class TokenRequest extends BaseRequest {

	private String username;
	private String imei;
	private String sessionKey;
	private String authType;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getAuthType() {
		return authType;
	}

	public void setAuthType(String authType) {
		this.authType = authType;
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	@Override
	public String toString() {
		return "TokenRequest [username=" + username + ", imei=" + imei + ", sessionKey=" + sessionKey + ", authType="
				+ authType + ", getTransmissionDateAndTime()=" + getTransmissionDateAndTime()
				+ ", getRetrievalReferenceNumber()=" + getRetrievalReferenceNumber() + "]";
	}

}
