package id.co.telkomsigma.btpns.mprospera.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.math.BigDecimal;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class SavingNonPRSListResponse {
	
	private String savingNonPrsId;
	private String accountNumber;
	private BigDecimal clearBalance;
	private BigDecimal depositAmount;
	public String getSavingNonPrsId() {
		return savingNonPrsId;
	}
	public void setSavingNonPrsId(String savingNonPrsId) {
		this.savingNonPrsId = savingNonPrsId;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public BigDecimal getClearBalance() {
		return clearBalance;
	}
	public void setClearBalance(BigDecimal clearBalance) {
		this.clearBalance = clearBalance;
	}
	public BigDecimal getDepositAmount() {
		return depositAmount;
	}
	public void setDepositAmount(BigDecimal depositAmount) {
		this.depositAmount = depositAmount;
	}
	
	

}
