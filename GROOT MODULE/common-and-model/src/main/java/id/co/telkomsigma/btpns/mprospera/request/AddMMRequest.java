package id.co.telkomsigma.btpns.mprospera.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class AddMMRequest extends BaseRequest {

	private String username;
	private String sessionKey;
	private String imei;
	private String areaId;
	private String mmLocationName;
	private String mmOwner;
	private String mmNumber;
	private String longitude;
	private String latitude;
	private String action;
	private String mmId;
	private String phoneNumber;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getMmLocationName() {
		return mmLocationName;
	}

	public void setMmLocationName(String mmLocationName) {
		this.mmLocationName = mmLocationName;
	}

	public String getMmOwner() {
		return mmOwner;
	}

	public void setMmOwner(String mmOwner) {
		this.mmOwner = mmOwner;
	}

	public String getMmNumber() {
		return mmNumber;
	}

	public void setMmNumber(String mmNumber) {
		this.mmNumber = mmNumber;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getMmId() {
		return mmId;
	}

	public void setMmId(String mmId) {
		this.mmId = mmId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	@Override
	public String toString() {
		return "AddMMRequest [username=" + username + ", sessionKey=" + sessionKey + ", imei=" + imei + ", areaId="
				+ areaId + ", mmLocationName=" + mmLocationName + ", mmOwner=" + mmOwner + ", mmNumber=" + mmNumber
				+ ", longitude=" + longitude + ", latitude=" + latitude + ", action=" + action + ", mmId=" + mmId
				+ ", getTransmissionDateAndTime()=" + getTransmissionDateAndTime() + ", getRetrievalReferenceNumber()="
				+ getRetrievalReferenceNumber() + "]";
	}

}
