package id.co.telkomsigma.btpns.mprospera.request;

import java.io.Serializable;
import java.util.Arrays;


public class ESBSubmitPrsRequest extends BaseRequest implements Serializable {

	private static final long serialVersionUID = 1L;
	private String username;
	private String imei;
	private String psCompanion;
	private String prsId;
	private String sentraId;
	private String prsDate;
	private String tokenChallenge;
	private String tokenResponse;
	private ESBCustomer[] customerList;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getPsCompanion() {
		return psCompanion;
	}

	public void setPsCompanion(String psCompanion) {
		this.psCompanion = psCompanion;
	}

	public String getPrsId() {
		return prsId;
	}

	public void setPrsId(String prsId) {
		this.prsId = prsId;
	}

	public String getSentraId() {
		return sentraId;
	}

	public void setSentraId(String sentraId) {
		this.sentraId = sentraId;
	}

	public String getPrsDate() {
		return prsDate;
	}

	public void setPrsDate(String prsDate) {
		this.prsDate = prsDate;
	}

	public ESBCustomer[] getCustomerList() {
		return customerList;
	}

	public void setCustomerList(ESBCustomer[] customerList) {
		this.customerList = customerList;
	}

	public String getTokenChallenge() {
		return tokenChallenge;
	}

	public void setTokenChallenge(String tokenChallenge) {
		this.tokenChallenge = tokenChallenge;
	}

	public String getTokenResponse() {
		return tokenResponse;
	}

	public void setTokenResponse(String tokenResponse) {
		this.tokenResponse = tokenResponse;
	}

	@Override
	public String toString() {
		return "ESBSubmitPrsRequest [username=" + username + ", imei=" + imei + ", psCompanion=" + psCompanion
				+ ", prsId=" + prsId + ", sentraId=" + sentraId + ", prsDate=" + prsDate + ", tokenChallenge="
				+ tokenChallenge + ", tokenResponse=" + tokenResponse + ", customerList="
				+ Arrays.toString(customerList) + ", getTransmissionDateAndTime()=" + getTransmissionDateAndTime()
				+ ", getRetrievalReferenceNumber()=" + getRetrievalReferenceNumber() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}

}
