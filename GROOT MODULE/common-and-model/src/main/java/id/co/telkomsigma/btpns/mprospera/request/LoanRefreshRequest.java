package id.co.telkomsigma.btpns.mprospera.request;

import java.util.List;

public class LoanRefreshRequest extends BaseRequest {

	private String imei;
	private String username;
	private String sessionKey;
	private String longitude;
	private String latitude;
	private List<LoanRefreshListRequest> loanList;

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public List<LoanRefreshListRequest> getLoanList() {
		return loanList;
	}

	public void setLoanList(List<LoanRefreshListRequest> loanList) {
		this.loanList = loanList;
	}

	@Override
	public String toString() {
		return "RefreshLoanRequest [imei=" + imei + ", username=" + username + ", sessionKey=" + sessionKey
				+ ", longitude=" + longitude + ", latitude=" + latitude + ", loanList=" + loanList
				+ ", getTransmissionDateAndTime()=" + getTransmissionDateAndTime() + ", getRetrievalReferenceNumber()="
				+ getRetrievalReferenceNumber() + ", getClass()=" + getClass() + ", hashCode()=" + hashCode()
				+ ", toString()=" + super.toString() + "]";
	}

}
