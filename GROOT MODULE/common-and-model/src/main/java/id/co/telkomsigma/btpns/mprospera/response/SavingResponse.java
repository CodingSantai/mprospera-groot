package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class SavingResponse extends BaseResponse {
	
	private String customerId;
	private String customerName;
	private List<SavingListResponse> savingList;
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	public List<SavingListResponse> getSavingList() {
		return savingList;
	}
	public void setSavingList(List<SavingListResponse> savingList) {
		this.savingList = savingList;
	}
	
	@Override
	public String toString() {
		return "SavingResponse [customerId=" + customerId + ", customerName=" + customerName + ", savingList="
				+ savingList + ", getResponseCode()=" + getResponseCode() + ", getResponseMessage()="
				+ getResponseMessage() + ", toString()=" + super.toString() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + "]";
	}
	
	

}
