package id.co.telkomsigma.btpns.mprospera.model.springbatch;

import org.hibernate.annotations.OrderBy;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "BATCH_JOB_EXECUTION")
public class BatchJobExecution {
	private Long jobExecutionId;
	private BigInteger version;
	private JobInstance jobInstance;
	private Date createTime;
	private Date startTime;
	private Date endTime;
	private String status;
	private String exitCode;
	private String exitMessage;
	private Date lastUpdated;
	private String jobConfigurationLocation;
	private List<BatchStepExecution> stepList;

	@OneToMany(fetch = FetchType.LAZY, targetEntity = BatchStepExecution.class)
	@JoinColumn(name = "job_execution_id")
	@OrderBy(clause = "job_execution_id asc")
	public List<BatchStepExecution> getStepList() {
		return stepList;
	}

	public void setStepList(List<BatchStepExecution> stepList) {
		this.stepList = stepList;
	}

	@Id
	@Column(name = "job_execution_id", nullable = false, unique = true)
	public Long getJobExecutionId() {
		return jobExecutionId;
	}

	public void setJobExecutionId(Long jobExecutionId) {
		this.jobExecutionId = jobExecutionId;
	}

	@Column(name = "version", nullable = true)
	public BigInteger getVersion() {
		return version;
	}

	public void setVersion(BigInteger version) {
		this.version = version;
	}

	@ManyToOne(fetch = FetchType.EAGER, targetEntity = JobInstance.class)
	@JoinColumn(name = "job_instance_id", referencedColumnName = "job_instance_id", nullable = false)
	public JobInstance getJobInstance() {
		return jobInstance;
	}

	public void setJobInstance(JobInstance jobInstance) {
		this.jobInstance = jobInstance;
	}

	@Column(name = "create_time", nullable = true)
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name = "start_time", nullable = true)
	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	@Column(name = "end_time", nullable = true)
	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	@Column(name = "status", nullable = true, length = 10)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column(name = "exit_code", nullable = true, length = 2500)
	public String getExitCode() {
		return exitCode;
	}

	public void setExitCode(String exitCode) {
		this.exitCode = exitCode;
	}

	@Column(name = "exit_message", nullable = true, length = 2500)
	public String getExitMessage() {
		return exitMessage;
	}

	public void setExitMessage(String exitMessage) {
		this.exitMessage = exitMessage;
	}

	@Column(name = "last_updated", nullable = true)
	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	@Column(name = "job_configuration_location", nullable = true, length = 2500)
	public String getJobConfigurationLocation() {
		return jobConfigurationLocation;
	}

	public void setJobConfigurationLocation(String jobConfigurationLocation) {
		this.jobConfigurationLocation = jobConfigurationLocation;
	}
}
