package id.co.telkomsigma.btpns.mprospera.response;

public class ListPMResponse {

	private String pmId;
	private String areaName;
	private String areaId;
	private String createdBy;
	private String createdDate;
	private String pmNumber;
	private String pmLocationName;
	private String pmOwner;

	public String getPmOwner() {
		return pmOwner;
	}

	public void setPmOwner(String pmOwner) {
		this.pmOwner = pmOwner;
	}

	public String getPmId() {
		return pmId;
	}

	public void setPmId(String pmId) {
		this.pmId = pmId;
	}

	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getPmNumber() {
		return pmNumber;
	}

	public void setPmNumber(String pmNumber) {
		this.pmNumber = pmNumber;
	}

	public String getPmLocationName() {
		return pmLocationName;
	}

	public void setPmLocationName(String pmLocationName) {
		this.pmLocationName = pmLocationName;
	}

	@Override
	public String toString() {
		return "ListPMResponse [pmId=" + pmId + ", areaName=" + areaName + ", areaId=" + areaId + ", createdBy="
				+ createdBy + ", createdDate=" + createdDate + ", pmNumber=" + pmNumber + ", pmLocationName="
				+ pmLocationName + "]";
	}

}
