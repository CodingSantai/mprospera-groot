package id.co.telkomsigma.btpns.mprospera.model.saving;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "T_SAVING_HIST_TRX")
public class SavingHistTrx extends GenericModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Long savingHistId;
	private Customer customerId;
	private String accountNumber;
	private BigDecimal clearBalance;
	private Date trxDate;
	private String description;
	private BigDecimal amountTrx;

	@Id
	@Column(name = "ID", nullable = false, unique = true)
	@GeneratedValue
	public Long getSavingHistId() {
		return savingHistId;
	}

	public void setSavingHistId(Long savingHistId) {
		this.savingHistId = savingHistId;
	}

	@Column(name = "account_number")
	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	@Column(name = "clear_balance")
	public BigDecimal getClearBalance() {
		return clearBalance;
	}

	public void setClearBalance(BigDecimal clearBalance) {
		this.clearBalance = clearBalance;
	}

	@Column(name = "trx_date")
	public Date getTrxDate() {
		return trxDate;
	}

	public void setTrxDate(Date trxDate) {
		this.trxDate = trxDate;
	}

	@Column(name = "description")
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Column(name = "trx_amt")
	public BigDecimal getAmountTrx() {
		return amountTrx;
	}

	public void setAmountTrx(BigDecimal amountTrx) {
		this.amountTrx = amountTrx;
	}

	@ManyToOne(fetch = FetchType.EAGER, targetEntity = Customer.class)
	@JoinColumn(name = "customer_Id", referencedColumnName = "id", nullable = true)
	public Customer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Customer customerId) {
		this.customerId = customerId;
	}

}
