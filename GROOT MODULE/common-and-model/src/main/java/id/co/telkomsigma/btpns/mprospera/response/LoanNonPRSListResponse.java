package id.co.telkomsigma.btpns.mprospera.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.math.BigDecimal;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class LoanNonPRSListResponse {
	
	private String loanNonPrsId;
	private String loanId;
	private String appId;
	private BigDecimal installmentAmount;
	private BigDecimal currentMargin;
	private BigDecimal installmentPaymentAmount;
	private BigDecimal inputMoney;
	private BigDecimal depositMoney;
	private BigDecimal autoDebet;
	private BigDecimal remainingPrincipal;
	private BigDecimal marginAmount;
	private String overdueDays;
	private BigDecimal outstandingAmount;
	private String outstandingCount;
	private String outstandingReason;
	private String meetingDate;
	private String dueDate;
	private String peopleMeet;
    private boolean payCommitment;
    private String nonPrsPhoto;
    private String status;
    private String createdBy;
    private String createdDate;;
	public String getLoanNonPrsId() {
		return loanNonPrsId;
	}
	public void setLoanNonPrsId(String loanNonPrsId) {
		this.loanNonPrsId = loanNonPrsId;
	}
	public String getLoanId() {
		return loanId;
	}
	public void setLoanId(String loanId) {
		this.loanId = loanId;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public BigDecimal getInstallmentAmount() {
		return installmentAmount;
	}
	public void setInstallmentAmount(BigDecimal installmentAmount) {
		this.installmentAmount = installmentAmount;
	}
	public BigDecimal getCurrentMargin() {
		return currentMargin;
	}
	public void setCurrentMargin(BigDecimal currentMargin) {
		this.currentMargin = currentMargin;
	}
	public String getOverdueDays() {
		return overdueDays;
	}
	public void setOverdueDays(String overdueDays) {
		this.overdueDays = overdueDays;
	}
	public BigDecimal getOutstandingAmount() {
		return outstandingAmount;
	}
	public void setOutstandingAmount(BigDecimal outstandingAmount) {
		this.outstandingAmount = outstandingAmount;
	}
	public String getOutstandingReason() {
		return outstandingReason;
	}
	public void setOutstandingReason(String outstandingReason) {
		this.outstandingReason = outstandingReason;
	}
	public String getMeetingDate() {
		return meetingDate;
	}
	public void setMeetingDate(String meetingDate) {
		this.meetingDate = meetingDate;
	}
	public String getPeopleMeet() {
		return peopleMeet;
	}
	public void setPeopleMeet(String peopleMeet) {
		this.peopleMeet = peopleMeet;
	}
	public boolean getPayCommitment() {
		return payCommitment;
	}
	public void setPayCommitment(boolean payCommitment) {
		this.payCommitment = payCommitment;
	}
	public String getNonPrsPhoto() {
		return nonPrsPhoto;
	}
	public void setNonPrsPhoto(String nonPrsPhoto) {
		this.nonPrsPhoto = nonPrsPhoto;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public BigDecimal getInstallmentPaymentAmount() {
		return installmentPaymentAmount;
	}
	public void setInstallmentPaymentAmount(BigDecimal installmentPaymentAmount) {
		this.installmentPaymentAmount = installmentPaymentAmount;
	}
	public String getOutstandingCount() {
		return outstandingCount;
	}
	public void setOutstandingCount(String outstandingCount) {
		this.outstandingCount = outstandingCount;
	}
	public String getDueDate() {
		return dueDate;
	}
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}
	public BigDecimal getRemainingPrincipal() {
		return remainingPrincipal;
	}
	public void setRemainingPrincipal(BigDecimal remainingPrincipal) {
		this.remainingPrincipal = remainingPrincipal;
	}
	public BigDecimal getMarginAmount() {
		return marginAmount;
	}
	public void setMarginAmount(BigDecimal marginAmount) {
		this.marginAmount = marginAmount;
	}
	public BigDecimal getInputMoney() {
		return inputMoney;
	}
	public void setInputMoney(BigDecimal inputMoney) {
		this.inputMoney = inputMoney;
	}
	public BigDecimal getDepositMoney() {
		return depositMoney;
	}
	public void setDepositMoney(BigDecimal depositMoney) {
		this.depositMoney = depositMoney;
	}
	public BigDecimal getAutoDebet() {
		return autoDebet;
	}
	public void setAutoDebet(BigDecimal autoDebet) {
		this.autoDebet = autoDebet;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}
	
}
