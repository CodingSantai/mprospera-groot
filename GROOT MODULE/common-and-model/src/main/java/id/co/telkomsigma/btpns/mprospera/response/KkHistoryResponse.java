package id.co.telkomsigma.btpns.mprospera.response;

import java.text.SimpleDateFormat;
import java.util.Date;

public class KkHistoryResponse {
	private String createdDate;
	private String countKk;
	private String sdaId;
	private String areaId;
	private String createdBy;
	private String createdAt;

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getSdaId() {
		return sdaId;
	}

	public void setSdaId(String sdaId) {
		this.sdaId = sdaId;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");
		this.createdDate = formatter.format(createdDate);
	}

	public String getCountKk() {
		return countKk;
	}

	public void setCountKk(String countKk) {
		this.countKk = countKk;
	}

	public String getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(String createdAt) {
		this.createdAt = createdAt;
	}

}
