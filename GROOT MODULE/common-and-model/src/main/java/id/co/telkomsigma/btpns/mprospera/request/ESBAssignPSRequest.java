package id.co.telkomsigma.btpns.mprospera.request;

import java.util.List;

public class ESBAssignPSRequest extends BaseRequest {

	private String username;
	private String imei;
	private String approvedBy;
	private List<SentraForAssignPSRequest> sentraList;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getApprovedBy() {
		return approvedBy;
	}

	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}

	public List<SentraForAssignPSRequest> getSentraList() {
		return sentraList;
	}

	public void setSentraList(List<SentraForAssignPSRequest> sentraList) {
		this.sentraList = sentraList;
	}

}
