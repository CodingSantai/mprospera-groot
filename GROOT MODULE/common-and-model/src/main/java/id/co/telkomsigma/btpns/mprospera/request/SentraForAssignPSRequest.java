package id.co.telkomsigma.btpns.mprospera.request;

public class SentraForAssignPSRequest {
	
	private String sentraId;

	public String getSentraId() {
		return sentraId;
	}

	public void setSentraId(String sentraId) {
		this.sentraId = sentraId;
	}
	
	

}
