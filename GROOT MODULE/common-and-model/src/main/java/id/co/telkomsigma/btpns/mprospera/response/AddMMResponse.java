package id.co.telkomsigma.btpns.mprospera.response;

public class AddMMResponse extends BaseResponse {

	private String mmId;

	public String getMmId() {
		return mmId;
	}

	public void setMmId(String mmId) {
		this.mmId = mmId;
	}

	@Override
	public String toString() {
		return "AddMMResponse [mmId=" + mmId + ", getResponseCode()=" + getResponseCode() + ", getResponseMessage()="
				+ getResponseMessage() + "]";
	}

}
