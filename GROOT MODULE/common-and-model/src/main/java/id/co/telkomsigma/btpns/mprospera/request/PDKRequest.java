package id.co.telkomsigma.btpns.mprospera.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.List;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class PDKRequest extends BaseRequest {

	/* untuk list PDK */
	private String getCountData;
	private String page;
	private String startLookupDate;
	private String endLookupDate;

	/* untuk add PDK */
	private String username;
	private String sessionKey;
	private String imei;
	private String pdkLocation;
	private String phoneNumber;
	private String pdkDate;
	private List<PDKDetailRequest> swList;
	private String longitude;
	private String latitude;
	private String action;
	private String pdkId;

	public String getGetCountData() {
		return getCountData;
	}

	public void setGetCountData(String getCountData) {
		this.getCountData = getCountData;
	}

	public String getPage() {
		return page;
	}

	public void setPage(String page) {
		this.page = page;
	}

	public String getStartLookupDate() {
		return startLookupDate;
	}

	public void setStartLookupDate(String startLookupDate) {
		this.startLookupDate = startLookupDate;
	}

	public String getEndLookupDate() {
		return endLookupDate;
	}

	public void setEndLookupDate(String endLookupDate) {
		this.endLookupDate = endLookupDate;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getPdkLocation() {
		return pdkLocation;
	}

	public void setPdkLocation(String pdkLocation) {
		this.pdkLocation = pdkLocation;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getPdkDate() {
		return pdkDate;
	}

	public void setPdkDate(String pdkDate) {
		this.pdkDate = pdkDate;
	}

	public List<PDKDetailRequest> getSwList() {
		return swList;
	}

	public void setSwList(List<PDKDetailRequest> swList) {
		this.swList = swList;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getPdkId() {
		return pdkId;
	}

	public void setPdkId(String pdkId) {
		this.pdkId = pdkId;
	}

	@Override
	public String toString() {
		return "PDKRequest [getCountData=" + getCountData + ", page=" + page + ", startLookupDate=" + startLookupDate
				+ ", endLookupDate=" + endLookupDate + ", username=" + username + ", sessionKey=" + sessionKey
				+ ", imei=" + imei + ", pdkLocation=" + pdkLocation + ", phoneNumber=" + phoneNumber + ", pdkDate="
				+ pdkDate + ", swList=" + swList + ", longitude=" + longitude + ", latitude=" + latitude + ", action="
				+ action + ", pdkId=" + pdkId + ", getTransmissionDateAndTime()=" + getTransmissionDateAndTime()
				+ ", getRetrievalReferenceNumber()=" + getRetrievalReferenceNumber() + "]";
	}

}
