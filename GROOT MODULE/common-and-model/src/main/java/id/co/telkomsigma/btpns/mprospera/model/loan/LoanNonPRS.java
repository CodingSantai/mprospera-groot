package id.co.telkomsigma.btpns.mprospera.model.loan;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;
import id.co.telkomsigma.btpns.mprospera.model.customer.Customer;
import id.co.telkomsigma.btpns.mprospera.model.parameter.PRSParameter;
import id.co.telkomsigma.btpns.mprospera.model.sentra.Sentra;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "T_Loan_Non_PRS")
public class LoanNonPRS extends GenericModel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Long loanNonPRSId;
    private Date createdDate;
    private Date dueDate;
    private Customer customerId;
    private Loan loanId;
    private Sentra sentraId;
    private String createdBy;
    private Date updatedDate;
    private String updatedBy;
    private String status;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getLoanNonPRSId() {
        return loanNonPRSId;
    }

    public void setLoanNonPRSId(Long loanNonPRSId) {
        this.loanNonPRSId = loanNonPRSId;
    }

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Customer.class)
    @JoinColumn(name = "customer_id", referencedColumnName = "id")
    public Customer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Customer customerId) {
        this.customerId = customerId;
    }

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Sentra.class)
    @JoinColumn(name = "SENTRA_ID", referencedColumnName = "id")
    public Sentra getSentraId() {
        return sentraId;
    }

    public void setSentraId(Sentra sentraId) {
        this.sentraId = sentraId;
    }

    @ManyToOne(fetch = FetchType.EAGER, targetEntity = Loan.class)
    @JoinColumn(name = "loan_Id", referencedColumnName = "id")
    public Loan getLoanId() {
        return loanId;
    }

    public void setLoanId(Loan loanId) {
        this.loanId = loanId;
    }

    @Column(name = "CREATED_DATE")
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "CREATED_BY")
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "UPDATED_DATE")
    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Column(name = "UPDATED_BY")
    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Column(name = "STATUS")
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}