package id.co.telkomsigma.btpns.mprospera.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class DenomRequest {

	private String denomId;
	private String count;

	public String getDenomId() {
		return denomId;
	}

	public void setDenomId(String denomId) {
		this.denomId = denomId;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

}
