package id.co.telkomsigma.btpns.mprospera.model.parameter;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "T_PRS_PARAMETER")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class PRSParameter extends GenericModel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private Long parameterId;
    private String paramName;
    private String paramType;
    private String paramGroup;
    private String paramValueString;
    private byte[] paramValueByte;
    private String paramDescription;
    private String fileType;
    private Long createdBy;
    private Date createdDate;
    private Long updatedBy;
    private Date updatedDate;

    @Id
    @Column(name = "id", nullable = false, unique = true)
    @GeneratedValue
    public Long getParameterId() {
        return parameterId;
    }

    public void setParameterId(Long parameterId) {
        this.parameterId = parameterId;
    }

    @Column(name = "PARAM_NAME", nullable = false, length = MAX_LENGTH_PARAM_NAME)
    public String getParamName() {
        return paramName;
    }

    public void setParamName(String paramName) {
        this.paramName = paramName;
    }

    @Column(name = "PARAM_TYPE", nullable = false)
    public String getParamType() {
        return paramType;
    }

    public void setParamType(String paramType) {
        this.paramType = paramType;
    }

    @Column(name = "PARAM_GROUP", nullable = false)
    public String getParamGroup() {
        return paramGroup;
    }

    public void setParamGroup(String paramGroup) {
        this.paramGroup = paramGroup;
    }

    @Column(name = "PARAM_VALUE_STRING", length = MAX_LENGTH_PARAM_VALUE)
    public String getParamValueString() {
        return paramValueString;
    }

    public void setParamValueString(String paramValueString) {
        this.paramValueString = paramValueString;
    }

    @Column(name = "PARAM_VALUE_BYTE", length = 2000000)
    public byte[] getParamValueByte() {
        return paramValueByte;
    }

    public void setParamValueByte(byte[] paramValueByte) {
        this.paramValueByte = paramValueByte;
    }

    @Column(name = "PARAM_DESCRIPTION", nullable = false, length = MAX_LENGTH_DESCRIPTION)
    public String getParamDescription() {
        return paramDescription;
    }

    public void setParamDescription(String paramDescription) {
        this.paramDescription = paramDescription;
    }

    @Column(name = "created_by")
    public Long getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(Long createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "CREATED_DT", nullable = false)
    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    @Column(name = "updated_by")
    public Long getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Long updatedBy) {
        this.updatedBy = updatedBy;
    }

    @Column(name = "UPDATE_DT")
    public Date getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(Date updatedDate) {
        this.updatedDate = updatedDate;
    }

    @Column(name = "FILE_TYPE")
    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public PRSParameter() {

    }

    public PRSParameter(String id) {
        this.parameterId = Long.parseLong(id);
    }

}