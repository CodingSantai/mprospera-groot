package id.co.telkomsigma.btpns.mprospera.model;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "T_HOLIDAY")
public class Holiday extends GenericModel {

	private Long id;
	private Date date;
	
	@Id
	@Column(name = "id", nullable = false, unique = true)
	@GeneratedValue
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Column(name = "holiday_dt", nullable = false)
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

}
