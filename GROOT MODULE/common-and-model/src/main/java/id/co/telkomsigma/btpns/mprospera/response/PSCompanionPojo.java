package id.co.telkomsigma.btpns.mprospera.response;

public class PSCompanionPojo {

	private String psId;
	private String code;
	private String psName;

	public String getPsId() {
		return psId;
	}

	public void setPsId(String psId) {
		this.psId = psId;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getPsName() {
		return psName;
	}

	public void setPsName(String psName) {
		this.psName = psName;
	}

}
