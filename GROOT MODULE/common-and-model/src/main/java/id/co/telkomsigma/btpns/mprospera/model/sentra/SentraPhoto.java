package id.co.telkomsigma.btpns.mprospera.model.sentra;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;

@Entity
@Table(name = "T_SENTRA_PHOTO")
public class SentraPhoto extends GenericModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2462843802614081624L;
	private Long id;
	private Sentra sentraId;
	private byte[] sentraPhoto;

	@Id
	@Column(name = "id", nullable = false, unique = true)
	@GeneratedValue
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@ManyToOne(fetch = FetchType.EAGER, targetEntity = Sentra.class)
	@JoinColumn(name = "sentra_id", referencedColumnName = "id", nullable = true)
	public Sentra getSentraId() {
		return sentraId;
	}

	public void setSentraId(Sentra sentraId) {
		this.sentraId = sentraId;
	}

	@Column(name = "sentra_photo", nullable = true)
	public byte[] getSentraPhoto() {
		return sentraPhoto;
	}

	public void setSentraPhoto(byte[] sentraPhoto) {
		this.sentraPhoto = sentraPhoto;
	}

}
