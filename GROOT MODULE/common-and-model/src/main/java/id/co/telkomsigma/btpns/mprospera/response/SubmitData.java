package id.co.telkomsigma.btpns.mprospera.response;

import id.co.telkomsigma.btpns.mprospera.request.ErrorLogRequest;

import java.util.List;

public class SubmitData {
	String transmissionDateAndTime;
	String retrievalReferenceNumber;
	String username;
	String sessionKey;
	String imei;
	String processor;
	String ram;
	String gpsVersion;
	String totalDeviceMemory;
	String operatorName;
	String signal;

	public String getOperatorName() {
		return operatorName;
	}

	public void setOperatorName(String operatorName) {
		this.operatorName = operatorName;
	}

	public String getSignal() {
		return signal;
	}

	public void setSignal(String signal) {
		this.signal = signal;
	}

	public String getTransmissionDateAndTime() {
		return transmissionDateAndTime;
	}

	public void setTransmissionDateAndTime(String transmissionDateAndTime) {
		this.transmissionDateAndTime = transmissionDateAndTime;
	}

	public String getRetrievalReferenceNumber() {
		return retrievalReferenceNumber;
	}

	public void setRetrievalReferenceNumber(String retrievalReferenceNumber) {
		this.retrievalReferenceNumber = retrievalReferenceNumber;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getProcessor() {
		return processor;
	}

	public void setProcessor(String processor) {
		this.processor = processor;
	}

	public String getRam() {
		return ram;
	}

	public void setRam(String ram) {
		this.ram = ram;
	}

	public String getGpsVersion() {
		return gpsVersion;
	}

	public void setGpsVersion(String gpsVersion) {
		this.gpsVersion = gpsVersion;
	}

	public String getTotalDeviceMemory() {
		return totalDeviceMemory;
	}

	public void setTotalDeviceMemory(String totalDeviceMemory) {
		this.totalDeviceMemory = totalDeviceMemory;
	}

	public String getFreeDeviceMemory() {
		return freeDeviceMemory;
	}

	public void setFreeDeviceMemory(String freeDeviceMemory) {
		this.freeDeviceMemory = freeDeviceMemory;
	}

	public List<ErrorLogRequest> getErrorLog() {
		return errorLog;
	}

	public void setErrorLog(List<ErrorLogRequest> errorLog) {
		this.errorLog = errorLog;
	}

	String freeDeviceMemory;
	List<ErrorLogRequest> errorLog;

}
