package id.co.telkomsigma.btpns.mprospera.request;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class AddCustomerRequest extends BaseRequest {

	private String username;
	private String sessionKey;
	private String imei;
	private String pdkId;
	private String swId;
	private String status;
	private String longitude;
	private String latitude;
	private String action;
	private String customerId;
	private String groupId;

	// for prospera purpose
	private String customerNumberCenter;
	private String customerNumberGroup;
	private String fullName;
	private String governmentId;
	private String actionFlag;
	private String reffId;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getPdkId() {
		return pdkId;
	}

	public void setPdkId(String pdkId) {
		this.pdkId = pdkId;
	}

	public String getSwId() {
		return swId;
	}

	public void setSwId(String swId) {
		this.swId = swId;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getCustomerId() {
		return customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getCustomerNumberCenter() {
		return customerNumberCenter;
	}

	public void setCustomerNumberCenter(String customerNumberCenter) {
		this.customerNumberCenter = customerNumberCenter;
	}

	public String getCustomerNumberGroup() {
		return customerNumberGroup;
	}

	public void setCustomerNumberGroup(String customerNumberGroup) {
		this.customerNumberGroup = customerNumberGroup;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getGovernmentId() {
		return governmentId;
	}

	public void setGovernmentId(String governmentId) {
		this.governmentId = governmentId;
	}

	public String getActionFlag() {
		return actionFlag;
	}

	public void setActionFlag(String actionFlag) {
		this.actionFlag = actionFlag;
	}

	public String getReffId() {
		return reffId;
	}

	public void setReffId(String reffId) {
		this.reffId = reffId;
	}

	@Override
	public String toString() {
		return "AddCustomerRequest [username=" + username + ", sessionKey=" + sessionKey + ", imei=" + imei + ", pdkId="
				+ pdkId + ", swId=" + swId + ", status=" + status + ", longitude=" + longitude + ", latitude="
				+ latitude + ", action=" + action + ", customerId=" + customerId + ", groupId=" + groupId
				+ ", getTransmissionDateAndTime()=" + getTransmissionDateAndTime() + ", getRetrievalReferenceNumber()="
				+ getRetrievalReferenceNumber() + "]";
	}

}
