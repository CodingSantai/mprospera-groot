package id.co.telkomsigma.btpns.mprospera.request;

public class ESBLoan {

	private String appId;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	@Override
	public String toString() {
		return "Loan [appId=" + appId + "]";
	}

}
