package id.co.telkomsigma.btpns.mprospera.response;

import java.util.List;

public class ListPDKResponse {

	private String pdkId;
	private String pdkLocation;
	private String phoneNumber;
	private String latitude;
	private String longitude;
	private Integer totalParticipant;
	private Integer graduatedParticipant;
	private String createdBy;
	private String pdkDate;
	private List<PDKDetailResponse> swList;

	public String getPdkId() {
		return pdkId;
	}

	public void setPdkId(String pdkId) {
		this.pdkId = pdkId;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getPdkLocation() {
		return pdkLocation;
	}

	public void setPdkLocation(String pdkLocation) {
		this.pdkLocation = pdkLocation;
	}

	public Integer getTotalParticipant() {
		return totalParticipant;
	}

	public void setTotalParticipant(Integer totalParticipant) {
		this.totalParticipant = totalParticipant;
	}

	public Integer getGraduatedParticipant() {
		return graduatedParticipant;
	}

	public void setGraduatedParticipant(Integer graduatedParticipant) {
		this.graduatedParticipant = graduatedParticipant;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getPdkDate() {
		return pdkDate;
	}

	public void setPdkDate(String pdkDate) {
		this.pdkDate = pdkDate;
	}

	public List<PDKDetailResponse> getPdkDetail() {
		return swList;
	}

	public void setPdkDetail(List<PDKDetailResponse> swList) {
		this.swList = swList;
	}

	@Override
	public String toString() {
		return "ListPDKResponse [pdkId=" + pdkId + ", pdkLocation=" + pdkLocation + ", totalParticipant="
				+ totalParticipant + ", graduatedParticipant=" + graduatedParticipant + ", createdBy=" + createdBy
				+ ", pdkDate=" + pdkDate + "]";
	}

}
