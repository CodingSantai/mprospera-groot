package id.co.telkomsigma.btpns.mprospera.model.sw;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import id.co.telkomsigma.btpns.mprospera.model.GenericModel;


@Entity
@Table(name = "T_PRODUCT_CYCLE")
public class ProductCycle extends GenericModel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private long cycleId;
	private long idProduct;
	private String productCode;
	private Integer cycleValue;
	
	
	@Id
	@Column(name = "id", nullable = false, unique = true)
	@GeneratedValue
	public long getCycleId() {
		return cycleId;
	}
	public void setCycleId(long cycleId) {
		this.cycleId = cycleId;
	}
	
	@Column(name = "product_id")
	public long getIdProduct() {
		return idProduct;
	}
	public void setIdProduct(long idProduct) {
		this.idProduct = idProduct;
	}
	
	@Column(name = "product_code")
	public String getProductCode() {
		return productCode;
	}
	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}
	
	@Column(name = "cycle_value")
	public Integer getCycleValue() {
		return cycleValue;
	}
	public void setCycleValue(Integer cycleValue) {
		this.cycleValue = cycleValue;
	}

	
}
