package id.co.telkomsigma.btpns.mprospera.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.math.BigDecimal;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class SavingPRSListResponse extends BaseResponse {

	private String accountId;
	private String accountNumber;
	private BigDecimal withdrawalAmount;
	private String withdrawalAmountPlan;
	private String nextWithdrawalAmountPlan;
	private String isCloseSavingAccount;
	private BigDecimal holdBalance;
	private BigDecimal clearBalance;
	private BigDecimal depositAmount;

	public String getAccountId() {
		return accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public BigDecimal getWithdrawalAmount() {
		return withdrawalAmount;
	}

	public void setWithdrawalAmount(BigDecimal withdrawalAmount) {
		this.withdrawalAmount = withdrawalAmount;
	}

	public String getWithdrawalAmountPlan() {
		return withdrawalAmountPlan;
	}

	public void setWithdrawalAmountPlan(String withdrawalAmountPlan) {
		this.withdrawalAmountPlan = withdrawalAmountPlan;
	}

	public String getNextWithdrawalAmountPlan() {
		return nextWithdrawalAmountPlan;
	}

	public void setNextWithdrawalAmountPlan(String nextWithdrawalAmountPlan) {
		this.nextWithdrawalAmountPlan = nextWithdrawalAmountPlan;
	}

	public String getIsCloseSavingAccount() {
		return isCloseSavingAccount;
	}

	public void setIsCloseSavingAccount(String isCloseSavingAccount) {
		this.isCloseSavingAccount = isCloseSavingAccount;
	}

	public BigDecimal getHoldBalance() {
		return holdBalance;
	}

	public void setHoldBalance(BigDecimal holdBalance) {
		this.holdBalance = holdBalance;
	}

	public BigDecimal getClearBalance() {
		return clearBalance;
	}

	public void setClearBalance(BigDecimal clearBalance) {
		this.clearBalance = clearBalance;
	}

	public BigDecimal getDepositAmount() {
		return depositAmount;
	}

	public void setDepositAmount(BigDecimal depositAmount) {
		this.depositAmount = depositAmount;
	}

	@Override
	public String toString() {
		return "SavingPRSListResponse [accountId=" + accountId + ", accountNumber=" + accountNumber
				+ ", withdrawalAmount=" + withdrawalAmount + ", withdrawalAmountPlan=" + withdrawalAmountPlan
				+ ", nextWithdrawalAmountPlan=" + nextWithdrawalAmountPlan + ", isCloseSavingAccount="
				+ isCloseSavingAccount + ", holdBalance=" + holdBalance + ", clearBalance=" + clearBalance
				+ ", depositAmount=" + depositAmount + ", getResponseCode()=" + getResponseCode()
				+ ", getResponseMessage()=" + getResponseMessage() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}

}
