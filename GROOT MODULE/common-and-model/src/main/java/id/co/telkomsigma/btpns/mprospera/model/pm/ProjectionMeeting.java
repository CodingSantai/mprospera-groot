package id.co.telkomsigma.btpns.mprospera.model.pm;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import id.co.telkomsigma.btpns.mprospera.model.GenericModel;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "T_PROJECTION_MEETING")
@JsonSerialize(include = JsonSerialize.Inclusion.NON_EMPTY)
public class ProjectionMeeting extends GenericModel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3336450864575228543L;
	private Long pmId;
	private String areaId;
	private String pmLocationName;
	private String pmOwner;
	private String pmNumber;
	private String longitude;
	private String latitude;
	private Date createdDate;
	private String createdBy;
	private String rrn;
	private Boolean isDeleted = false;

	private String areaName;

	@Id
	@Column(name = "id", nullable = false, unique = true)
	@GeneratedValue
	public Long getPmId() {
		return pmId;
	}

	public void setPmId(Long pmId) {
		this.pmId = pmId;
	}

	@Column(name = "area_id", nullable = false)
	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	@Column(name = "location_name", nullable = false)
	public String getPmLocationName() {
		return pmLocationName;
	}

	public void setPmLocationName(String pmLocationName) {
		this.pmLocationName = pmLocationName;
	}

	@Column(name = "owner", nullable = false)
	public String getPmOwner() {
		return pmOwner;
	}

	public void setPmOwner(String pmOwner) {
		this.pmOwner = pmOwner;
	}

	@Column(name = "pm_number", nullable = false)
	public String getPmNumber() {
		return pmNumber;
	}

	public void setPmNumber(String pmNumber) {
		this.pmNumber = pmNumber;
	}

	@Column(name = "longitude", nullable = false)
	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	@Column(name = "latitude", nullable = false)
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	@Column(name = "created_date", nullable = false)
	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	@Column(name = "created_by", nullable = false)
	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	@Column(name = "is_deleted")
	public Boolean getIsDeleted() {
		return isDeleted;
	}

	public void setIsDeleted(Boolean isDeleted) {
		this.isDeleted = isDeleted;
	}

	@Column(name = "rrn")
	public String getRrn() {
		return rrn;
	}

	public void setRrn(String rrn) {
		this.rrn = rrn;
	}

	@Transient
	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

}
