/**
 * 
 */
package id.co.telkomsigma.btpns.mprospera.request;

import java.util.List;

/**
 * 
 *
 */
public class SubmitPRSRequest extends BaseRequest {

	private String username;
	private String sessionKey;
	private String imei;
	private String longitude;
	private String latitude;

	private String prsId;
	private String sentraId;
	private String prsPhoto;
	private String prsDate;
	private String status;
	private String bringMoney;
	private String actualMoney;
	private String paymentMoney;
	private String withdrawalAdhoc;
	private String psIdCompanion;
	private List<DenomRequest> denomList;
	private List<CustomerPRSRequest> customerList;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getSessionKey() {
		return sessionKey;
	}

	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public String getImei() {
		return imei;
	}

	public void setImei(String imei) {
		this.imei = imei;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getPrsId() {
		return prsId;
	}

	public void setPrsId(String prsId) {
		this.prsId = prsId;
	}

	public String getSentraId() {
		return sentraId;
	}

	public void setSentraId(String sentraId) {
		this.sentraId = sentraId;
	}

	public String getPrsPhoto() {
		return prsPhoto;
	}

	public void setPrsPhoto(String prsPhoto) {
		this.prsPhoto = prsPhoto;
	}

	public String getPrsDate() {
		return prsDate;
	}

	public void setPrsDate(String prsDate) {
		this.prsDate = prsDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPsIdCompanion() {
		return psIdCompanion;
	}

	public void setPsIdCompanion(String psIdCompanion) {
		this.psIdCompanion = psIdCompanion;
	}

	public List<DenomRequest> getDenomList() {
		return denomList;
	}

	public void setDenomList(List<DenomRequest> denomList) {
		this.denomList = denomList;
	}

	public List<CustomerPRSRequest> getCustomerList() {
		return customerList;
	}

	public void setCustomerList(List<CustomerPRSRequest> customerList) {
		this.customerList = customerList;
	}

	public String getBringMoney() {
		return bringMoney;
	}

	public void setBringMoney(String bringMoney) {
		this.bringMoney = bringMoney;
	}

	public String getActualMoney() {
		return actualMoney;
	}

	public void setActualMoney(String actualMoney) {
		this.actualMoney = actualMoney;
	}

	public String getPaymentMoney() {
		return paymentMoney;
	}

	public void setPaymentMoney(String paymentMoney) {
		this.paymentMoney = paymentMoney;
	}

	public String getWithdrawalAdhoc() {
		return withdrawalAdhoc;
	}

	public void setWithdrawalAdhoc(String withdrawalAdhoc) {
		this.withdrawalAdhoc = withdrawalAdhoc;
	}

}
