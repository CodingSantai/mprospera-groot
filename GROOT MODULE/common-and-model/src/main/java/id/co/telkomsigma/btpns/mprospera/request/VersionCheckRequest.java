package id.co.telkomsigma.btpns.mprospera.request;

public class VersionCheckRequest extends BaseRequest {
	private String currentVersion;
	private String[] allowedVersions;
	private String url;

	public String getCurrentVersion() {
		return currentVersion;
	}

	public void setCurrentVersion(String currentVersion) {
		this.currentVersion = currentVersion;
	}

	public String[] getAllowedVersions() {
		return allowedVersions;
	}

	public void setAllowedVersions(String[] allowedVersions) {
		this.allowedVersions = allowedVersions;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
}
