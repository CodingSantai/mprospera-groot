package id.co.telkomsigma.btpns.mprospera.model.survey;

import scala.Serializable;

import javax.persistence.*;

@Embeddable
public class SurveyDetailId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String questionId;

	public SurveyDetailId() {

	}

	@Column(name = "question_id")
	public String getQuestionId() {
		return questionId;
	}

	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}

	private Survey survey;

	@ManyToOne(fetch = FetchType.EAGER, targetEntity = Survey.class)
	@JoinColumn(name = "survey_id", referencedColumnName = "id", nullable = true)
	public Survey getSurvey() {
		return survey;
	}

	public void setSurvey(Survey survey) {
		this.survey = survey;
	}

}
