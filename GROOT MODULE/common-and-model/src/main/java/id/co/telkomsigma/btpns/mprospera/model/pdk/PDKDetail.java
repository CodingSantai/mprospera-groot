package id.co.telkomsigma.btpns.mprospera.model.pdk;

import scala.Serializable;

import javax.persistence.*;

@Entity
@Table(name = "T_PDK_DETAIL")
@IdClass(PDKDetailId.class)
public class PDKDetail implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 8383613399759073380L;

    private String graduateStatus;

    public PDKDetail() {

    }

    @Column(name = "graduate_status")
    public String getGraduateStatus() {
        return graduateStatus;
    }

    public void setGraduateStatus(String graduateStatus) {
        this.graduateStatus = graduateStatus;
    }

    private Long swId;

    @Id
    @Column(name = "sw_id")
    public Long getSwId() {
        return swId;
    }

    public void setSwId(Long swId) {
        this.swId = swId;
    }

    private PelatihanDasarKeanggotaan pdk;

    @Id
    @ManyToOne(fetch = FetchType.LAZY, targetEntity = PelatihanDasarKeanggotaan.class)
    @JoinColumn(name = "pdk_id", referencedColumnName = "id")
    public PelatihanDasarKeanggotaan getPdk() {
        return pdk;
    }

    public void setPdk(PelatihanDasarKeanggotaan pdk) {
        this.pdk = pdk;
    }

}