package id.co.telkomsigma.btpns.mprospera.response;

import java.util.Arrays;

public class NonPrsPhotoResponse extends BaseResponse{
	
	private byte[] photo;

	public byte[] getPhoto() {
		return photo;
	}

	public void setPhoto(byte[] photo) {
		this.photo = photo;
	}

	@Override
	public String toString() {
		return "NonPrsPhotoResponse [photo=" + Arrays.toString(photo) + ", getResponseCode()=" + getResponseCode()
				+ ", getResponseMessage()=" + getResponseMessage() + ", toString()=" + super.toString()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + "]";
	}
	
}
