package id.co.telkomsigma.btpns.mprospera.exception;

/**
 * Created by daniel on 4/20/15.
 */
public class UserProfileException extends Exception {
    /**
     *
     */
    private static final long serialVersionUID = 1L;
    public static final String NOT_UNIQUE_USERNAME_AND_EMAIL = "11";
    public static final String NOT_UNIQUE_USERNAME = "10";
    public static final String NOT_UNIQUE_EMAIL = "01";

    private String message = "";

    public UserProfileException(boolean notUniqueUsername, boolean notUniqueEmail) {
        super("CustomerProfileException");
        if (notUniqueUsername && notUniqueEmail) {
            setMessage(NOT_UNIQUE_USERNAME_AND_EMAIL);
        } else if (notUniqueUsername) {
            setMessage(NOT_UNIQUE_USERNAME);
        } else if (notUniqueEmail) {
            setMessage(NOT_UNIQUE_EMAIL);
        }
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}