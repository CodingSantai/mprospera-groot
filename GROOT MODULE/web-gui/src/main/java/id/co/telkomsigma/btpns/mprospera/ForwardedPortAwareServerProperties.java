/*
package id.co.telkomsigma.btpns.mprospera;

import org.apache.catalina.Valve;
import org.apache.catalina.valves.RemoteIpValve;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.boot.context.embedded.ConfigurableEmbeddedServletContainer;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ForwardedPortAwareServerProperties extends ServerProperties {
    @Override
    public void customize(ConfigurableEmbeddedServletContainer container) {
        super.customize(container);

        if (container instanceof TomcatEmbeddedServletContainerFactory) {
            final TomcatEmbeddedServletContainerFactory factory = (TomcatEmbeddedServletContainerFactory) container;
            RemoteIpValve remoteIpValve = new RemoteIpValve();
            remoteIpValve.setPortHeader("x-forwarded-port");
//            remoteIpValve.setInternalProxies("");
            remoteIpValve.setRemoteIpHeader("x-forwarded-for");
            remoteIpValve.setProtocolHeader("x-forwarded-proto");
            factory.getEngineValves().add(remoteIpValve);
        }
    }
}
*/
